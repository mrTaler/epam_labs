﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04.MVVMAddin
{
    using System.Collections.ObjectModel;
    using System.Globalization;

    /// <summary>
    /// The string converter.
    /// </summary>
    public class StringTypleConverter : ConvertorBase<StringTypleConverter>
    {
        /// <summary>
        /// The convert.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var otv = (ObservableCollection<string>)value;
            return otv;
        }
    }
}
