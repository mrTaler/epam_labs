﻿namespace Task04.MVVMAddin
{
    using System.Collections.Generic;
    using System.ComponentModel;

    /// <summary>
    /// The view model base.
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Dictionary which holds information about which property is dependent on which. This is used by the
        /// property change notification system.
        /// Key: a property of the viewmodel.
        /// Value: an array of properties, which are dependent on the property.
        /// </summary>
        protected Dictionary<string, string[]> PropertyDependencyMap
            = new Dictionary<string, string[]>();

        /// <summary>
        /// Notify registered listeners that a change has happened. Used when setting property values.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        protected void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                // Notify property change
                PropertyChanged(this, new PropertyChangedEventArgs(property));

                // Notify dependencies, if any
                if (PropertyDependencyMap.ContainsKey(property))
                    foreach (string dependencyProperty in PropertyDependencyMap[property])
                        NotifyPropertyChanged(dependencyProperty);
            }
        }
    }
}
