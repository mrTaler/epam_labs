﻿using System;

namespace Task04.MVVM.Model
{
    /// <summary>
    /// The test init model.
    /// </summary>
    public class TestInitDataModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestInitDataModel"/> class.
        /// </summary>
        public TestInitDataModel()
        {
            this.Treads =2;
            this.RequestsQuanity = 10;
            this.BaseUrl = "http://localhost:59079";
            this.TestDuration =TimeSpan.FromSeconds(10);
            this.Field5 = "j";
            this.Field6 = "j";
            this.Field7 = "j";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestInitDataModel"/> class.
        /// </summary>
        /// <param name="test">
        /// The test.
        /// </param>
        public TestInitDataModel(TestInitDataModel test)
        {
            this.Treads = test.Treads;
            this.RequestsQuanity = test.RequestsQuanity;
            this.BaseUrl = test.BaseUrl;
            this.TestDuration = test.TestDuration;
            this.Field5 = test.Field5;
            this.Field6 = test.Field6;
            this.Field7 = test.Field7;
        }

        /// <summary>
        /// Gets or sets the field 1.
        /// </summary>
        public int Treads { get; set; }

        /// <summary>
        /// Gets or sets the field 2.
        /// </summary>
        public int RequestsQuanity{ get; set; }

        /// <summary>
        /// Gets or sets the field 3.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Gets or sets the field 4.
        /// </summary>
        public TimeSpan TestDuration { get; set; }

        /// <summary>
        /// Gets or sets the field 5.
        /// </summary>
        public string Field5 { get; set; }

        /// <summary>
        /// Gets or sets the field 6.
        /// </summary>
        public string Field6 { get; set; }

        /// <summary>
        /// Gets or sets the field 7.
        /// </summary>
        public string Field7 { get; set; }
    }
}
