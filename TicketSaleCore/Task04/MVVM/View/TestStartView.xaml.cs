﻿namespace Task04.MVVM.View
{
    using System.Windows;

    using Task04.MVVM.ViewModel;

    /// <summary>
    /// Логика взаимодействия для TestStartView.xaml
    /// </summary>
    public partial class TestStartView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestStartView"/> class.
        /// </summary>
        public TestStartView()
        {
            this.DataContext = new TestStartViewModel(this);
            this.InitializeComponent();
        }
    }
}
