﻿namespace Task04.MVVM.View
{
    using System.Windows;

    using Task04.MVVM.Model;
    using Task04.MVVM.ViewModel;

    /// <summary>
    /// Логика взаимодействия для TestInitView.xaml
    /// </summary>
    public partial class TestInitView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestInitView"/> class.
        /// </summary>
        /// <param name="test">
        /// The test.
        /// </param>
        public TestInitView(TestInitDataModel test)
        {
            this.DataContext = new TestInitViewModel(test);
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
