﻿using System.Linq;
using System.Threading;

namespace Task04.MVVM.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Threading;

    using Task04.MVVM.Model;
    using Task04.MVVM.View;
    using Task04.MVVMAddin;

    /// <summary>
    /// The test start view model.
    /// </summary>
    public class TestStartViewModel : ViewModelBase
    {
        /// <summary>
        /// The owner.
        /// </summary>
        private readonly Window owner;

        private readonly Dispatcher dispatcher;

        private List<Tuple<bool, int, int>> requestInfoList;
        private CancellationTokenSource cancelTokenSource;
        private static object synObject = new Object();

        private ObservableCollection<string> log;
        private int progress;
        private int ordinateSize;
        private ObservableCollection<Tuple<int, int, int, int>> graphPointsCollection;
        private Tuple<int, int, int, int, double> output;


        /// <summary>
        /// Initializes a new instance of the <see cref="TestStartViewModel"/> class.
        /// </summary>
        /// <param name="owner">
        /// The owner.
        /// </param>
        public TestStartViewModel(Window owner)
        {
            this.owner = owner;
            this.TestInitDataCommand = new ViewModelCommand(this.TestInitData, true);
            this.TestStartCommand = new ViewModelCommand(this.StartTest, false);
            this.TestCancelCommand = new ViewModelCommand(this.TestCancel, false);
            this.dispatcher = Dispatcher.CurrentDispatcher;
        }

        /// <summary>
        /// Gets or sets the test init data Command.
        /// </summary>
        public ViewModelCommand TestInitDataCommand { get; set; }

        /// <summary>
        /// Test Cancel Command
        /// </summary>
        public ViewModelCommand TestCancelCommand { get; set; }

        /// <summary>
        /// Gets or sets the test start command.
        /// </summary>
        public ViewModelCommand TestStartCommand { get; set; }

        /// <summary>
        /// Gets or sets the init test data.
        /// </summary>
        public TestInitDataModel InitTestData { get; set; }

        /// <summary>
        /// The test init data.
        /// </summary>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        private void TestInitData(object parameter)
        {
            if (this.InitTestData == null)
            {
                this.InitTestData = new TestInitDataModel();
            }

            var copy = new TestInitDataModel(this.InitTestData);


            var testWin = new TestInitView(copy)
            {
                // Owner = this.owner
            };
            var result = testWin.ShowDialog();

            if (!result.HasValue || (result != true)) return;

            var qeryF = ((TestInitViewModel)testWin.DataContext).TestDataInit;
            this.InitTestData = qeryF;
            cancelTokenSource = new CancellationTokenSource();
            this.TestStartCommand.CanExecute = true;
            this.NotifyPropertyChanged(nameof(this.InitTestData));
        }

        /// <summary>
        /// TestCancel
        /// </summary>
        /// <param name="parameter"></param>
        private void TestCancel(object parameter)
        {
            this.TestStartCommand.CanExecute = false;
            this.TestCancelCommand.CanExecute = false;
            this.TestInitDataCommand.CanExecute = true;
            cancelTokenSource.Cancel();
        }

        /// <summary>
        /// The start test.
        /// </summary>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        private void StartTest(object parameter)
        {
            this.TestStartCommand.CanExecute = false;
            this.TestCancelCommand.CanExecute = true;
            this.TestInitDataCommand.CanExecute = false;

            Task runningTestTask = new Task(() => Run(InitTestData));

            runningTestTask.Start();
        }
        /*
            
                                  #region MyRegion




                                  /*   string qt = null;
                                     Stopwatch sw = Stopwatch.StartNew();
                                     try
                                     {

                                         HttpClient client = new HttpClient();


                                         client.Timeout = (TimeSpan.FromMinutes(5));
                                         client.DefaultRequestHeaders
                                             .TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8"); // "Content-Type"
                                         client.BaseAddress = new Uri(InitTestData.BaseUrl + $"?taskNo={i}");

                                         HttpResponseMessage response = await client.GetAsync(client.BaseAddress);
                                         sw.Stop();
                                         var statusCode = response.StatusCode.ToString();



                                         qt = $" numder:{i}," + $" code :{statusCode}, "
                                              + $" time is:{sw.Elapsed.ToString()}";
                                     }

                                     catch (AggregateException ex)
                                     {
                                         foreach (Exception inner in ex.InnerExceptions)
                                         {
                                             sw.Stop();
                                             qt = qt + inner.Message + sw.Elapsed.ToString();
                                             /*   await this.dispatcher.BeginInvoke(
                                                    new Action(
                                                        () =>
                                                            {
                                                                this.ResPonseOBS.Add(
                                                              $"Error: {inner.Message}");
                                                            }));*/
        /*      }
          }
          catch (TaskCanceledException ex)
          {
              sw.Stop();
    
              qt = ex.Message + sw.Elapsed.ToString();
    
          }
          catch (HttpRequestException ex)
          {
              sw.Stop();
    
              qt = ex.Message + sw.Elapsed.ToString(); ;
          }
          if (ct.IsCancellationRequested)
          {
              qt = $"Cancel By Time {i}";
          }
    
          await this.dispatcher.BeginInvoke(
              new Action(
                  () =>
                      {
                          this.ResPonseOBS.Add(qt);
                      }));*/

        /*               #endregion
                   });
               newPoint();
           }
           catch (OperationCanceledException ex)
           {
               MessageBox.Show("Operation is cancelled"); //is wrong
               cancelTokenSource.Dispose();
    
           }
       }
           }*/



        public ObservableCollection<string> Log
        {
            get => log;
            set
            {
                log = value;
                NotifyPropertyChanged(nameof(Log));
            }
        }

        public int Progress
        {
            get => progress;
            set
            {
                progress = value;
                NotifyPropertyChanged(nameof(Progress));
            }
        }

        public int OrdinateSize
        {
            get => ordinateSize;
            private set
            {
                ordinateSize = value;
                NotifyPropertyChanged(nameof(OrdinateSize));
            }
        }

        public ObservableCollection<Tuple<int, int, int, int>> GraphPointsCollection
        {
            get => graphPointsCollection;
            private set
            {
                graphPointsCollection = value;
                NotifyPropertyChanged(nameof(GraphPointsCollection));
            }
        }

        public Tuple<int, int, int, int, double> Output
        {
            get => output;
            set
            {
                output = value;
                NotifyPropertyChanged(nameof(Output));
            }
        }

        private void SendRequest(string url, int index, int time)
        {
            bool responseResult;
            var sw = Stopwatch.StartNew();
            using (var client = new HttpClient())
            {
                client.Timeout = (TimeSpan.FromMinutes(5));
                client.DefaultRequestHeaders
                    .TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8"); // "Content-Type"
                try
                {
                    var result = client.GetAsync(url);
                    responseResult = result.Result.IsSuccessStatusCode;
                }
                catch (HttpRequestException ex)
                {
                    responseResult = false;
                }
            }
            sw.Stop();
            lock (synObject)
            {
                requestInfoList.Add(new Tuple<bool, int, int>(responseResult, (int)sw.ElapsedMilliseconds, Progress));
            }

            this.dispatcher.InvokeAsync(
               new Action(
                   () =>
                   {
                       Log.Add(
                                   time +
                                    " sec. index task " + index +
                                    " with result : " + responseResult +
                                    ", and time : " + sw.ElapsedMilliseconds + "ms.");
                   }));
        }

        private void AddGraphPoint()
        {
            lock (synObject)
            {
                var srq = this.requestInfoList.Count(i => (i.Item1)
                && (i.Item3 == Progress));
                var frq = this.requestInfoList.Count(i => (!i.Item1)
                && (i.Item3 == Progress));

                GraphPointsCollection.Add(new Tuple<int, int, int, int>(Progress, srq + frq,
                    frq, srq));
            }
        }

        private void Statistics()
        {
            lock (synObject)
            {
                this.requestInfoList.Sort((x, y) => x.Item2.CompareTo(y.Item2));

                Output = new Tuple<int, int, int, int, double>
                    (
                    this.requestInfoList.Count(),
                    this.requestInfoList.Count(i => !i.Item1),
                    this.requestInfoList.First().Item2,
                    this.requestInfoList.First().Item2,
                    this.requestInfoList.Average(i => i.Item2)
                    );
            }
        }

        private void Step(object sender, int time, int totalQuery, int amountOfThreads, string queryUrl, CancellationToken ct)
        {
            if (Progress == 0)
            {
                this.dispatcher.InvokeAsync(
                    new Action(
                        () =>
                        {
                            Log.Add("start: "
                                + DateTime.Now.ToString("HH:mm:ss"));
                        }));
            }

            if (Progress < time)
            {
                try
                {
                    Parallel.For(0, totalQuery,
                        new ParallelOptions
                        {
                            CancellationToken = ct,
                            MaxDegreeOfParallelism = amountOfThreads
                        }, (index) => SendRequest(queryUrl, index, Progress));

                    AddGraphPoint();
                }
                catch (OperationCanceledException ex)
                {

                    MessageBox.Show("cancel"); //is wrong
                    cancelTokenSource.Dispose();
                }
            }

            if ((Progress == time - 1) || cancelTokenSource.IsCancellationRequested)
            {
                ((DispatcherTimer)sender).Stop();
                this.dispatcher.InvokeAsync(
                    new Action(
                        () =>
                        {
                            Log.Add("test completed in " +
                                DateTime.Now.ToString("HH:mm:ss"));
                        }));
                GraphPointsCollection.Add(new Tuple<int, int, int, int>(time, 0, 0, 0));

                Progress = time;

                Statistics();

                TestCancel(1);

            }
            else
                Progress++;
        }

      private  void Run(TestInitDataModel testSettings)
        {
            Reset();
            OrdinateSize = 2 * testSettings.RequestsQuanity;

            DispatcherTimer lTimer = new DispatcherTimer(DispatcherPriority.Background,dispatcher);
            lTimer.Interval = new TimeSpan(0, 0, 1);
            lTimer.Tick += (sender, eventArgs) =>
            {
                Step(sender, testSettings.TestDuration.Seconds,
                    testSettings.RequestsQuanity, testSettings.Treads, testSettings.BaseUrl, cancelTokenSource.Token);
            };
            lTimer.Start();
        }

        private void Reset()
        {
            cancelTokenSource = new CancellationTokenSource();
            requestInfoList = new List<Tuple<bool, int, int>>();
            Progress = 0;
            Log = new ObservableCollection<string>();
            GraphPointsCollection = new ObservableCollection<Tuple<int, int, int, int>>();
            Output = new Tuple<int, int, int, int, double>(0, 0, 0, 0, 0);
        }

    }
}
