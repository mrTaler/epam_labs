﻿namespace Task04.MVVM.ViewModel
{
    using System.Security.Cryptography.X509Certificates;

    using Task04.MVVM.Model;

    /// <summary>
    /// The test init view model.
    /// </summary>
    public class TestInitViewModel
    {
        /// <summary>
        /// Gets or sets the test data init.
        /// </summary>
        public TestInitDataModel TestDataInit { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestInitViewModel"/> class.
        /// </summary>
        /// <param name="testData">
        /// The test Data.
        /// </param>
        public TestInitViewModel(TestInitDataModel testData)
        {
            this.TestDataInit = testData;
        }
    }
}
