﻿namespace TicketSaleCore.Services
{
    /// <summary>
    /// The message sender options for send SendGrid.
    /// </summary>
    public class AuthMessageSenderOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthMessageSenderOptions"/> class by default data.
        /// </summary>
        public AuthMessageSenderOptions()
        {
            this.SendGridUser = "mrtaler";
            this.SendGridKey = "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";
        }

        /// <summary>
        /// Gets or sets the send grid user.
        /// </summary>
        public string SendGridUser { get; set; }

        /// <summary>
        /// Gets or sets the send grid key.
        /// </summary>
        public string SendGridKey { get; set; }
    }
}
