﻿using System;
using System.Linq;
using System.Reflection;

using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TicketSaleCore.CustomTagHelper
{
    /// <summary>
    /// DisplayName for table
    /// </summary>
    [HtmlTargetElement("th")]
    public class DisplayNameTagHelper : TagHelper
    {
        public DisplayNameTagHelper(IModelMetadataProvider metadataProvider)
        {
            this.MetadataProvider = metadataProvider;
        }

        private const string DisplayForAttributeName = "display-for";
        [HtmlAttributeName(DisplayForAttributeName)]
        public string DisplayFor
        {
            get; set;
        }

        [ViewContext]
        public ViewContext ViewContext
        {
            get; set;
        }

        protected internal IModelMetadataProvider MetadataProvider
        {
            get; set;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if(context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if(output == null)
            {
                throw new ArgumentNullException(nameof(output));
            }

            var modelMetadata = this.ViewContext.ViewData.ModelMetadata;

            if(this.DisplayFor == null)
            {
                return;
            }

            foreach(var elementType in modelMetadata.ModelType.GetGenericArguments())
            {
                var props = this.MetadataProvider.GetMetadataForProperties(elementType);
                var chosen = props.SingleOrDefault(p => p.PropertyName.Equals(this.DisplayFor, StringComparison.OrdinalIgnoreCase));
                if(chosen != null)
                {
                    output.Content.SetContent(chosen.GetDisplayName());
                    return;
                }
            }
        }

    }
}
