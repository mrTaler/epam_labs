﻿using System.Linq;
using System.Security.Claims;

namespace TicketSaleCore
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetId(
            this ClaimsPrincipal user)
        {
           return user.Claims.FirstOrDefault(
                p => p.Type == ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
