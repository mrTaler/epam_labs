﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;
using TicketSaleCore.api;

namespace TicketSaleCore
{
    public class CsvOutputFormatter : OutputFormatter
    {
        private readonly CsvFormatterOptions _options;

        public string ContentType { get; private set; }

        public CsvOutputFormatter(CsvFormatterOptions csvFormatterOptions)
        {
            this.ContentType = "text/csv";
            this.SupportedMediaTypes.Add(new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("text/csv"));

            this._options = csvFormatterOptions ?? throw new ArgumentNullException(nameof(csvFormatterOptions));

        }

        protected override bool CanWriteType(Type type)
        {

            if (type == null)
                throw new ArgumentNullException("type");

            return this.isTypeOfIEnumerable(type);
        }

        private bool isTypeOfIEnumerable(Type type)
        {
            // foreach (Type interfaceType in type.GetInterfaces())
            // {

            // if (interfaceType == typeof(IEnumerable))
            return true;

            // }

            // return false;
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {
            var response = context.HttpContext.Response;

            Type type = context.Object.GetType();
            Type itemType;
            StringWriter _stringWriter = new StringWriter();
            if (type.GetGenericArguments().Length > 0)
            {
                if (this._options.UseSingleLineHeaderInCsv)
                {
                    var head = ((IEnumerable<ICsvExtend>)context.Object).FirstOrDefault().ConvertToCsv().Item1;
                    _stringWriter.WriteLine(head);
                }

                foreach (var obj in (IEnumerable<ICsvExtend>)context.Object)
                {
                    _stringWriter.WriteLine(obj.ConvertToCsv().Item2);
                }
            }
            else
            {
                if (this._options.UseSingleLineHeaderInCsv)
                {
                    var head = ((ICsvExtend)context.Object).ConvertToCsv().Item1;
                    _stringWriter.WriteLine(head);
                }

                var boby = (ICsvExtend) context.Object;
                _stringWriter.WriteLine(boby.ConvertToCsv().Item2);
            }




            var streamWriter = new StreamWriter(response.Body);
            await streamWriter.WriteAsync(_stringWriter.ToString());
            await streamWriter.FlushAsync();

        }
    }
  
    public class CsvFormatterOptions
    {
        public bool UseSingleLineHeaderInCsv { get; set; } = true;

        public string CsvDelimiter { get; set; } = ";";
    }
}

