﻿using System;

using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketSaleCore.Migrations
{
    public partial class dateTimeOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateTimeOrderCreate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateTimeOrderCreate",
                table: "Orders");
        }
    }
}
