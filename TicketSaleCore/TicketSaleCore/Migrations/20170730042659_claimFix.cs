﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketSaleCore.Migrations
{
    public partial class claimFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClaimOwner",
                table: "AspNetUserClaims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ValueType",
                table: "AspNetUserClaims",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClaimOwner",
                table: "AspNetUserClaims");

            migrationBuilder.DropColumn(
                name: "ValueType",
                table: "AspNetUserClaims");
        }
    }
}
