﻿using System;

using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketSaleCore.Migrations
{
    public partial class NewTableForReject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Orders_OrderId",
                table: "Tickets");

            migrationBuilder.DropIndex(
                name: "IX_Tickets_OrderId",
                table: "Tickets");

            migrationBuilder.CreateTable(
                name: "OrderingTickets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OrderId = table.Column<int>(nullable: true),
                    TicketStatusId = table.Column<int>(nullable: true),
                    TiketId = table.Column<int>(nullable: true),
                    dateAdded = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderingTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderingTickets_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderingTickets_OrderStatuses_TicketStatusId",
                        column: x => x.TicketStatusId,
                        principalTable: "OrderStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderingTickets_Tickets_TiketId",
                        column: x => x.TiketId,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderingTickets_OrderId",
                table: "OrderingTickets",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderingTickets_TicketStatusId",
                table: "OrderingTickets",
                column: "TicketStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderingTickets_TiketId",
                table: "OrderingTickets",
                column: "TiketId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderingTickets");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_OrderId",
                table: "Tickets",
                column: "OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Orders_OrderId",
                table: "Tickets",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
