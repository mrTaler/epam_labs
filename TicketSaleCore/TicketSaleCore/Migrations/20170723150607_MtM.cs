﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketSaleCore.Migrations
{
    public partial class MtM : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_OrderCart_OrderingCartId",
                table: "Tickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Venues_Citys_CityFk",
                table: "Venues");

            migrationBuilder.DropIndex(
                name: "IX_Tickets_OrderingCartId",
                table: "Tickets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Citys",
                table: "Citys");

            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "Tickets");

            migrationBuilder.DropColumn(
                name: "OrderingCartId",
                table: "Tickets");

            migrationBuilder.RenameTable(
                name: "Citys",
                newName: "Cites");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cites",
                table: "Cites",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "OrderCartTicket",
                columns: table => new
                {
                    TicketId = table.Column<int>(nullable: false),
                    OrderingCartId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderCartTicket", x => new { x.TicketId, x.OrderingCartId });
                    table.ForeignKey(
                        name: "FK_OrderCartTicket_OrderCart_OrderingCartId",
                        column: x => x.OrderingCartId,
                        principalTable: "OrderCart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderCartTicket_Tickets_TicketId",
                        column: x => x.TicketId,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderCartTicket_OrderingCartId",
                table: "OrderCartTicket",
                column: "OrderingCartId");

            migrationBuilder.AddForeignKey(
                name: "FK_Venues_Cites_CityFk",
                table: "Venues",
                column: "CityFk",
                principalTable: "Cites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Venues_Cites_CityFk",
                table: "Venues");

            migrationBuilder.DropTable(
                name: "OrderCartTicket");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cites",
                table: "Cites");

            migrationBuilder.RenameTable(
                name: "Cites",
                newName: "Citys");

            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "Tickets",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrderingCartId",
                table: "Tickets",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Citys",
                table: "Citys",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_OrderingCartId",
                table: "Tickets",
                column: "OrderingCartId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_OrderCart_OrderingCartId",
                table: "Tickets",
                column: "OrderingCartId",
                principalTable: "OrderCart",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Venues_Citys_CityFk",
                table: "Venues",
                column: "CityFk",
                principalTable: "Citys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
