﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TicketSaleCore.Models.DAL._Ef;

namespace TicketSaleCore.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20170730042659_claimFix")]
    partial class claimFix
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationRoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationUserLogin", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationUserRole", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationUserToken", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.AppUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("Address");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.Property<int>("Year");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.AppUserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimOwner");

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.Property<string>("ValueType");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.City", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Cites");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<int>("EventsTypeId");

                    b.Property<string>("Name");

                    b.Property<int>("VenueId");

                    b.HasKey("Id");

                    b.HasIndex("EventsTypeId");

                    b.HasIndex("VenueId");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.EventsType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("NameEventsType");

                    b.HasKey("Id");

                    b.ToTable("EventsTypes");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuyerId");

                    b.Property<DateTime>("DateTimeOrderCreate");

                    b.Property<int?>("StatusId");

                    b.Property<string>("TrackNo");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("StatusId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderCartTicket", b =>
                {
                    b.Property<int?>("TicketId");

                    b.Property<int?>("OrderingCartId");

                    b.HasKey("TicketId", "OrderingCartId");

                    b.HasIndex("OrderingCartId");

                    b.ToTable("OrderCartTicket");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderingCart", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Count");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime>("DateLastUpdate");

                    b.Property<Guid>("OrderId");

                    b.Property<string>("OwnerId");

                    b.HasKey("Id");

                    b.HasIndex("OwnerId")
                        .IsUnique();

                    b.ToTable("OrderCart");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderingTicket", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("OrderId");

                    b.Property<int?>("TicketStatusId");

                    b.Property<int?>("TiketId");

                    b.Property<DateTime>("dateAdded");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("TicketStatusId");

                    b.HasIndex("TiketId");

                    b.ToTable("OrderingTickets");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("StatusName");

                    b.HasKey("Id");

                    b.ToTable("OrderStatuses");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("EventId");

                    b.Property<decimal>("Price");

                    b.Property<string>("SellerId");

                    b.Property<string>("SellerNotes");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("SellerId");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Venue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int>("CityFk");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CityFk");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationRoleClaim", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationUserLogin", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.AppUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.ApplicationUserRole", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.ApplicationRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketSaleCore.Models.Entities.AppUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.AppUserClaim", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.AppUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Event", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.EventsType", "EventsType")
                        .WithMany("Events")
                        .HasForeignKey("EventsTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketSaleCore.Models.Entities.Venue", "Venue")
                        .WithMany("Events")
                        .HasForeignKey("VenueId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Order", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.AppUser", "Buyer")
                        .WithMany("Orders")
                        .HasForeignKey("BuyerId");

                    b.HasOne("TicketSaleCore.Models.Entities.OrderStatus", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderCartTicket", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.OrderingCart", "OrderingCart")
                        .WithMany("OrderCartTickets")
                        .HasForeignKey("OrderingCartId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketSaleCore.Models.Entities.Ticket", "Ticket")
                        .WithMany("OrderCartTickets")
                        .HasForeignKey("TicketId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderingCart", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.AppUser", "CartOwner")
                        .WithOne("OrderCart")
                        .HasForeignKey("TicketSaleCore.Models.Entities.OrderingCart", "OwnerId");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.OrderingTicket", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.Order", "Order")
                        .WithMany("OrderTickets")
                        .HasForeignKey("OrderId");

                    b.HasOne("TicketSaleCore.Models.Entities.OrderStatus", "TicketStatus")
                        .WithMany("OrderTickets")
                        .HasForeignKey("TicketStatusId");

                    b.HasOne("TicketSaleCore.Models.Entities.Ticket", "Tiket")
                        .WithMany("OrderTickets")
                        .HasForeignKey("TiketId");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Ticket", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.Event", "Event")
                        .WithMany("Tickets")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketSaleCore.Models.Entities.AppUser", "Seller")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerId");
                });

            modelBuilder.Entity("TicketSaleCore.Models.Entities.Venue", b =>
                {
                    b.HasOne("TicketSaleCore.Models.Entities.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityFk")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
