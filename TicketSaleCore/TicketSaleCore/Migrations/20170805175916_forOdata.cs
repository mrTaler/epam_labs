﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketSaleCore.Migrations
{
    public partial class forOdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderCartTicket_OrderCart_OrderingCartId",
                table: "OrderCartTicket");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderCartTicket_Tickets_TicketId",
                table: "OrderCartTicket");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderCartTicket",
                table: "OrderCartTicket");

            migrationBuilder.AlterColumn<int>(
                name: "OrderingCartId",
                table: "OrderCartTicket",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TicketId",
                table: "OrderCartTicket",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "OrderCartTicket",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderCartTicket",
                table: "OrderCartTicket",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_OrderCartTicket_TicketId",
                table: "OrderCartTicket",
                column: "TicketId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderCartTicket_OrderCart_OrderingCartId",
                table: "OrderCartTicket",
                column: "OrderingCartId",
                principalTable: "OrderCart",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderCartTicket_Tickets_TicketId",
                table: "OrderCartTicket",
                column: "TicketId",
                principalTable: "Tickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderCartTicket_OrderCart_OrderingCartId",
                table: "OrderCartTicket");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderCartTicket_Tickets_TicketId",
                table: "OrderCartTicket");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderCartTicket",
                table: "OrderCartTicket");

            migrationBuilder.DropIndex(
                name: "IX_OrderCartTicket_TicketId",
                table: "OrderCartTicket");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "OrderCartTicket");

            migrationBuilder.AlterColumn<int>(
                name: "TicketId",
                table: "OrderCartTicket",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrderingCartId",
                table: "OrderCartTicket",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderCartTicket",
                table: "OrderCartTicket",
                columns: new[] { "TicketId", "OrderingCartId" });

            migrationBuilder.AddForeignKey(
                name: "FK_OrderCartTicket_OrderCart_OrderingCartId",
                table: "OrderCartTicket",
                column: "OrderingCartId",
                principalTable: "OrderCart",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderCartTicket_Tickets_TicketId",
                table: "OrderCartTicket",
                column: "TicketId",
                principalTable: "Tickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
