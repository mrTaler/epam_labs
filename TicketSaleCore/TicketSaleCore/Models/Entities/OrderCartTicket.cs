﻿namespace TicketSaleCore.Models.Entities
{
    public class OrderCartTicket
    {
        public int Id { get; set; }
        public Ticket Tickett { get; set; }
        public int? TicketId { get; set; }

        public int? OrderingCartId { get; set; }
        public OrderingCart OrderingCart { get; set; }
    }
}
