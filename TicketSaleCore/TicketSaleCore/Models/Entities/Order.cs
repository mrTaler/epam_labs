﻿using System.Collections.Generic;

namespace TicketSaleCore.Models.Entities
{
    using System;

    public class Order
    {
        public Order()
        {
            this.OrderTickets=new HashSet<OrderingTicket>();
            this.DateTimeOrderCreate=DateTime.UtcNow;
        }


        public int Id { get; set; }

        public virtual OrderStatus Status { get; set; }

        public string TrackNo { get; set; }

        public string BuyerId { get; set; }
        public virtual AppUser Buyer { get; set; }

        public ICollection<OrderingTicket> OrderTickets { get; set; }

        public DateTime DateTimeOrderCreate { get; set; }
    }

    // public class OrderConfiguration : EntityTypeConfiguration<Order>
    // {
    // public OrderConfiguration()
    // {
    // this.HasKey(t => t.Id);

    // this.HasRequired<User>(t => t.Buyer)
    // .WithMany(t => t.Orders)
    // .HasForeignKey(t => t.BuyerId);


    // }
    // }
}
