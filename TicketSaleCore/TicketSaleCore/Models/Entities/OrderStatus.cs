﻿
namespace TicketSaleCore.Models.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// The order status.
    /// </summary>
    public class OrderStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderStatus"/> class.
        /// </summary>
        public OrderStatus()
        {
            this.OrderTickets = new HashSet<OrderingTicket>();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the status name.
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// Gets or sets the orders.
        /// </summary>
        public virtual ICollection<OrderingTicket> OrderTickets { get; set; }
    }

    // public class StatusConfiguration : EntityTypeConfiguration<Status>
    // {
    // public StatusConfiguration()
    // {
    // this.HasKey(t => t.Id);

    // this.HasMany<Order>(t => t.Orders)
    // .WithRequired(t => t.Status);
    // }
    // }
}
