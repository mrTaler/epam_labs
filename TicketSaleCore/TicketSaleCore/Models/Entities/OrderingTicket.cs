﻿using System;

namespace TicketSaleCore.Models.Entities
{
    public class OrderingTicket
    {
        public Guid Id { get; set; }

        public Ticket Tiket { get; set; }
        public int? TiketId { get; set; }

        public Order Order { get; set; }
        public int? OrderId { get; set; }

        public OrderStatus TicketStatus { get; set; }
        public int? TicketStatusId { get; set; }

        public DateTime dateAdded { get; set; }
    }
}
