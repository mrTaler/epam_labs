﻿namespace TicketSaleCore.Models.Entities
{
    using System.Security.Claims;

    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    /// <summary>
    /// The app user claim.
    /// </summary>
    public class AppUserClaim : IdentityUserClaim<string>
    {
        public virtual string ClaimOwner { get; set; }

        public virtual string ValueType { get; set; }

        public override Claim ToClaim()
        {
            var claim = new Claim(this.ClaimType, this.ClaimValue, this.ValueType, this.ClaimOwner);
            
            return claim;
        }

        /// <summary>Reads the type and value from the Claim.</summary>
        /// <param name="claim"></param>
        public override void InitializeFromClaim(Claim claim)
        {
            this.ClaimType = claim.Type;
            this.ClaimValue = claim.Value;
            this.ValueType = claim.ValueType;
            this.ClaimOwner = claim.Issuer;
        }
    }
}
