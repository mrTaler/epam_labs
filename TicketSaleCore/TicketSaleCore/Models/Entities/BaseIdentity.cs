﻿namespace TicketSaleCore.Models.Entities
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    public class ApplicationUserLogin : IdentityUserLogin<string>
    {
    }

    public class ApplicationUserRole : IdentityUserRole<string>
    {
    }

    public class ApplicationUserToken : IdentityUserToken<string>
    {
    }

    public class ApplicationRoleClaim : IdentityRoleClaim<string>
    {
    }

}
