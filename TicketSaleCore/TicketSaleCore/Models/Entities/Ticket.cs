﻿
using System.Collections.Generic;

namespace TicketSaleCore.Models.Entities
{
    public class Ticket
    {
        public Ticket()
        {
            this.OrderTickets = new HashSet<OrderingTicket>();
            this.OrderCartTickets = new HashSet<OrderCartTicket>();
        }

        public int Id { get; set; }

        public decimal Price { get; set; }

        public string SellerNotes { get; set; }

        public ICollection<OrderingTicket> OrderTickets { get; set; }

        public string SellerId { get; set; }

        public AppUser Seller { get; set; }

        public int EventId { get; set; }

        public virtual Event Event { get; set; }

        public virtual ICollection<OrderCartTicket> OrderCartTickets { get; set; }
    }

    // public class TicketConfiguration : EntityTypeConfiguration<Ticket>
    // {
    // public TicketConfiguration()
    // {
    // this.HasKey(t => t.Id);

    // this.HasRequired<Event>(t => t.Event)
    // .WithMany(t => t.Tickets)
    // .HasForeignKey(t => t.EventId);

    // this.HasRequired<User>(t => t.Seller)
    // .WithMany(t => t.Tickets)
    // .HasForeignKey(t => t.SellerId);

    // this.HasOptional(t => t.Order)
    // .WithRequired(tt => tt.Ticket);
    // }
    // }
}
