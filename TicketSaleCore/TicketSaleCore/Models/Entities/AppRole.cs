﻿namespace TicketSaleCore.Models.Entities
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    public class ApplicationRole : IdentityRole<string,
        ApplicationUserRole,
        ApplicationRoleClaim>
    {
        public ApplicationRole()
            : base()
        {
        }

        public ApplicationRole(string roleName)
            : base(roleName)
        {
        }
    }
}
