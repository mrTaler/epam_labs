﻿using System;
using System.Collections.Generic;

namespace TicketSaleCore.Models.Entities
{
    public class OrderingCart
    {
        public OrderingCart()
        {
            this.OrderId = Guid.NewGuid();
            this.OrderCartTickets = new HashSet<OrderCartTicket>();
            this.DateCreated = DateTime.UtcNow;
        }

        public int Id { get; set; }
        public Guid OrderId { get; set; }

        public int Count { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdate { get; set; }

        // owner
        public string OwnerId { get; set; }
        public AppUser CartOwner { get; set; }

        // Tickets
        public virtual ICollection<OrderCartTicket> OrderCartTickets { get; set; }
    }

}
