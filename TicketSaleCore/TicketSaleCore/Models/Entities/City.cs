﻿namespace TicketSaleCore.Models.Entities
{
    using System.Collections.Generic;

    public class City
    {
        public City()
        {
            this.Venues = new HashSet<Venue>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Venue> Venues { get; set; }
    }

    // public class CityConfiguration : EntityTypeConfiguration<City>
    // {
    // public override void Map(EntityTypeBuilder<City> builder)
    // {
    // builder.HasKey(t => t.Id);
    // }
    // }
}
