﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The city include.
    /// </summary>
    public class OrderingCartInclude : IIncludeSpecification<OrderingCart>
    {
        public OrderingCartInclude()
        {
            this.AddInclude(p => p.Include(x => x.OrderCartTickets));

            this.AddInclude(p => p.Include(x => x.CartOwner));
            this.AddInclude(p => p.Include(x => x.OrderCartTickets).ThenInclude(x => x.Tickett).ThenInclude(x => x.Seller));
            this.AddInclude(p => p.Include(x => x.OrderCartTickets).ThenInclude(x => x.Tickett).ThenInclude(x => x.Event));
        }

        public List<Func<IQueryable<OrderingCart>, IQueryable<OrderingCart>>> Includes { get; } =
            new List<Func<IQueryable<OrderingCart>, IQueryable<OrderingCart>>>();

        public void AddInclude(Func<IQueryable<OrderingCart>, IQueryable<OrderingCart>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
