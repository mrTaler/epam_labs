﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    public class VenueInclude : IIncludeSpecification<Venue>
    {
        public VenueInclude()
        {
            this.AddInclude(p => p.Include(x => x.City));
            this.AddInclude(p => p.Include(x => x.Events).ThenInclude(m => m.Tickets));
        }

        public List<Func<IQueryable<Venue>, IQueryable<Venue>>> Includes { get; } = new List<Func<IQueryable<Venue>, IQueryable<Venue>>>();

        public void AddInclude(Func<IQueryable<Venue>, IQueryable<Venue>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
