﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The order include.
    /// </summary>
    public class OrderInclude : IIncludeSpecification<Order>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderInclude"/> class.
        /// </summary>
        public OrderInclude()
        {
            this.AddInclude(p => p.Include(x => x.Buyer));
            this.AddInclude(p => p.Include(x => x.OrderTickets));
            this.AddInclude(p => p.Include(x => x.Status));

            this.AddInclude(p => p.Include(x => x.OrderTickets).ThenInclude(z => z.TicketStatus));

            this.AddInclude(p => p.Include(x => x.OrderTickets).ThenInclude(z => z.Tiket).ThenInclude(z => z.Event));
            this.AddInclude(p => p.Include(x => x.OrderTickets).ThenInclude(z => z.Tiket).ThenInclude(z => z.Seller));
        }

        /// <summary>
        /// Gets the order includes.
        /// </summary>
        public List<Func<IQueryable<Order>, IQueryable<Order>>> Includes { get; } =
            new List<Func<IQueryable<Order>, IQueryable<Order>>>();

        /// <summary>
        /// The method to add order include.
        /// </summary>
        /// <param name="includeExpression">
        /// The include expression.
        /// </param>
        public void AddInclude(Func<IQueryable<Order>, IQueryable<Order>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
