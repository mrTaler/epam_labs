﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    public class TicketsInclude : IIncludeSpecification<Ticket>
    {
        public TicketsInclude()
        {
            this.AddInclude(p => p.Include(x => x.OrderTickets));
            this.AddInclude(p => p.Include(x => x.Event));
            this.AddInclude(p => p.Include(x => x.Seller));
            this.AddInclude(p => p.Include(x => x.OrderCartTickets));

            this.AddInclude(p => p.Include(x => x.OrderTickets).ThenInclude(m => m.TicketStatus));
            this.AddInclude(p => p.Include(x => x.Event).ThenInclude(m => m.Venue).ThenInclude(z => z.City));
            this.AddInclude(p => p.Include(x => x.OrderTickets).ThenInclude(m => m.Order).ThenInclude(z => z.Buyer));
        }

        public List<Func<IQueryable<Ticket>, IQueryable<Ticket>>> Includes { get; } 
            = new List<Func<IQueryable<Ticket>, IQueryable<Ticket>>>();

        public void AddInclude(Func<IQueryable<Ticket>, IQueryable<Ticket>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
