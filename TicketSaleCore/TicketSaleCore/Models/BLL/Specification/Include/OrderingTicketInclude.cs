namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    public class OrderingTicketInclude : IIncludeSpecification<OrderingTicket>
    {
        public OrderingTicketInclude()
        {
            this.AddInclude(p => p.Include(x => x.TicketStatus));
            this.AddInclude(p => p.Include(x => x.Tiket));
            this.AddInclude(p => p.Include(x => x.Order));

            this.AddInclude(p => p.Include(x => x.Tiket).ThenInclude(m => m.Event));
            this.AddInclude(p => p.Include(x => x.Tiket).ThenInclude(m => m.Seller));
            this.AddInclude(p => p.Include(x => x.Order).ThenInclude(m => m.Buyer));

        }

        public List<Func<IQueryable<OrderingTicket>, IQueryable<OrderingTicket>>> Includes { get; } = 
            new List<Func<IQueryable<OrderingTicket>, IQueryable<OrderingTicket>>>();

        public void AddInclude(Func<IQueryable<OrderingTicket>, IQueryable<OrderingTicket>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
