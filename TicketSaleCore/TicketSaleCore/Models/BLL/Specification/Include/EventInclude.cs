﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event include.
    /// </summary>
    public class EventInclude : IIncludeSpecification<Event>
    {
        public EventInclude()
        {
            this.AddInclude(p => p.Include(x => x.Venue));
            this.AddInclude(p => p.Include(x => x.EventsType));
            this.AddInclude(p => p.Include(x => x.Tickets));
            this.AddInclude(p => p.Include(x => x.Venue).ThenInclude(z => z.City));
            this.AddInclude(p => p.Include(x => x.Tickets).ThenInclude(z => z.Seller));
            this.AddInclude(p => p.Include(x => x.Tickets).ThenInclude(z => z.OrderTickets));
            this.AddInclude(p => p.Include(x => x.Tickets).ThenInclude(z => z.OrderTickets).ThenInclude(z => z.Order));
        }

        public List<Func<IQueryable<Event>, IQueryable<Event>>> Includes { get; } =
           new List<Func<IQueryable<Event>, IQueryable<Event>>>();

        public void AddInclude(Func<IQueryable<Event>, IQueryable<Event>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
