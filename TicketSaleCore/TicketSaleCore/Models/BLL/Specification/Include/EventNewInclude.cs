﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event include.
    /// </summary>
    public class EventNewInclude : IIncludeSpecification<Event>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventNewInclude"/> class.
        /// </summary>
        public EventNewInclude()
        {
            this.AddInclude(p => p.Include(x => x.Venue)
                .Include(x => x.Venue)
                .ThenInclude(z => z.City)
                .Include(x => x.EventsType)
                .Include(x => x.Tickets));

            // AddInclude(p => p.Include(x => x.EventsType));
            // AddInclude(p => p.Include(x => x.Tickets));
            // AddInclude(p => p.Include(x => x.Venue).ThenInclude(z => z.City));
        }

        /// <summary>
        /// Gets the includes.
        /// </summary>
        public List<Func<IQueryable<Event>, IQueryable<Event>>> Includes { get; } =
           new List<Func<IQueryable<Event>, IQueryable<Event>>>();

        /// <summary>
        /// The add include.
        /// </summary>
        /// <param name="includeExpression">
        /// The include expression.
        /// </param>
        public void AddInclude(Func<IQueryable<Event>, IQueryable<Event>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
