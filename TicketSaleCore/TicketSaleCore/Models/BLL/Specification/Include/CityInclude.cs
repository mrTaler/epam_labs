﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The city include.
    /// </summary>
    public class CityInclude : IIncludeSpecification<City>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CityInclude"/> class.
        /// </summary>
        public CityInclude()
        {
            this.AddInclude(p => p.Include(x => x.Venues));
        }

        /// <summary>
        /// Gets the includes.
        /// </summary>
        public List<Func<IQueryable<City>, IQueryable<City>>> Includes { get; } =
            new List<Func<IQueryable<City>, IQueryable<City>>>();

        /// <summary>
        /// The add include.
        /// </summary>
        /// <param name="includeExpression">
        /// The include expression.
        /// </param>
        public void AddInclude(Func<IQueryable<City>, IQueryable<City>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
