﻿namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    public class OrderStatusInclude : IIncludeSpecification<OrderStatus>
    {
        public OrderStatusInclude()
        {
            this.AddInclude(p => p.Include(x => x.OrderTickets));
        }

        public List<Func<IQueryable<OrderStatus>, IQueryable<OrderStatus>>> Includes { get; } = 
            new List<Func<IQueryable<OrderStatus>, IQueryable<OrderStatus>>>();

        public void AddInclude(Func<IQueryable<OrderStatus>, IQueryable<OrderStatus>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
