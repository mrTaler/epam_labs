namespace TicketSaleCore.Models.BLL.Specification.Include
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The city include.
    /// </summary>
    public class EventsTypeInclude : IIncludeSpecification<EventsType>
    {
        public EventsTypeInclude()
        {
            this.AddInclude(p => p.Include(x => x.Events));
        }

        public List<Func<IQueryable<EventsType>, IQueryable<EventsType>>> Includes { get; } =
            new List<Func<IQueryable<EventsType>, IQueryable<EventsType>>>();

        public void AddInclude(Func<IQueryable<EventsType>, IQueryable<EventsType>> includeExpression)
        {
            this.Includes.Add(includeExpression);
        }
    }
}
