namespace TicketSaleCore.Models.BLL.Specification.OrderBy
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.OrderSpecification;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The data order specification.
    /// </summary>
    public class SortEventByDataOrderSpecification : OrderSpecification<Event>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SortEventByDataOrderSpecification"/> class.
        /// </summary>
        /// <param name="order">
        /// The order.
        /// </param>
        public SortEventByDataOrderSpecification(OrderEn order = OrderEn.Ascending)
            : base(order)
        {
        }

        /// <summary>
        /// The as expression.
        /// </summary>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public override Expression<Func<Event, IComparable>> AsExpression()
        {
            return entity => entity.Date;
        }
    }
}