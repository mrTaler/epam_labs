namespace TicketSaleCore.Models.BLL.Specification.Filter.VenueSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    public class FindVenueByNameOrId : Specification<Venue>
    {
        private string name;
        private int? id;

        public FindVenueByNameOrId(string name)
        {
            this.name = name;
        }

        public FindVenueByNameOrId(int? id)
        {
            this.id = id;
        }
        
        public override Expression<Func<Venue, bool>> AsExpression()
        {
            return p => (this.id.HasValue && p.Id == this.id)
                        || (this.name != null && p.Name == this.name);
        }
    }
}
