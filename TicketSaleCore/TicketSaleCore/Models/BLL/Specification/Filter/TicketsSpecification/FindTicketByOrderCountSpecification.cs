namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The ticket order count spec.
    /// </summary>
    public class FindTicketByOrderCountSpecification : Specification<Ticket>
    {
        /// <summary>
        /// The count.
        /// </summary>
        private readonly int count;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindTicketByOrderCountSpecification"/> class.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        public FindTicketByOrderCountSpecification(int count = 0)
        {
            this.count = count;
        }

        /// <summary>
        /// The as expression.
        /// </summary>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            return c => c.OrderTickets.Count == this.count;
        }
    }
}