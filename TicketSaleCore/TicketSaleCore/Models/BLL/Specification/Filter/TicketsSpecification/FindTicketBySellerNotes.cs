﻿namespace TicketSaleCore.Models.BLL.Specification.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;
    using DAL.Specifications.Interfaces;
    using Entities;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The find ticket by seller notes.
    /// </summary>
    public class FindTicketBySellerNotes : Specification<Ticket>
    {
        private string name;

        public FindTicketBySellerNotes(string name)
        {
            this.name = name;
        }

        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            return p => p.SellerNotes == this.name;
        }
    }
}
