﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;
    using System.Security.Claims;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The ticket by seller spec.
    /// </summary>
    public class FindTicketBySellerSpecification : Specification<Ticket>
    {
        /// <summary>
        /// The user.
        /// </summary>
        private ClaimsPrincipal user;

        /// <summary>
        /// The user id.
        /// </summary>
        private string userId;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindTicketBySellerSpecification"/> class.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        public FindTicketBySellerSpecification(ClaimsPrincipal user)
        {
            this.user = user;
        }

        public FindTicketBySellerSpecification(string userId)
        {
            this.userId = userId;
        }


        /// <summary>
        /// The as expression.
        /// </summary>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            if (this.user.GetId() != null)
            {
                return z => z.SellerId != this.user.GetId();
            }
            if (this.userId != null)
            {
                return z => z.SellerId != this.userId;
            }
            return z => false;


        }
    }
}