﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The ticket by status not.
    /// </summary>
    public class FindTicketByStatusSpecification : Specification<Ticket>
    {
        /// <summary>
        /// The status name.
        /// </summary>
        private readonly string statusName;

        private readonly TicketStatusLog log;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindTicketByStatusSpecification"/> class.
        /// </summary>
        /// <param name="statusName">
        /// The status name. default "Confirmed"
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        public FindTicketByStatusSpecification(TicketStatusLog log, string statusName = "Confirmed")
        {
            this.statusName = statusName;
            this.log = log;
        }

        /// <summary>
        /// The as expression.
        /// </summary>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            switch (this.log)
            {
                case TicketStatusLog.Equal:
                    return p => p.OrderTickets.Any(z => z.TicketStatus.StatusName == this.statusName);
                case TicketStatusLog.NotEqual:
                    return c => c.OrderTickets.Any(d => d.TicketStatus.StatusName != this.statusName);
                default: return null;
            }

        }
    }
}