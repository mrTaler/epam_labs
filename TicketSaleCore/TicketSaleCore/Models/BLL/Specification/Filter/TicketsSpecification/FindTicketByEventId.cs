﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The find ticket by event id.
    /// </summary>
    public class FindTicketByEventId : Specification<Ticket>
    {
        private readonly int id;

        public FindTicketByEventId(int id)
        {
            this.id = id;
        }

        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            return p => p.EventId == this.id;
        }
    }
}
