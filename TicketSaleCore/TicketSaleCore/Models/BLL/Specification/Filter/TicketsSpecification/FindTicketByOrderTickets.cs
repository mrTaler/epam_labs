﻿namespace TicketSaleCore.Models.BLL.Specification.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using DAL.Specifications.Interfaces;
    using Entities;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The find ticket by order tickets.
    /// </summary>
    public class FindTicketByOrderTickets : Specification<Ticket>
    {
        private OrderingTicket OrderTickets;

        public FindTicketByOrderTickets(OrderingTicket OrderTickets)
        {
            this.OrderTickets = OrderTickets;
        }

        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            return p => p.OrderTickets == this.OrderTickets;
        }
    }
}
