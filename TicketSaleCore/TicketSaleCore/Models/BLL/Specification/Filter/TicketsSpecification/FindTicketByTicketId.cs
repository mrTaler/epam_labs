﻿namespace TicketSaleCore.Models.BLL.Specification.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using DAL.Specifications.Interfaces;
    using Entities;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The find ticket by ticket id.
    /// </summary>
    public class FindTicketByTicketId : Specification<Ticket>
    {
        private int id;

        public FindTicketByTicketId(int id)
        {
            this.id = id;
        }
    
        public override Expression<Func<Ticket, bool>> AsExpression()
        {
            return p => p.Id == this.id;
        }
    }
}
