namespace TicketSaleCore.Models.BLL.Specification.Filter.CitySpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event find by id.
    /// </summary>
    public class FindCityByCityId : Specification<City>
    {
        /// <summary>
        /// The id.
        /// </summary>
        private readonly int? id;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindCityByCityId"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public FindCityByCityId(int? id)
        {
            this.id = id;
        }
        
        public override Expression<Func<City, bool>> AsExpression()
        {
            return p => p.Id == this.id;
        }
    }
}
