﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.CitySpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event find by name.
    /// </summary>
    public class FindCityByCityName : Specification<City>
    {
        /// <summary>
        /// The name.
        /// </summary>
        private readonly string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindCityByCityName"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public FindCityByCityName(string name)
        {
            this.name = name;
        }

        public override Expression<Func<City, bool>> AsExpression()
        {
            return p => p.Name == this.name;
        }
    }
}
