﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.OrderStatusSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The find order status by status name.
    /// </summary>
    public class FindOrderStatusByStatusName : Specification<OrderStatus> 
    {
        private string statusName;

        public FindOrderStatusByStatusName(string statusName)
        {
            this.statusName = statusName;
        }
    
        public override Expression<Func<OrderStatus, bool>> AsExpression()
        {
            return m => m.StatusName == this.statusName;
        }
    }
}
