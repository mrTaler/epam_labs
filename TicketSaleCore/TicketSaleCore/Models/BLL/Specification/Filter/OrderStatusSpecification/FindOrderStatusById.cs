﻿namespace TicketSaleCore.Models.BLL.Specification.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using DAL.Specifications.Interfaces;
    using Entities;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The find order status by id.
    /// </summary>
    public class FindOrderStatusById : Specification<OrderStatus>
    {
        private int? id;

        public FindOrderStatusById(int? id)
        {
            this.id = id;
        }
    
        public override Expression<Func<OrderStatus, bool>> AsExpression()
        {
            return p => p.Id == this.id;
        }
    }
}
