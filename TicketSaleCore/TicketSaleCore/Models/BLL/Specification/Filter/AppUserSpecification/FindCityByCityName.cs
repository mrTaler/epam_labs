﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.CitySpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event find by name.
    /// </summary>
    public class FindUserByEmail : Specification<AppUser>
    {
        /// <summary>
        /// The name.
        /// </summary>
        private readonly string email;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindCityByCityName"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public FindUserByEmail(string email)
        {
            this.email = email;
        }

        public override Expression<Func<AppUser, bool>> AsExpression()
        {
            return p => p.Email.Equals(this.email);
        }
    }
}
