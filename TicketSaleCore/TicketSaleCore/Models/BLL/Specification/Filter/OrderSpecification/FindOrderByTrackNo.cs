﻿namespace TicketSaleCore.Models.BLL.Specification.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using DAL.Specifications.Interfaces;
    using Entities;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The find ticket by ticket id.
    /// </summary>
    public class FindOrderByTrackNo : Specification<Order>
    {
        private string trackNoId;

        public FindOrderByTrackNo(string trackNoId)
        {
            this.trackNoId = trackNoId;
        }
    
        public override Expression<Func<Order, bool>> AsExpression()
        {
            return p => p.TrackNo == this.trackNoId;
        }
    }
}
