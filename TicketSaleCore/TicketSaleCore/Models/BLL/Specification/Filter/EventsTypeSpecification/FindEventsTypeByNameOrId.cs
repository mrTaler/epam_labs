﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.EventsTypeSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    public class FindEventsTypeByNameOrId : Specification<EventsType>
    {
        private string name;
        private int? id;

        public FindEventsTypeByNameOrId(string name)
        {
            this.name = name;
        }

        public FindEventsTypeByNameOrId(int? id)
        {
            this.id = id;
        }
        
        public override Expression<Func<EventsType, bool>> AsExpression()
        {
            return p => (this.id.HasValue && p.Id == this.id)
                        || (this.name != null && p.NameEventsType == this.name);
        }
    }
}
