﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.EventSpecification
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event find by name.
    /// </summary>
    public class FindEventsWithFreeTickets : Specification<Event>
    {
        public override Expression<Func<Event, bool>> AsExpression()
        {
            return t => t.Tickets.Count(
                c => c.OrderTickets == null) > 0;
        }
    }
}
