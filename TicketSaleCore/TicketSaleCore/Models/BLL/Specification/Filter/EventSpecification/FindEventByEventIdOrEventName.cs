﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.EventSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event find by id.
    /// </summary>
    public class FindEventByEventIdOrEventName : Specification<Event>
    {
        private int? id;
        private string name;

        public FindEventByEventIdOrEventName(int? id)
        {
            this.id = id;
        }
        public FindEventByEventIdOrEventName(string name)
        {
            this.name = name;
        }

        public override Expression<Func<Event, bool>> AsExpression()
        {
            return p => (this.id.HasValue && p.Id == this.id)
                        || (this.name != null && p.Name == this.name);
        }
    }
}
