﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.OrderingCartSpecification
{
    using System;
    using System.Linq.Expressions;
    using System.Security.Claims;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    using static FindMode;

    /// <summary>
    /// The event find by id.
    /// </summary>
    public class FindOrderingCartByBuyerOrId : Specification<OrderingCart>
    {
        private int? id;
        private ClaimsPrincipal buyer;
        private string buyerId;

        private FindMode findMode;

        public FindOrderingCartByBuyerOrId(int? id)
        {
            this.id = id;
            this.findMode = FindMode.id;
        }

        public FindOrderingCartByBuyerOrId(ClaimsPrincipal user)
        {
            this.buyer = user;
            this.findMode = FindMode.buyer;
        }

        public FindOrderingCartByBuyerOrId(string userId)
        {
            this.buyerId = userId;
            this.findMode = FindMode.userId;
        }

        public override Expression<Func<OrderingCart, bool>> AsExpression()
        {
            switch (findMode)
            {
                case FindMode.id: return p => this.id.HasValue && p.Id == this.id;
                case FindMode.buyer: return p => this.buyer != null && p.OwnerId == this.buyer.GetId();
                case FindMode.userId: return p => this.buyerId != null && p.OwnerId == this.buyerId;
                default: return null;
            }
        }
    }
    internal enum FindMode
    {
        id,
        buyer,
        userId
    }

}
