﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.OrderingCartSpecification
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Security.Claims;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event find by id.
    /// </summary>
    public class FindTicketInOrderingCartByTicketId : Specification<OrderingCart>
    {
        private int? id;
        public FindTicketInOrderingCartByTicketId(int? id)
        {
            this.id = id;
        }

       public override Expression<Func<OrderingCart, bool>> AsExpression()
        {
            return p => p.OrderCartTickets.Any(
                z => z.TicketId == id);
        } 
    }
}
