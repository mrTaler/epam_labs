﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The find ticket by event id.
    /// </summary>
    public class FindOrderingTicketById : Specification<OrderingTicket>
    {
        private readonly string id;

        public FindOrderingTicketById(string id)
        {
            this.id = id;
        }

        public override Expression<Func<OrderingTicket, bool>> AsExpression()
        {
            return p => p.Id.ToString() == this.id;
        }
    }
}
