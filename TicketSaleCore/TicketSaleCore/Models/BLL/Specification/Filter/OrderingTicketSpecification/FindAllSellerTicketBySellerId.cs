﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The find ticket by event id.
    /// </summary>
    public class FindAllSellerTicketBySellerId : Specification<OrderingTicket>
    {
        private readonly string userId;

        public FindAllSellerTicketBySellerId(string userId)
        {
            this.userId = userId;
        }

        public override Expression<Func<OrderingTicket, bool>> AsExpression()
        {
            return p => p.Tiket.SellerId == userId;
        }
    }
}
