﻿namespace TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Specifications;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The find ticket by event id.
    /// </summary>
    public class FindAllOrderingTicketByStatusName : Specification<OrderingTicket>
    {
        private readonly string statusName;

        public FindAllOrderingTicketByStatusName(string statusName)
        {
            this.statusName = statusName;
        }

        public override Expression<Func<OrderingTicket, bool>> AsExpression()
        {
            return p => p.TicketStatus.StatusName == this.statusName;
        }
    }
}
