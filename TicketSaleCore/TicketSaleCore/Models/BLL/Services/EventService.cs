﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using AutoMapper;

    using DAL.Interfaces;

    using Entities;

    using Infrastructure;

    using Interfaces;

    using Microsoft.Extensions.Logging;

    using TicketSaleCore.Models.BLL.DTO;
    using TicketSaleCore.Models.BLL.Specification.Filter.EventSpecification;
    using TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The event service.
    /// </summary>
    public class EventService : IEventService
    {
        /// <summary>
        /// The get not confirmet ticket.
        /// </summary>
        private readonly ISpecification<Ticket> getNotConfirmetTicket =
            new FindTicketByStatusSpecification(TicketStatusLog.NotEqual);

        /// <summary>
        /// The get ticket in order tickets by zero count.
        /// </summary>
        private readonly ISpecification<Ticket> getTicketInOrderTicketsByZeroCount =
            new FindTicketByOrderCountSpecification();

        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger factory.
        /// </param>
        public EventService(
            IUnitOfWork context,
            ILoggerFactory loggerFactory)
        {
            this.context = context;
            this.logger = loggerFactory.CreateLogger<EventService>();
            this.logger.LogWarning("create EventService");
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="EventService"/> class. 
        /// </summary>
        ~EventService()
        {
            this.logger.LogError("Destructor EventService");
        }

        /// <inheritdoc/>
        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        /// <exception cref="BllValidationException">
        /// </exception>
        public Event GetById(int? id)
        {
            if (id == null)
            {
                throw new BllValidationException("Not specified id", string.Empty);
            }

            var getEventsById = new FindEventByEventIdOrEventName(id);

            var eventOne = this.context.Events.GetOne(
            filter: getEventsById,
             includes: new EventInclude());

            if (eventOne == null)
            {
                throw new BllValidationException("eventType not found", string.Empty);
            }

            return eventOne;
        }

        /// <summary>
        /// The get for edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="EventsDTO"/>.
        /// </returns>
        public EventsDTO GetForEdit(int? id)
        {
            if (id != null)
            {
                var getEventsById = new FindEventByEventIdOrEventName(id.Value);
                Mapper.Initialize(cfg => cfg.CreateMap<Event, EventsDTO>());

                var eventToEdit = Mapper.Map<Event, EventsDTO>(
                    this.context.Events.GetOne(
                filter: getEventsById,
                 includes: new EventInclude()));
                return eventToEdit;
            }

            return null;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="EventsDTO"/>.
        /// </returns>
        public EventsDTO Update(EventsDTO entity)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<EventsDTO, Event>());
            var eventForEdit = this.context.Events.GetOne(
                filter: new FindEventByEventIdOrEventName(entity.Id),
                includes: new EventInclude());

            eventForEdit.Id = entity.Id;
            eventForEdit.Name = entity.Name;
            eventForEdit.Date = entity.Date;
            eventForEdit.EventsTypeId = entity.EventsTypeId;
            eventForEdit.Banner = entity.Banner;
            eventForEdit.Description = entity.Description;
            eventForEdit.VenueId = entity.VenueId;

            this.context.Events.Update(eventForEdit);

            this.context.SaveChanges();

            return entity;
        }

        /// <inheritdoc />
        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.IEnumerable`1" />.
        /// </returns>
        public IEnumerable<Event> GetAll() => this.context.Events.GetAll(
            includes: new EventInclude());

        /// <summary>
        /// The get all event with tickets.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Event> GetAllEventWithTickets() => this.context.Events.GetByFilter(
                new FindEventsWithFreeTickets());

        /// <summary>
        /// The get all event with tickets.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Event> GetAllEventWithTickets(ClaimsPrincipal user)
        {
            var logUser = user.GetId();
            var ticketQuerySpecification1 = this.getTicketInOrderTicketsByZeroCount;
            var ticketQuerySpecification2 = this.getNotConfirmetTicket;

            if (logUser != null)
            {
                var getNotCurrentUserTicket = new FindTicketBySellerSpecification(user);

                ticketQuerySpecification1 = ticketQuerySpecification1
                    .And(getNotCurrentUserTicket);
                ticketQuerySpecification2 = this.getNotConfirmetTicket
                    .And(getNotCurrentUserTicket);
            }

            var notOrderinngTicketsNotLogUser = this.context.Tickets.GetByFilter(
               filter: ticketQuerySpecification1,
               includes: new TicketsInclude());

            var orderingNotSoldNotLogUser = this.context.Tickets.GetByFilter(
                filter: ticketQuerySpecification2,
                includes: new TicketsInclude());

            var allQueryTicket = notOrderinngTicketsNotLogUser.Union(orderingNotSoldNotLogUser);

            var allEventWithAvailableTicket = allQueryTicket.GroupBy(p => p.Event).Select(this.Selector);

            return allEventWithAvailableTicket.ToList();
        }

        /// <summary>
        /// The get all event with tickets async.
        /// </summary>
        /// <param name="evenSpecification">
        /// The even Specification.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<IEnumerable<Event>> GetAllEventWithTicketsAsync(
            ISpecification<Event> evenSpecification)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            var notOrderinngTickets = this.context.Tickets.GetAsync(
             filter: this.getTicketInOrderTicketsByZeroCount,
             includes: new TicketsInclude());

            var orderingNotSold = this.context.Tickets.GetAsync(
                filter: this.getNotConfirmetTicket,
                includes: new TicketsInclude());

            var allQueryTicket = notOrderinngTickets.Result.Union(orderingNotSold.Result);

            var allEventWithAvailableTicket =
                allQueryTicket.GroupBy(p => p.Event)
                .Select(this.Selector);

            sw.Stop();

            this.logger.LogError($"old service:{sw.Elapsed}");

            if (evenSpecification != null)
            {
                return Task.FromResult(  allEventWithAvailableTicket.Where(evenSpecification));
            }

            return Task.FromResult(allEventWithAvailableTicket);
        }

        /// <summary>
        /// The get all event with tickets with spec async.
        /// </summary>
        /// <param name="findSpecificaion">
        /// The find specificaion.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public  Task<IEnumerable<Event>> GetAllEventWithTicketsWithSpecAsync(
            ISpecification<Event> findSpecificaion)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            var notOrderinngTickets = this.context.Tickets.GetAsync(
                                          filter: this.getTicketInOrderTicketsByZeroCount,
                                          includes: new TicketsInclude());

            var orderingNotSold = this.context.Tickets.GetAsync(
                                      filter: this.getNotConfirmetTicket,
                                      includes: new TicketsInclude());

            var allQueryTicket = notOrderinngTickets.Result.Union(orderingNotSold.Result);

            var allEventWithAvailableTicket = allQueryTicket
                .GroupBy(p => p.Event)
                .Select(this.Selector);
            var qwe = allEventWithAvailableTicket.Where(findSpecificaion);//.ToList();
            sw.Stop();
            this.logger.LogError($"Method {nameof(this.GetAllEventWithTicketsWithSpecAsync)} work time is:{sw.Elapsed}");
            return  Task.FromResult(qwe) ;
        }

        /// <summary>
        /// The get count async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<int> GetCountAsync()
        {
            return this.context.Events.GetCountAsync();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.logger.LogError("Dispose EventService");
            this.context.Dispose();
        }

        /// <inheritdoc />
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Boolean" />.
        /// </returns>
        /// <exception cref="T:TicketSaleCore.Models.BLL.Infrastructure.BllValidationException"> cannot delete
        /// </exception>
        public bool Delete(Event entity)
        {
            if (this.IsExists(entity.Id))
            {
                /* if (entity.Tickets.Count != 0)
                 {
                     throw new BllValidationException(
                         $"This Event {entity.Name} cannot delete form DB because need cascade delete",
                         "Need cascade");
                 }
                 else
                 {*/
                this.context.Events.Delete(entity);
                return Convert.ToBoolean(this.context.SaveChanges());

                /* }*/
            }
            else
            {
                throw new BllValidationException($"This Event {entity.Name} cannot delete form DB because is not exist", string.Empty);
            }
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        /// <exception cref="BllValidationException">If exist
        /// </exception>
        public Event Create(Event entity)
        {
            if (!this.IsExists(entity.Name))
            {
                this.context.Events.Create(entity);
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException($"This Event {entity.Name} is alredy exist", "alredy exist");
            }

            return entity;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        /// <exception cref="BllValidationException">if not exist
        /// </exception>
        public Event Update(Event entity)
        {
            if (this.IsExists(entity.Id))
            {
                // var updateEentitty = Get(entity.Id);

                // updateEentitty = entity;
                this.context.Events.Update(entity);

                // Context.Context.Entry(entity).State = EntityState.Modified;
                // updateEentitty.Name = entity.Name;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This Event name:{entity.Name},Id:{entity.Id} cannot Update in DB because is not exist",
                    string.Empty);
            }

            return entity;
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            return this.context.Events.GetExist(
             filter: new FindEventByEventIdOrEventName(id));
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            return this.context.Events.GetExist(
                filter: new FindEventByEventIdOrEventName(name));
        }

        /// <inheritdoc />
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="T:TicketSaleCore.Models.Entities.Event" />. Event 
        /// </returns>
        public Event GetByName(string name) => this.context.Events.GetOne(
            new FindEventByEventIdOrEventName(name), new EventInclude());

        /// <summary>
        /// The Event selector.
        /// </summary>
        /// <param name="grouping">
        /// The grouping.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        private Event Selector(IGrouping<Event, Ticket> grouping)
        {
            var value = grouping.Select(p => p).ToList();
            var tim = grouping.Key;
            tim.Tickets = value;
            return tim;
        }
    }
}
