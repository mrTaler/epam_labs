﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using TicketSaleCore.Models.BLL.Infrastructure;
    using TicketSaleCore.Models.BLL.Interfaces;
    using TicketSaleCore.Models.BLL.Specification.Filter.OrderStatusSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.BLL.Specification.TicketsSpecification;
    using TicketSaleCore.Models.DAL.Interfaces;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The order status service.
    /// </summary>
    public class OrderStatusService : IOrderStatusService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderStatusService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public OrderStatusService(IUnitOfWork context)
        {
            this.context = context;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<OrderStatus> GetAll()
        {
            return this.context.OrderStatuses.GetAll(null, new OrderStatusInclude());
        }

        /// <inheritdoc /> OrderStatusSpecification
        public OrderStatus GetById(int? id)
        {
            return this.context.OrderStatuses.GetOne(
                new FindOrderStatusById(id),
                new OrderStatusInclude());
        }

        /// <inheritdoc />
        public OrderStatus GetByName(string name)
        {
            var nameExist = new FindOrderStatusByStatusName(name);
            var ret = this.context.OrderStatuses.GetOne(nameExist, new OrderStatusInclude());

            return ret;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="OrderStatus"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public OrderStatus Create(OrderStatus entity)
        {
            if (!this.IsExists(entity.StatusName))
            {
                this.context.OrderStatuses.Create(entity);
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This OrderStatus {entity.StatusName} is" + $" alredy exist",
                    "alredy exist");
            }

            return entity;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public bool Delete(
            OrderStatus entity)
        {
            if (this.IsExists(entity.Id))
            {
                if (entity.OrderTickets.Count != 0)
                {
                    throw new BllValidationException(
                        $"This OrderStatus {entity.StatusName} cannot delete" + $" form DB because need cascade delete",
                        "Need cascade");
                }
                else
                {
                    this.context.OrderStatuses.Delete(entity);
                    return Convert.ToBoolean(this.context.SaveChanges());
                }
            }
            else
            {
                throw new BllValidationException($"This City {entity.StatusName} cannot delete form DB because is not exist", string.Empty);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="OrderStatus"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public OrderStatus Update(
            OrderStatus entity)
        {
            if (this.IsExists(entity.Id))
            {
                var updateEentitty = this.GetById(entity.Id);
                updateEentitty.StatusName = entity.StatusName;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This OrderStatus name:{entity.StatusName},Id:{entity.Id} cannot Update"
                    + $" in DB because is not exist",
                    string.Empty);
            }

            return entity;
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            return this.context.OrderStatuses.GetExist(new FindOrderStatusById(id));
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            return this.context.OrderStatuses.GetExist(new FindOrderStatusByStatusName(name));
        }
    }
}
