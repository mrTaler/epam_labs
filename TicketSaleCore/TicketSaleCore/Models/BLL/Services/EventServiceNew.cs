﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    using Specification.TicketsSpecification;

    using TicketSaleCore.Models.BLL.Interfaces;
    using TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.BLL.Specification.OrderBy;
    using TicketSaleCore.Models.DAL.Interfaces;
    using TicketSaleCore.Models.DAL.Specifications.OrderSpecification;
    using TicketSaleCore.Models.DAL._Ef;
    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The event service new.
    /// </summary>
    public class EventServiceNew : IEventServiceNew
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// The app.
        /// </summary>
        private ApplicationContext app;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventServiceNew"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger factory.
        /// </param>
        /// <param name="app">
        /// The app.
        /// </param>
        public EventServiceNew(
            IUnitOfWork context,
            ILoggerFactory loggerFactory,
            ApplicationContext app)
        {
            this.context = context;
            this.logger = loggerFactory.CreateLogger<EventServiceNew>();
            this.logger.LogWarning("create EventServiceNew");
            this.app = app;
        }

        /// <summary>
        /// The get all event with tickets async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<Event>> GetAllEventWithTicketsAsync(
            ClaimsPrincipal user)
        {
           var order = new SortEventByDataOrderSpecification(OrderEn.Ascending);

            var qm1 = this.context.Tickets.GetAsync(
                         filter: new FindTicketByOrderCountSpecification(),
                         includes: new TicketsInclude());

            var qm2 = this.context.Tickets.GetAsync(
                filter: new FindTicketByStatusSpecification(TicketStatusLog.NotEqual),
                includes: new TicketsInclude());

            var qm3 = qm1.Result.Union(qm2.Result);

            var qt = qm3.GroupBy(p => p.Event)
                .Select(this.Selector1)
                .OrderBy(order);
         
            return await Task.FromResult(qt);
        }

        /// <summary>
        /// The selector 1.
        /// </summary>
        /// <param name="grouping">
        /// The grouping.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        private Event Selector1(IGrouping<Event, Ticket> grouping)
        {
            var value = grouping.Select(p => p).ToList();
            var tim = grouping.Key;
            tim.Tickets = value;
            return tim;
        }
    }
}