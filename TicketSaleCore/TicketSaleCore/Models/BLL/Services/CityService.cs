﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using Entities;

    using Infrastructure;

    using Interfaces;

    using Microsoft.Extensions.Logging;

    using TicketSaleCore.Models.BLL.Specification.Filter.CitySpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.DAL.Interfaces;

    /// <summary>
    /// The city service.
    /// </summary>
    public class CityService : ICityService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="CityService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger factory.
        /// </param>
        public CityService(
            IUnitOfWork context,
            ILoggerFactory loggerFactory)
        {
            this.context = context;
            this.logger = loggerFactory.CreateLogger<CityService>();
            this.logger.LogWarning("create CityService");
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CityService"/> class. 
        /// </summary>
        ~CityService()
        {
            this.logger.LogError("Destructor CityService");
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.logger.LogError("Dispose CityService");
            this.context.Dispose();
        }

        /// <inheritdoc />
        /// <summary>
        /// Get all entities
        /// </summary>
        /// <returns>Cities with include Venues</returns>
        public IEnumerable<City> GetAll()
        {
            return this.context.Cites.GetAll(
               orderBy: null,
               includes: new CityInclude());
        }

        /// <inheritdoc />
        /// <summary>
        /// Get City by Id
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>City with include Venues</returns>
        public City GetById(int? id)
        {
            var idSpec = new FindCityByCityId(id);
            return this.context.Cites.GetOne(
                filter: idSpec,
                includes: new CityInclude());
        }

        /// <inheritdoc />
        /// <summary>
        /// Get City by Id
        /// </summary>
        /// <param name="id">Uniqe identifier</param>
        /// <returns>City with include Venues</returns>
        public City GetByName(string name)
        {
            var nameSpec = new FindCityByCityName(name);
            var ret = this.context.Cites.GetOne(
                filter: nameSpec,
                includes: new CityInclude());
            return ret;
        }

        /// <inheritdoc />
        /// <summary>
        /// Delete City from db (not cascade)
        /// </summary>
        /// <param name="entity">City to delete</param>
        /// <returns>Success of the result</returns>
        /// <exception cref="T:TicketSaleCore.Models.BLL.Infrastructure.BllValidationException">Need access to cascade removal</exception>
        public bool Delete(City entity)
        {
            if (this.IsExists(entity.Id))
            {
                if (entity.Venues.Count != 0)
                {
                    throw new BllValidationException(
                        $"This City {entity.Name} cannot delete form DB because need cascade delete",
                        "Need cascade");
                }
                else
                {
                    this.context.Cites.Delete(entity);
                    return Convert.ToBoolean(this.context.SaveChanges());
                }
            }
            else
            {
                throw new BllValidationException($"This City {entity.Name} cannot delete form DB because is not exist", string.Empty);
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Add City to db with check on exist
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public City Create(City entity)
        {
            if (!this.IsExists(entity.Name))
            {
                this.context.Cites.Create(entity);
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException($"This City {entity.Name} is alredy exist", "alredy exist");
            }

            return entity;
        }

        /// <inheritdoc />
        /// <summary>
        /// Update exist City
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public City Update(City entity)
        {
            if (this.IsExists(entity.Id))
            {
                var updateEentitty = this.GetById(entity.Id);
                updateEentitty.Name = entity.Name;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This City name:{entity.Name},Id:{entity.Id} cannot Update in DB because is not exist",
                    string.Empty);
            }

            return entity;
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            var idExist = new FindCityByCityId(id);
            return this.context.Cites.GetExist(
                filter: idExist);
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            var nameExist = new FindCityByCityName(name);
            return this.context.Cites.GetExist(
                filter: nameExist);
        }
    }
}