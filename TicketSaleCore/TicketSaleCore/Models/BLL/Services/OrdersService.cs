﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;

    using DAL.Interfaces;

    using Entities;

    using Infrastructure;

    using Interfaces;

    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.BLL.Specification.TicketsSpecification;

    /// <inheritdoc />
    /// <summary>
    /// The orders service.
    /// </summary>
    public class OrdersService : IOrdersService
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// The order status service.
        /// </summary>
        private readonly IOrderStatusService orderStatusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="orderStatusService">
        /// The order Status Service.
        /// </param>
        public OrdersService(
            IUnitOfWork context,
            IOrderStatusService orderStatusService)
        {
            this.orderStatusService = orderStatusService;
            this.context = context;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <inheritdoc />
        /// <summary>
        /// The get Order by unique identifier
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T:TicketSaleCore.Models.Entities.Order" />.
        /// </returns>
        public Order GetById(int? id)
        {
            var idExist = new FindOrderByOrderId(id);

            return this.context.Orders.GetOne(idExist, new OrderInclude());
        }

        /// <summary>
        /// The get all Orders id DB
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Order> GetAll()
        {
            return this.context.Orders.GetAll(null, new OrderInclude());
        }

        /// <inheritdoc />
        /// <summary>
        /// The get user orders by buyer id
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The Order <see cref="T:System.Collections.Generic.IEnumerable`1" />.
        /// </returns>
        public IEnumerable<Order> GetUserOrders(string id)
        {
            var ret = this.context.Orders.GetByFilter(
                new FindOrderByBuyerId(id),
                null,
                new OrderInclude());
            return ret;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The <see cref="bool"/>j j</returns>
        /// <exception cref="BllValidationException">Ex j</exception>
        public bool Delete(
            Order entity)
        {
            if (this.IsExists(entity.Id))
            {
                // if (entity.OrderTickets.Count != 0)
                // {
                // throw new BllValidationException($"This Order {entity.TrackNo} cannot delete" +
                // $" form DB because need cascade delete", "Need cascade");
                // }
                // else
                // {
                // context.Entry(item).State = EntityState.Deleted
                this.context.Orders.Delete(entity);
                return Convert.ToBoolean(this.context.SaveChanges());
            }
            else
            {
                throw new BllValidationException($"This Venue {entity.TrackNo} cannot delete form DB because is not exist", string.Empty);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Order"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public Order Update(
            Order entity)
        {
            if (this.IsExists(entity.Id))
            {
                // var updateEntity = Get(entity.Id);

                // updateEntity = entity;
                this.context.Orders.Update(entity);

                // Context.Context.Entry(entity).State = EntityState.Modified;
                // updateEentitty.Name = entity.Name;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException($" cannot Update" + $" in DB because is not exist", string.Empty);
            }

            return entity;
        }

        /// <inheritdoc />
        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="T:TicketSaleCore.Models.Entities.Order" />.
        /// </returns>
        /// <exception cref="T:System.NotImplementedException">
        /// </exception>
        public Order Create(Order entity)
        {
            if (!this.IsExists(entity.TrackNo))
            {
                this.context.Orders.Create(entity);
                this.context.SaveChanges();
            }
            else
            {
                // throw new BllValidationException($"This City {entity.Name} is alredy exist", "alredy exist");
            }

            return entity;
        }

        /// <summary>
        /// The new order with ticket.
        /// </summary>
        /// <param name="orderingTickets">
        /// The ordering tickets.
        /// </param>
        /// <returns>
        /// The <see cref="Order"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">j j
        /// </exception>
        public Order NewOrderWithTicket(
            IEnumerable<Ticket> orderingTickets)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The new order with tickets.
        /// </summary>
        /// <param name="orderingTickets">
        /// The ordering tickets.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Order> NewOrderWithTickets(IEnumerable<Ticket> orderingTickets, ClaimsPrincipal user)
        {
            List<Order> newOrders = new List<Order>(); // new order

            // dictionary for Seller and his tickets
            var ticketBySeller = new Dictionary<AppUser, List<Ticket>>();

            // get orderingTickets as List<Ticket>
            var orderingTicketsEnumerable =
                orderingTickets as IList<Ticket>
                ?? orderingTickets.ToList();

            // find all uniqe seller in Ticket list
            var sellers = orderingTicketsEnumerable.Select(p => p.Seller).Distinct().ToList();

            // GroupBy(p => p).ToList();
            var buyer = user.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;

            var sel = sellers.Remove(sellers.FirstOrDefault(p => p.Id == buyer));

            // make seller ticket dictionary group by Seller 
            foreach (var seller in sellers)
            {
                ticketBySeller.Add(seller, new List<Ticket>());
            }

            foreach (var ticket in orderingTicketsEnumerable)
            {
                // find ticket seller
                var sellerT = ticket.Seller;

                // check current buyer cannnot sell and buy himself tickets
                if (!Equals(ticket.Seller, buyer))
                {
                    // id
                    // check selling ticket
                    if (ticket.OrderTickets == null)
                    {
                        ticketBySeller[sellerT].Add(ticket); // add current ticket to equal seller
                    }
                    else
                    {
                        {
                            // ticket cannot be a sold
                            // if (ticket.Order.Status.StatusName != ("Confirmed"/*nameof(TicketStatus.Sold)*/))
                            ticketBySeller[sellerT].Add(ticket);
                        }
                    }
                }
            }

            foreach (var ti in ticketBySeller)
            {
                var order = new Order
                {
                    BuyerId = buyer,
                    DateTimeOrderCreate = DateTime.UtcNow,
                    Status = this.orderStatusService.GetByName(
                                            "Waiting for conformation" /*nameof(TicketStatus.WaitingConfomition)*/),
                    TrackNo = Guid.NewGuid().ToString()
                };
                foreach (var ticket in ti.Value)
                {
                    order.OrderTickets.Add(
                        new OrderingTicket
                        {
                            dateAdded = DateTime.UtcNow,
                            Id = Guid.NewGuid(),
                            Tiket = ticket,
                            TicketStatus = this.orderStatusService.GetByName(
                                        "Waiting for conformation" /*nameof(TicketStatus.WaitingConfomition)*/)
                        });
                }

                // this.Add(order);
                newOrders.Add(order);
            }

            return newOrders;
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            var idExist = new FindOrderByOrderId(id);

            return this.context.Orders.GetExist(idExist);
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="Order"/>.
        /// </returns>
        public Order GetByName(string name)
        {
            var nameExist = new FindOrderByTrackNo(name);
            return this.context.Orders.GetOne(nameExist, new OrderInclude());
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            var nameExist = new FindOrderByTrackNo(name);

            return this.context.Orders.GetExist(nameExist);
        }

        /// <summary>
        /// The get new user orders.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Order> GetNewUserOrders(ClaimsPrincipal user)
        {
            var buyer = user.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;
            var newOrders = this.GetAll()
                .Where(p => p.Buyer.Id == buyer)
                .Where(p => p.DateTimeOrderCreate >= DateTime.UtcNow.AddDays(-2));
            return newOrders;
        }

        /// <summary>
        /// The is in order.
        /// </summary>
        /// <param name="ticket">
        /// The ticket.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsInOrder(Ticket ticket, ClaimsPrincipal user)
        {
            var buyer = user.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;
            var userOreder = this.GetUserOrders(buyer);
            var ret = userOreder.Any(p => p.OrderTickets.Any(z => z.TiketId.Equals(ticket.Id)));
            return ret;
        }
    }
}
