﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;

    using DAL.Interfaces;
    using DAL.Specifications.Interfaces;

    using Entities;

    using Features.Tickets.Tickets.ViewModels;

    using Infrastructure;

    using Interfaces;

    using Specification.TicketsSpecification;

    using TicketSaleCore.Models.BLL.Specification.Filter.EventSpecification;
    using TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;

    /// <inheritdoc />
    /// <summary>
    /// The tickets service.
    /// </summary>
    public class TicketsService : ITicketsService
    {
        /// <summary>
        /// The get any not confirmed ticket.
        /// </summary>
        private readonly ISpecification<Ticket> getAnyNotConfirmetTicket
            = new FindTicketByStatusSpecification(TicketStatusLog.NotEqual);

        /// <summary>
        /// The get count.
        /// </summary>
        private readonly ISpecification<Ticket> getNotSoldTicket
            = new FindTicketByOrderCountSpecification();

        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TicketsService"/> class.
        /// </summary>
        /// <param name="context">
        /// The UoW context.
        /// </param>
        public TicketsService(
            IUnitOfWork context)
        {
            this.context = context;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <inheritdoc />
        public Ticket GetByName(string name)
        {
            var ticket = this.context.Tickets.GetOne(
                new FindTicketBySellerNotes(name),
                new TicketsInclude());
            return ticket;
        }

        /// <inheritdoc />
        public Ticket GetById(int? ticketId)
        {
            if (ticketId != null)
            {
                var ticket = this.context.Tickets.GetOne(
                    new FindTicketByTicketId(ticketId.Value),
                    new TicketsInclude());
                return ticket;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// The get all Tickets
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Ticket> GetAll()
        {
            var tickets = this.context.Tickets.GetAll(null, new TicketsInclude());
            return tickets;
        }

        /// <summary>
        /// Get ticket by event.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="TicketIndexViewModel"/>.
        /// </returns>
        public TicketIndexViewModel GetTicketByEvent(int? eventId)
        {
            TicketIndexViewModel ticketsVm = new TicketIndexViewModel();
            if (eventId != null)
            {
                ticketsVm.AvailableTicketsToSale = this.context.Tickets.GetByFilter(
                    new FindTicketByEventId(eventId.Value)
                        .And(new FindTicketByOrderTickets(null)),
                    null,
                    new TicketsInclude());

                ticketsVm.CurentEvent = this.context.Events.GetOne(
                    new FindEventByEventIdOrEventName(eventId),
                    new EventInclude());
            }
            else
            {
                ticketsVm.AvailableTicketsToSale = this.context.Tickets.GetAll(null, new TicketsInclude());

                ticketsVm.CurentEvent = null;
            }

            return ticketsVm;
        }

        /// <summary>
        /// Get ticket by event w buyer.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="TicketIndexViewModel"/>.
        /// </returns>
        public TicketIndexViewModel GetTicketByEventWBuyer(int? eventId, ClaimsPrincipal user)
        {
            ISpecification<Ticket> genSpec;
            var logUser = user.GetId();
            if (logUser != null)
            {
                var getSeller = new FindTicketBySellerSpecification(logUser);
                TicketIndexViewModel ticketsVm = new TicketIndexViewModel();
                if (eventId != null)
                {
                    var eventSpec = new FindTicketByEventId(eventId.Value);
                    genSpec = this.getNotSoldTicket.And(getSeller.And(eventSpec));

                    var notOrderinngTicketsNotLogUser = this.context.Tickets.GetByFilter(
                       filter: genSpec,
                       includes: new TicketsInclude());

                    var orderingNotSoldNotLogUser = this.context.Tickets.GetByFilter(
                                 filter: this.getAnyNotConfirmetTicket.And(getSeller.And(eventSpec)),
                                 includes: new TicketsInclude());

                    var allQueryTicket = notOrderinngTicketsNotLogUser.Union(orderingNotSoldNotLogUser);

                    ticketsVm.AvailableTicketsToSale = allQueryTicket;
                    ticketsVm.CurentEvent = allQueryTicket.FirstOrDefault().Event;

                    /* .Include(p => p.Venue)
                                         .ThenInclude(z => z.City)
                                         .First(p => p.Id == eventId);*/
                }
                else
                {
                    ticketsVm.CurentEvent = null;
                    ticketsVm.AvailableTicketsToSale = this.context.Tickets.GetAll(
                        null,
                        new TicketsInclude());
                }

                /*  if (eventId != null)
                  {
                     /* var notOrderinngTicketsNotLogUser = this.context.Tickets

                          .Include(p => p.Event)
                          .Include(t => t.Seller)
                          .Where(p => p.EventId == eventId)
                          .Where(p => p.SellerId != logUser)
                          .Where(p => p.OrderTickets.Count == 0).ToList();

                      var orderingNotSoldNotLogUser = this.context.Tickets
                          .Include(p => p.Event)
                          .Include(t => t.Seller)
                          .Where(p => p.EventId == eventId)
                          .Where(p => p.SellerId != logUser)
                          .Where(p => p.OrderTickets.Any(z => z.TicketStatus.StatusName != "Confirmed"))
                          .ToList();

                     // var q3 = notOrderinngTicketsNotLogUser.Union(orderingNotSoldNotLogUser).ToList();

                    //  ticketsVm.AvailableTicketsToSale = q3;
                    //  ticketsVm.CurentEvent = this.context.Events
                    //      .Include(p => p.Venue)
                    //      .ThenInclude(z => z.City)
                    //      .First(p => p.Id == eventId);

                  }*/
                return ticketsVm;
            }
            else
            {
                TicketIndexViewModel ticketsVm = new TicketIndexViewModel();

                var eventSpec = new FindTicketByEventId(
                    eventId.Value);

                genSpec = eventSpec.And(this.getNotSoldTicket);

                var notOrderinngTicketsNotLogUser = this.context.Tickets.GetByFilter(
                    genSpec,
                    null,
                    new TicketsInclude());

                var orderingNotSoldNotLogUser = this.context.Tickets.GetByFilter(this.getAnyNotConfirmetTicket.And(eventSpec));

                var allQueryTicket = notOrderinngTicketsNotLogUser.Union(orderingNotSoldNotLogUser);

                ticketsVm.AvailableTicketsToSale = allQueryTicket;
                ticketsVm.CurentEvent = allQueryTicket.FirstOrDefault().Event;

                return ticketsVm;
            }
        }

        /// <summary>
        /// The get waiting confomition tickets.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<OrderingTicket> GetWaitingConfomitionTickets(string userId)
        {
            var spec = new FindAllSellerTicketBySellerId(userId);
            var returnTickets1 = this.context.OrderingTickets.GetByFilter(
                spec.And(
                    new FindAllOrderingTicketByStatusName("Waiting for conformation")),
                null,
                new OrderingTicketInclude());
            return returnTickets1;
        }

        /// <summary>
        /// The get all user tickets.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Ticket> GetAllUserTickets(string userId, TicketStatus param)
        {
            List<Ticket> returnTickets = null;
            var spec = new FindTicketBySellerSpecification(userId);
            switch (param)
            {
                case TicketStatus.SellingTickets:

                    var allUserTicket = this.context.Tickets.GetByFilter(
                        spec.And(this.getNotSoldTicket),
                        null,
                        new TicketsInclude());

                    returnTickets = new List<Ticket>(allUserTicket);
                    return returnTickets;


                case TicketStatus.Sold:
                    var soldSpec = new FindTicketByStatusSpecification(TicketStatusLog.Equal, nameof(TicketStatus.Confirmed));
                    returnTickets = new List<Ticket>(
                        this.context.Tickets.GetByFilter(
                            soldSpec.And(spec),
                            null,
                            new TicketsInclude()));

                    return returnTickets;
                default:
                    return (List<Ticket>)null;
            }
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            var idExist = new FindTicketByTicketId(id);
            return this.context.Tickets.GetExist(idExist);
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            var nameExist = new FindTicketBySellerNotes(name);
            return this.context.Tickets.GetExist(nameExist);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Delete(Ticket entity)
        {
            this.context.Tickets.Delete(entity);

            return Convert.ToBoolean(this.context.SaveChanges());
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        public Ticket Create(Ticket entity)
        {
            this.context.Tickets.Create(entity);
            this.context.SaveChanges();
            return entity;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public Ticket Update(Ticket entity)
        {
            if (this.IsExists(entity.Id))
            {
                var updateEentitty = this.GetById(entity.Id);

                updateEentitty.Price = entity.Price;
                this.context.Tickets.Update(updateEentitty);

                // Context.Context.Entry(entity).State = EntityState.Modified;
                // updateEentitty.Name = entity.Name;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This Ticket name:{entity.SellerNotes},Id:{entity.Id} cannot Update"
                    + $" in DB because is not exist",
                    string.Empty);
            }

            return entity;
        }

        /// <summary>
        /// The is solded.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsSolded(Ticket entity)
        {
            var sold = entity.OrderTickets.Any(p => p.TicketStatus.StatusName == "Confirmed");
            return sold;
        }

        /// <summary>
        /// The create new ticket.
        /// </summary>
        /// <param name="tiket">
        /// The tiket.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        public Ticket CreateNewTicket(Ticket tiket, ClaimsPrincipal user)
        {
            var seller = user.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;

            tiket.SellerId = seller;
            return this.Create(tiket);
        }
    }
}
