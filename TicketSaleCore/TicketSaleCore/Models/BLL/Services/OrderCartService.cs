﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;

    using Entities;

    using Interfaces;

    using TicketSaleCore.Models.BLL.Specification.Filter.OrderingCartSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.BLL.Specification.TicketsSpecification;
    using TicketSaleCore.Models.DAL.Interfaces;

    /// <summary>
    /// The order cart service.
    /// </summary>
    public class OrderCartService : IOrderCartService
    {
        /// <summary>
        /// The tickets service.
        /// </summary>
        private readonly ITicketsService ticketsService;

        /// <summary>
        /// The orders service.
        /// </summary>
        private readonly IOrdersService ordersService;

        /// <summary>
        /// Gets the context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderCartService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="ordersService">
        /// The orders Service.
        /// </param>
        /// <param name="ticketsService">
        /// The tickets Service.
        /// </param>
        public OrderCartService(
            IUnitOfWork context,
            IOrdersService ordersService,
            ITicketsService ticketsService)
        {
            this.ticketsService = ticketsService;
            this.ordersService = ordersService;
            this.context = context;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <summary>
        /// The add to cart.
        /// </summary>
        /// <param name="ticketId">
        /// The ticket id.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        public Ticket AddToCart(
            int? ticketId,
            ClaimsPrincipal user)
        {
            Ticket entity = this.ticketsService.GetById(ticketId);

            var buyesrId = user.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;
            if (entity.SellerId != buyesrId)
            {
                if (!this.ordersService.IsInOrder(entity, user) &&
                    !this.ticketsService.IsSolded(entity))
                {
                    if (this.IsExists(buyesrId))
                    {
                        if (this.IsTicketExist(entity, user))
                        {
                            return entity;
                        }
                        else
                        {
                            var idSpec = new FindTicketByTicketId(entity.Id);
                            var ti = this.context.Tickets.GetOne(idSpec);
                            var cart = this.GetUserCart(user);
                            cart.DateLastUpdate = DateTime.UtcNow;
                            cart.OrderCartTickets.Add(new OrderCartTicket { Tickett = ti });
                            this.context.OrderCart.Update(cart);
                            this.context.SaveChanges();
                            return entity;
                        }
                    }
                    else
                    {
                        var cart = new OrderingCart { OwnerId = buyesrId, DateCreated = DateTime.UtcNow };
                        this.Create(cart);
                        cart.OrderCartTickets.Add(new OrderCartTicket { Tickett = entity });
                        this.context.SaveChanges();
                        return entity;
                    }
                }
                else
                {
                    return entity;
                }
            }
            else
            {
                return entity;
            }
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingCart"/>.
        /// </returns>
        public OrderingCart Create(OrderingCart entity)
        {
            this.context.OrderCart.Create(entity);
            this.context.SaveChanges();
            return entity;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool Delete(OrderingCart entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingCart"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public OrderingCart GetById(int? id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingCart"/>.
        /// </returns>
        public OrderingCart GetByName(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get user cart.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingCart"/>.
        /// </returns>
        public OrderingCart GetUserCart(ClaimsPrincipal user)
        {

            if (this.IsExists(user.GetId()))
            {
                var idSpec = new FindOrderingCartByBuyerOrId(user);

                var ret = this.context.OrderCart.GetOne(idSpec, new OrderingCartInclude());
                return ret;
            }
            else
            {
                var cart = new OrderingCart { OwnerId = user.GetId(), DateCreated = DateTime.UtcNow };
                var ret = this.Create(cart);
                this.context.SaveChanges();

                return ret;
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IEnumerable<OrderingCart> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool IsExists(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            var nameExist = new FindOrderingCartByBuyerOrId(name);
            return this.context.OrderCart.GetExist(nameExist);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingCart"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public OrderingCart Update(OrderingCart entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The is ticket exist.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="user">
        /// The User.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsTicketExist(
            Ticket entity,
            ClaimsPrincipal user)
        {
            var orderCartTicketIdSpec = new FindOrderingCartByBuyerOrId(user);
            var orderCartIdSpec =
                new FindTicketInOrderingCartByTicketId(entity.Id);

            var gen = orderCartIdSpec.And(orderCartTicketIdSpec);

            bool ticketInUserCart = this.context.OrderCart.GetExist(gen);

            // .Include(p => p.OrderCartTickets)
            // .ThenInclude(p => p.Tickett).FirstOrDefault(p => p.OwnerId == buyesrId)
            // .OrderCartTickets.Any(p => p.TicketId == entity.Id);
            return ticketInUserCart;

            // .OrderCartTickets.Any(p => p.TicketId == entity.Id);
        }

        /// <summary>
        /// The make orders.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Order> MakeOrders(ClaimsPrincipal user)
        {
            if (user != null)
            {
                var buyer = user.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;
                if (buyer != null)
                {
                    // var cartCurrentUser = this.GetUserCart(user);
                    var order = new List<Order>();

                    var orders = this.ordersService.NewOrderWithTickets(this.GetUserCart(user).OrderCartTickets?.Select(p => p.Tickett), user);

                    if (orders != null)
                    {
                        foreach (var orderItem in orders)
                        {
                            this.ordersService.Create(orderItem);
                            order.Add(orderItem);
                        }

                        this.CleanCart(user);
                        return order;
                    }
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// The clean cart.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CleanCart(ClaimsPrincipal user)
        {
            var cartToClean = this.GetUserCart(user);
            cartToClean.OrderCartTickets.Clear();
            cartToClean.DateLastUpdate = DateTime.UtcNow;

            this.context.OrderCart.Update(cartToClean);

            return Convert.ToBoolean(this.context.SaveChanges());
        }
    }
}
