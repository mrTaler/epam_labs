﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using DAL.Interfaces;

    using Entities;

    using Infrastructure;

    using Interfaces;

    using Microsoft.Extensions.Logging;

    using TicketSaleCore.Models.BLL.Specification.Filter.VenueSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;

    /// <summary>
    /// The venues service.
    /// </summary>
    public class VenuesService : IVenuesService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="VenuesService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger factory.
        /// </param>
        public VenuesService(
            IUnitOfWork context,
            ILoggerFactory loggerFactory)
        {
            this.context = context;
            this.logger = loggerFactory.CreateLogger<VenuesService>();
            this.logger.LogWarning($"create {nameof(VenuesService)}");
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="VenuesService"/> class. 
        /// </summary>
        ~VenuesService()
        {
            this.logger.LogError("Destructor VenuesService");
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.logger.LogError("Dispose VenuesService");
            this.context.Dispose();

        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        public Venue GetById(int? id)
        {
            return this.context.Venues.GetOne(
                new FindVenueByNameOrId(id),
                new VenueInclude());
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Venue> GetAll()
        {
            return this.context.Venues.GetAll(includes:new VenueInclude());
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public bool Delete(Venue entity)
        {
            if (this.IsExists(entity.Id))
            {
                if (entity.Events.Count != 0)
                {
                    throw new BllValidationException(
                        $"This Venue {entity.Name} cannot delete form DB because need cascade delete",
                        "Need cascade");
                }
                else
                {
                    // context.Entry(item).State = EntityState.Deleted
                    this.context.Venues.Delete(entity);

                    // Remove(entity);
                    return Convert.ToBoolean(this.context.SaveChanges());
                }
            }
            else
            {
                throw new BllValidationException($"This Venue {entity.Name} cannot delete form DB because is not exist", string.Empty);
            }
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        /// <exception cref="BllValidationException">j j
        /// </exception>
        public Venue Create(Venue entity)
        {
            if (!this.IsExists(entity.Name))
            {
                this.context.Venues.Create(entity);
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException($"This City {entity.Name} is alredy exist", "alredy exist");
            }

            return entity;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        /// <exception cref="BllValidationException">
        /// </exception>
        public Venue Update(Venue entity)
        {
            if (this.IsExists(entity.Id))
            {
                // var updateEentitty = Get(entity.Id);

                // updateEentitty = entity;
                this.context.Venues.Update(entity);

                // Context.Context.Entry(entity).State = EntityState.Modified;
                // updateEentitty.Name = entity.Name;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This Venue name:{entity.Name},Id:{entity.Id} cannot Update" + $" in DB because is not exist",
                    string.Empty);
            }

            return entity;
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            return this.context.Venues.GetExist(new FindVenueByNameOrId(id));
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            return this.context.Venues.GetExist(new FindVenueByNameOrId(name));
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        public Venue GetByName(string name)
        {
            return this.context.Venues.GetOne(
                new FindVenueByNameOrId(name),
                new VenueInclude());
        }
    }
}
