﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using Entities;

    using Infrastructure;

    using Interfaces;

    using TicketSaleCore.Models.BLL.Specification.Filter.TicketsSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.DAL.Interfaces;

    /// <summary>
    /// The ordering ticket service.
    /// </summary>
    public class OrderingTicketService : IOrderingTicketService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// The order status service.
        /// </summary>
        private readonly IOrderStatusService orderStatusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderingTicketService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="orderStatusService">
        /// The order status service.
        /// </param>
        public OrderingTicketService(
            IUnitOfWork context,
            IOrderStatusService orderStatusService)
        {
            this.context = context;
            this.orderStatusService = orderStatusService;
        }

        /// <summary>
        /// The confirm to sale ticket.
        /// </summary>
        /// <param name="ticketOrderingId">
        /// The ticket ordering id.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        public Ticket ConfirmToSaleTicket(
            string ticketOrderingId)
        {
            var ticForConfirm = this.GetByName(ticketOrderingId);
            var allWaitingTicket = this.GetByTickets(ticForConfirm.TiketId);
            var confirm = this.orderStatusService.GetByName(nameof(TicketStatus.Confirmed));
            var rejected = this.orderStatusService.GetByName(nameof(TicketStatus.Rejected));
            foreach (var confRejecTicket in allWaitingTicket)
            {
                if (confRejecTicket.Id.ToString() == ticketOrderingId)
                {
                    confRejecTicket.TicketStatus = confirm;
                    this.Update(confRejecTicket);
                }
                else
                {
                    confRejecTicket.TicketStatus = rejected;
                    this.Update(confRejecTicket);
                }
            }

            return ticForConfirm.Tiket;
        }

        /// <summary>
        /// The reject ticket.
        /// </summary>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">.j j.
        /// </exception>
        public Ticket RejectTicket()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool Delete(OrderingTicket entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingTicket"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public OrderingTicket Create(OrderingTicket entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingTicket"/>.
        /// </returns>
        public OrderingTicket Update(OrderingTicket entity)
        {
            this.context.OrderingTickets.Update(entity);
            this.context.SaveChanges();
            return entity;
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingTicket"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public OrderingTicket GetById(int? id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingTicket"/>.
        /// </returns>
        public OrderingTicket GetByName(string name)
        {
            var qq = new FindOrderingTicketById(name);

            return this.context.OrderingTickets.GetOne(qq, new OrderingTicketInclude());
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IEnumerable<OrderingTicket> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool IsExists(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool IsExists(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get by tickets.
        /// </summary>
        /// <param name="idTicket">
        /// The id ticket.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<OrderingTicket> GetByTickets(int? idTicket)
        {
            return this.context.OrderingTickets.GetByFilter(
                 filter: new FindAllWaitingToConfirmTicketsByTicketName(idTicket),
               includes: new OrderingTicketInclude());
        }
    }
}
