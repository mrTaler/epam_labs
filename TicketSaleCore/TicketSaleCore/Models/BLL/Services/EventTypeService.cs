﻿namespace TicketSaleCore.Models.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using Entities;

    using Infrastructure;

    using Interfaces;

    using TicketSaleCore.Models.BLL.Specification.Filter.EventsTypeSpecification;
    using TicketSaleCore.Models.BLL.Specification.Include;
    using TicketSaleCore.Models.DAL.Interfaces;

    /// <summary>
    /// The event type service.
    /// </summary>
    public class EventTypeService : IEventTypeService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IUnitOfWork context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventTypeService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public EventTypeService(IUnitOfWork context)
        {
            this.context = context;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="EventsType"/>.
        /// </returns>
        public EventsType GetById(int? id)
        {
            var idSpec = new FindEventsTypeByNameOrId(id);
            return this.context.EventsTypes.GetOne(
                idSpec, 
                new EventsTypeInclude());
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="EventsType"/>.
        /// </returns>
        public EventsType GetByName(string name)
        {
            var nameSpec = new FindEventsTypeByNameOrId(name);
            return this.context.EventsTypes.GetOne(nameSpec, new EventsTypeInclude());
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<EventsType> GetAll()
        {
            return this.context.EventsTypes.GetAll(null, new EventsTypeInclude());
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="EventsType"/>.
        /// </returns>
        /// <exception cref="BllValidationException">if  alredy exist
        /// </exception>
        public EventsType Create(EventsType entity)
        {
            if (!this.IsExists(entity.NameEventsType))
            {
                this.context.EventsTypes.Create(entity);
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException($"This EventsType {entity.NameEventsType} is alredy exist", "alredy exist");
            }

            return entity;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="BllValidationException">Need cascade
        /// </exception>
        public bool Delete(EventsType entity)
        {
            if (this.IsExists(entity.Id))
            {
                if (entity.Events.Count != 0)
                {
                    throw new BllValidationException(
                        $"This EventsType {entity.NameEventsType} cannot delete"
                        + $" form DB because need cascade delete",
                        "Need cascade");
                }
                else
                {
                    this.context.EventsTypes.Delete(entity);
                    return Convert.ToBoolean(this.context.SaveChanges());
                }
            }
            else
            {
                throw new BllValidationException(
                    $"This EventsTypeCity {entity.NameEventsType} " + $"cannot delete form DB because is not exist",
                    string.Empty);
            }
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(int id)
        {
            var idExist = new FindEventsTypeByNameOrId(id);
            return this.context.EventsTypes.GetExist(idExist);
        }

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsExists(string name)
        {
            var idExist = new FindEventsTypeByNameOrId(name);
            return this.context.EventsTypes.GetExist(idExist);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="EventsType"/>.
        /// </returns>
        /// <exception cref="BllValidationException">not exist
        /// </exception>
        public EventsType Update(EventsType entity)
        {
            if (this.IsExists(entity.Id))
            {
                var updateEentitty = this.GetById(entity.Id);
                updateEentitty.NameEventsType = entity.NameEventsType;
                this.context.SaveChanges();
            }
            else
            {
                throw new BllValidationException(
                    $"This EventsType name:{entity.NameEventsType},Id:{entity.Id} cannot Update"
                    + $" in DB because is not exist",
                    string.Empty);
            }

            return entity;
        }
    }
}
