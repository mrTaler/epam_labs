﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using System.Collections.Generic;
    using System.Security.Claims;

    using Entities;

    using Features.Tickets.Tickets.ViewModels;

    using Infrastructure;

    /// <summary>
    /// The TicketsService interface.
    /// </summary>
    public interface ITicketsService : IBllService<Ticket>
    {
        /// <summary>
        /// Get all event tickets 
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <returns>event ticket list</returns>
        TicketIndexViewModel GetTicketByEvent(int? eventId);

        /// <summary>
        /// Get all User Ticket
        /// </summary>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>  IEnumerable Ticket  </returns>
        IEnumerable<Ticket> GetAllUserTickets(string userId, TicketStatus param);

        /// <summary>
        /// The get ticket by event w buyer.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <param name="buyerId">
        /// The buyer Id.
        /// </param>
        /// <returns>
        /// The <see cref="TicketIndexViewModel"/>.
        /// </returns>
        TicketIndexViewModel GetTicketByEventWBuyer(int? eventId, ClaimsPrincipal buyerId);

        /// <summary>
        /// The is solded.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsSolded(Ticket entity);

        /// <summary>
        /// The get waiting confomition tickets.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<OrderingTicket> GetWaitingConfomitionTickets(string userId);

        /// <summary>
        /// The create new ticket.
        /// </summary>
        /// <param name="tiket">
        /// The tiket.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        Ticket CreateNewTicket(Ticket tiket, ClaimsPrincipal user);

        /// <summary>
        /// The get by seller notes.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        Ticket GetByName(string name);
    }
}
