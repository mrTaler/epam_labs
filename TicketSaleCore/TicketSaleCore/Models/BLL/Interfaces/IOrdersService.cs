﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using System.Collections.Generic;
    using System.Security.Claims;

    using Entities;

    using Infrastructure;

    /// <summary>
    /// The OrdersService interface.
    /// </summary>
    public interface IOrdersService : IBllService<Order>
    {
        /// <summary>
        /// The new order with tickets.
        /// </summary>
        /// <param name="orderingTickets">
        /// The ordering tickets.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Order> NewOrderWithTickets(
            IEnumerable<Ticket> orderingTickets,
            ClaimsPrincipal user);

        /// <summary>
        /// The new order with ticket.
        /// </summary>
        /// <param name="orderingTickets">
        /// The ordering tickets.
        /// </param>
        /// <returns>
        /// The <see cref="Order"/>.
        /// </returns>
        Order NewOrderWithTicket(IEnumerable<Ticket> orderingTickets);

        /// <summary>
        /// The get user orders.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Order> GetUserOrders(string id);

        /// <summary>
        /// The get new user orders.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Order> GetNewUserOrders(ClaimsPrincipal user);

        /// <summary>
        /// The is in order.
        /// </summary>
        /// <param name="ticket">
        /// The ticket.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsInOrder(Ticket ticket, ClaimsPrincipal user);
    }
}
