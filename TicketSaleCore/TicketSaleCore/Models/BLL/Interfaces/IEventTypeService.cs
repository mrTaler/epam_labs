﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using Entities;

    using Infrastructure;

    /// <summary>
    /// The EventTypeService interface.
    /// </summary>
    public interface IEventTypeService : IBllService<EventsType>
    {
    }
}
