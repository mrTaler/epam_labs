﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using DAL.Specifications.Interfaces;

    using Entities;

    using Infrastructure;

    using TicketSaleCore.Models.BLL.DTO;

    /// <summary>
    /// The EventService interface.
    /// </summary>
    public interface IEventService : IBllService<Event>
    {
        /// <summary>
        /// The get all event with tickets.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Event> GetAllEventWithTickets(ClaimsPrincipal user);

        /// <summary>
        /// The get all event with tickets async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<Event>> GetAllEventWithTicketsAsync(ISpecification<Event> evenSpecification);

        /// <summary>
        /// The get all event with tickets with spec async.
        /// </summary>
        /// <param name="findSpecificaion">
        /// The find specificaion.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<Event>> GetAllEventWithTicketsWithSpecAsync(
            ISpecification<Event> findSpecificaion);

        /// <summary>
        /// The get count async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<int> GetCountAsync();

        /// <summary>
        /// The get for edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="EventsDTO"/>.
        /// </returns>
        EventsDTO GetForEdit(int? id);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="EventsDTO"/>.
        /// </returns>
        EventsDTO Update(EventsDTO entity);
    }
}
