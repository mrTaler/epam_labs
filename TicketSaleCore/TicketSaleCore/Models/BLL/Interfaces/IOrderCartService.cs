﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using System.Collections.Generic;
    using System.Security.Claims;

    using Entities;

    using Infrastructure;

    /// <summary>
    /// The OrderCartService interface.
    /// </summary>
    public interface IOrderCartService : IBllService<OrderingCart>
    {
        /// <summary>
        /// The add to cart.
        /// </summary>
        /// <param name="ticketId">
        /// The ticket Id.
        /// </param>
        /// <param name="user">
        /// The User.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        Ticket AddToCart(int? ticketId, ClaimsPrincipal user);
       

        /// <summary>
        /// The get user cart.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="OrderingCart"/>.
        /// </returns>
        OrderingCart GetUserCart(ClaimsPrincipal user);

        /// <summary>
        /// The is ticket exist.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="user">
        /// The User.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsTicketExist(Ticket entity, ClaimsPrincipal user);

        /// <summary>
        /// The make orders.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Order> MakeOrders(ClaimsPrincipal user);

        /// <summary>
        /// The clean cart.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CleanCart(ClaimsPrincipal user);
    }
}
