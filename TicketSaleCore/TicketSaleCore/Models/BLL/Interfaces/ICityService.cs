﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using Entities;

    using Infrastructure;

    /// <summary>
    /// Interface for work with Cities
    /// </summary>
    public interface ICityService : IBllService<City>
    {
    }
}
