﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using Entities;

    using Infrastructure;

    /// <summary>
    /// The VenuesService interface.
    /// </summary>
    public interface IVenuesService : IBllService<Venue>
    {
    }
}
