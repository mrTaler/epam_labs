﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The EventServiceNew interface.
    /// </summary>
    public interface IEventServiceNew
    {
        /// <summary>
        /// The get all event with tickets async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<Event>> GetAllEventWithTicketsAsync(ClaimsPrincipal user);
    }
}