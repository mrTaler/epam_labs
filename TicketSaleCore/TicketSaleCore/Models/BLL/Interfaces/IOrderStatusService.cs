﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using Entities;

    using Infrastructure;

    /// <summary>
    /// The OrderStatusService interface.
    /// </summary>
    public interface IOrderStatusService : IBllService<OrderStatus>
    {
    }
}
