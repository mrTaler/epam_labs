﻿namespace TicketSaleCore.Models.BLL.Interfaces
{
    using System.Collections.Generic;

    using Entities;

    using Infrastructure;

    /// <summary>
    /// The OrderingTicketService interface.
    /// </summary>
    public interface IOrderingTicketService : IBllService<OrderingTicket>
    {
        /// <summary>
        /// The confirm to sale ticket.
        /// </summary>
        /// <param name="ticketOrderingId">
        /// The ticket ordering id.
        /// </param>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        Ticket ConfirmToSaleTicket(string ticketOrderingId);

        /// <summary>
        /// The reject ticket.
        /// </summary>
        /// <returns>
        /// The <see cref="Ticket"/>.
        /// </returns>
        Ticket RejectTicket();

        /// <summary>
        /// The get by tickets.
        /// </summary>
        /// <param name="idTicket">
        /// The id ticket.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<OrderingTicket> GetByTickets(int? idTicket);
    }
}