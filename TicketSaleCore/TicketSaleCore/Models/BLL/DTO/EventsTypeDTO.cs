﻿namespace TicketSaleCore.Models.BLL.DTO
{
    public class EventsTypeDTO
    {
        public int Id
        {
            get; set;
        }
        public string NameEventsType
        {
            get; set;
        }
    }
}
