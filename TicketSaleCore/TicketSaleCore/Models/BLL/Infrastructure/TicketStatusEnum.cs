﻿namespace TicketSaleCore.Models.BLL.Infrastructure
{
    /// <summary>
    /// The ticket status.
    /// </summary>
    public enum TicketStatus
    {
        /// <summary>
        /// The selling tickets.
        /// </summary>
        SellingTickets,

        /// <summary>
        /// The waiting confomition.
        /// </summary>
        WaitingConfomition,

        /// <summary>
        /// The sold.
        /// </summary>
        Sold,

        /// <summary>
        /// The confirmed.
        /// </summary>
        Confirmed,

        /// <summary>
        /// The rejected.
        /// </summary>
        Rejected
    }
}
