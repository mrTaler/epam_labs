﻿namespace TicketSaleCore.Models.BLL.Infrastructure
{
    /// <inheritdoc />
    /// <summary>
    /// The BllService interface.
    /// </summary>
    /// <typeparam name="T">db entity</typeparam>
    public interface IBllService<T> : IEditableBllService<T>, IReadableBllService<T>
    {
        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}
