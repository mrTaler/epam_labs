﻿namespace TicketSaleCore.Models.BLL.Infrastructure
{
    using System;

    /// <summary>
    /// The bll validation exception.
    /// </summary>
    public class BllValidationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BllValidationException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="prop">
        /// The prop.
        /// </param>
        public BllValidationException(string message, string prop) : base(message)
        {
            this.Property = prop;
        }

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        public string Property
        {
            get; protected set;
        }
    }
}
