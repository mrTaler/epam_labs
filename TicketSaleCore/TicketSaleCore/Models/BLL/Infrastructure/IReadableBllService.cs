﻿namespace TicketSaleCore.Models.BLL.Infrastructure
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for read data
    /// </summary>
    /// <typeparam name="T">Db Class</typeparam>
    public interface IReadableBllService<out T>
    {
        /// <summary>
        /// Get Data by id
        /// </summary>
        /// <param name="id">Id to find</param>
        /// <returns>Db Class</returns>
        T GetById(int? id);

        /// <summary>
        /// Get Data by name
        /// </summary>
        /// <param name="name">Name to find</param>
        /// <returns>Db Class</returns>
        T GetByName(string name);

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsExists(int id);

        /// <summary>
        /// The is exists.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsExists(string name);
    }
}
