﻿namespace TicketSaleCore.Models.BLL.Infrastructure
{
    /// <summary>
    /// interface for edit service in BLL
    /// </summary>
    /// <typeparam name="T">Db Class</typeparam>
    public interface IEditableBllService<T>
    {
        /// <summary>
        /// Delete entry T from DAL
        /// </summary>
        /// <param name="entity">Db Class</param>
        /// <returns>Delete Db Class Sucsecs</returns>
        bool Delete(T entity);

        /// <summary>
        /// Add entry T to DAL 
        /// </summary>
        /// <param name="entity">Db Class</param>
        /// <returns>New Db Class</returns>
        T Create(T entity);

        /// <summary>
        /// Update entry in DAL
        /// </summary>
        /// <param name="entity">Db Class</param>
        /// <returns>Upadated Db Class</returns>
        T Update(T entity);
    }
}
