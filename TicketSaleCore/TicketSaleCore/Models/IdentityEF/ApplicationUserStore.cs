﻿namespace TicketSaleCore.Models.IdentityEF
{
    using System.Security.Claims;

    using DAL._Ef;

    using Entities;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    public class ApplicationUserStore : UserStore<AppUser, ApplicationRole, ApplicationContext, string, AppUserClaim,
        ApplicationUserRole, ApplicationUserLogin, ApplicationUserToken, ApplicationRoleClaim>
    {
        public ApplicationUserStore(ApplicationContext context, IdentityErrorDescriber describer = null)
            : base(context, describer)
        {
        }

        protected override ApplicationUserRole CreateUserRole(AppUser user, ApplicationRole role)
        {
            return new ApplicationUserRole()
                       {
                           UserId = user.Id,
                           RoleId = role.Id
                       };
        }

        protected override AppUserClaim CreateUserClaim(AppUser user, Claim claim)
        {
            var userClaim = new AppUserClaim() { UserId = user.Id };
            userClaim.InitializeFromClaim(claim);
            return userClaim;
        }

        protected override ApplicationUserLogin CreateUserLogin(AppUser user, UserLoginInfo login)
        {
            return new ApplicationUserLogin()
                       {
                           UserId = user.Id,
                           ProviderKey = login.ProviderKey,
                           LoginProvider = login.LoginProvider,
                           ProviderDisplayName = login.ProviderDisplayName
                       };
        }

        protected override ApplicationUserToken CreateUserToken(AppUser user, string loginProvider, string name, string value)
        {
            return new ApplicationUserToken()
                       {
                           UserId = user.Id,
                           LoginProvider = loginProvider,
                           Name = name,
                           Value = value
                       };
        }
    }
}