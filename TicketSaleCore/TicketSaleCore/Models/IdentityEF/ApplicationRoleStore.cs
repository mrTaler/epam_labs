﻿namespace TicketSaleCore.Models.IdentityEF
{
    using System.Security.Claims;

    using DAL._Ef;

    using Entities;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    public class ApplicationRoleStore : RoleStore<ApplicationRole, ApplicationContext, string, ApplicationUserRole,
        ApplicationRoleClaim>
    {
        public ApplicationRoleStore(ApplicationContext context, IdentityErrorDescriber describer = null)
            : base(context, describer)
        {
        }

        protected override ApplicationRoleClaim CreateRoleClaim(ApplicationRole role, Claim claim)
        {
            return new ApplicationRoleClaim()
                       {
                           RoleId = role.Id,
                           ClaimType = claim.Type,
                           ClaimValue = claim.Value
                       };
        }
    }
}