﻿namespace TicketSaleCore.Models.DAL._Memory
{
    using TicketSaleCore.Models.DAL.Interfaces;
    using TicketSaleCore.Models.Entities;

    public class MemoryUnitOfWork : IUnitOfWork
    {
        #region private
        private IRepository<OrderingCart> orderCart;
        private IRepository<City> cites;
        private IRepository<Event> events;
        private IRepository<EventsType> eventsTypes;
        private IRepository<Order> orders;
        private IRepository<OrderStatus> orderStatuses;
        private IRepository<Ticket> tickets;
        private IRepository<Venue> venues;
        private IRepository<AppUser> appUsers;
        private IRepository<OrderingTicket> orderingTickets;
        private IRepository<AppUserClaim> userClaims;
        private IRepository<ApplicationUserLogin> userLogins;
        private IRepository<ApplicationUserRole> userRoles;
        private IRepository<ApplicationUserToken> userTokens;
        private IRepository<ApplicationRole> roles;
        private IRepository<ApplicationRoleClaim> roleClaims;
        #endregion

        public IRepository<OrderingCart> OrderCart
            => this.orderCart ?? (this.orderCart = new MemoryGenericRepository<OrderingCart>());
        public IRepository<City> Cites
            => this.cites ?? (this.cites = new MemoryGenericRepository<City>());
        public IRepository<Event> Events
            => this.events ?? (this.events = new MemoryGenericRepository<Event>());
        public IRepository<EventsType> EventsTypes
            => this.eventsTypes ?? (this.eventsTypes = new MemoryGenericRepository<EventsType>());
        public IRepository<Order> Orders
            => this.orders ?? (this.orders = new MemoryGenericRepository<Order>());
        public IRepository<OrderStatus> OrderStatuses
            => this.orderStatuses ?? (this.orderStatuses = new MemoryGenericRepository<OrderStatus>());
        public IRepository<Ticket> Tickets
            => this.tickets ?? (this.tickets = new MemoryGenericRepository<Ticket>());
        public IRepository<Venue> Venues
            => this.venues ?? (this.venues = new MemoryGenericRepository<Venue>());
        public IRepository<AppUser> AppUsers
            => this.appUsers ?? (this.appUsers = new MemoryGenericRepository<AppUser>());
        public IRepository<OrderingTicket> OrderingTickets
            => this.orderingTickets ?? (this.orderingTickets = new MemoryGenericRepository<OrderingTicket>());
        public IRepository<AppUserClaim> UserClaims
            => this.userClaims ?? (this.userClaims = new MemoryGenericRepository<AppUserClaim>());
        public IRepository<ApplicationUserLogin> UserLogins
            => this.userLogins ?? (this.userLogins = new MemoryGenericRepository<ApplicationUserLogin>());
        public IRepository<ApplicationUserRole> UserRoles
            => this.userRoles ?? (this.userRoles = new MemoryGenericRepository<ApplicationUserRole>());
        public IRepository<ApplicationUserToken> UserTokens
            => this.userTokens ?? (this.userTokens = new MemoryGenericRepository<ApplicationUserToken>());
        public IRepository<ApplicationRole> Roles
            => this.roles ?? (this.roles = new MemoryGenericRepository<ApplicationRole>());
        public IRepository<ApplicationRoleClaim> RoleClaims
            => this.roleClaims ?? (this.roleClaims = new MemoryGenericRepository<ApplicationRoleClaim>());

        public int SaveChanges()
        {
            return 1;
        }

        public void Dispose()
        {
        }
    }
}
