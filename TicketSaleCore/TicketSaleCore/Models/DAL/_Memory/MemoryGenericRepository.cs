﻿namespace TicketSaleCore.Models.DAL._Memory

{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using TicketSaleCore.Models.DAL.Interfaces;
    using TicketSaleCore.Models.DAL.Specifications.Interfaces;
    using TicketSaleCore.Models.DAL.Specifications.OrderSpecification;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// Memory repository based on DbSet
    /// </summary>
    /// <typeparam name="T">Class</typeparam>
    public class MemoryGenericRepository<T> : IRepository<T>
        where T : class
    {

        private List<T> list = new List<T>();

        public void Create(T entity, string createdBy = null)
        {
            this.list.Add(entity);
        }

        public void Delete(object id, string deleteBy = null)
        {
            throw new NotImplementedException();
        }

        public void Delete(T entity, string deleteBy = null)
        {
            this.list.Remove(entity);
        }

        public IEnumerable<T> GetAll(
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            List<T> ret = orderBy != null ? new List<T>(this.list.OrderBy(orderBy)) : new List<T>(this.list);

            return ret;
        }

        public Task<IEnumerable<T>> GetAllAsync(IOrderSpecification<T> orderBy = null, IIncludeSpecification<T> includeSpecification = null)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetAsync(ISpecification<T> specification = null, IOrderSpecification<T> orderby = null, IIncludeSpecification<T> includeSpecification = null)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetCountAsync(ISpecification<T> filter = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetExistAsync(ISpecification<T> filter = null)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetOneAsync(ISpecification<T> filter = null, IIncludeSpecification<T> includeSpecification = null)
        {
            throw new NotImplementedException();
        }




        public IEnumerable<T> GetByFilter(
            ISpecification<T> filter = null,
            IOrderSpecification<T> orderBy = null, 
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = this.list.Where(filter).OrderBy(orderBy);
            return ret;
        }

        public int GetCount(ISpecification<T> filter = null)
        {
            var ret = this.list.Where(filter).Count();
            return ret;
        }

        public bool GetExist(ISpecification<T> filter = null)
        {
            var ret = this.list.Where(filter).Any();
            return ret;
        }

        public T GetOne(ISpecification<T> filter = null, IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = this.list.Where(filter).First();
            return ret;
        }

        public T GetOneNoTr(ISpecification<T> filter = null, IIncludeSpecification<T> includeSpecification = null)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity, string modifiedBy = null)
        {
            this.list.Add(entity);
        }
    }
}
