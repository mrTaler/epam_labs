﻿namespace TicketSaleCore.Models.DAL.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;

    /// <summary>
    /// The AsyncRepository interface.
    /// </summary>
    /// <typeparam name="T">Ef Class
    /// </typeparam>
    public interface IAsyncRepository<T> where T : class
    {
        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="filter">
        /// The specification.
        /// </param>
        /// <param name="orderBy">
        /// The orderby.
        /// </param>
        /// <param name="includes">
        /// The include specification.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<T>> GetAsync(
            ISpecification<T> filter = null,
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includes = null);
       

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="orderBy">
        /// The order by.
        /// </param>
        /// <param name="includes">
        /// The include Specification.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<T>> GetAllAsync(
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includes = null);

        /// <summary>
        /// The get one async.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="includes">
        /// The include specification.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<T> GetOneAsync(
            ISpecification<T> filter = null,
            IIncludeSpecification<T> includes = null);

        /// <summary>
        /// The get count async.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<int> GetCountAsync(
            ISpecification<T> filter = null);

        /// <summary>
        /// The get exist async.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> GetExistAsync(
            ISpecification<T> filter = null);
    }
}