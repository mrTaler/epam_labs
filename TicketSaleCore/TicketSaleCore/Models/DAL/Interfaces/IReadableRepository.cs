﻿namespace TicketSaleCore.Models.DAL.Interfaces
{
    using System.Collections.Generic;

    using Specifications.Interfaces;

    /// <summary>
    /// The ReadableRepository interface.
    /// </summary>
    /// <typeparam name="T"> db Entity
    /// </typeparam>
    public interface IReadableRepository<T> where T : class
    {
        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="orderBy">
        /// The order by.
        /// </param>
        /// <param name="includes">
        /// The include specification.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<T> GetAll(
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includes = null);

        /// <summary>
        /// The get by Filter
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="orderBy">
        /// The order by.
        /// </param>
        /// <param name="includes">
        /// The include specification.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<T> GetByFilter(
            ISpecification<T> filter = null,
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includes = null);

        /// <summary>
        /// The get one by filter
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="includes">
        /// The include specification.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        T GetOne(
            ISpecification<T> filter = null,
             IIncludeSpecification<T> includes = null);

        /// <summary>
        /// The get one no tracing.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="includes">
        /// The include specification.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        [System.Obsolete("Not use this method (noTracking implimented in Specifications")]
        T GetOneNoTr(
            ISpecification<T> filter = null,
            IIncludeSpecification<T> includes = null);

        /// <summary>
        /// The get count.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int GetCount(
            ISpecification<T> filter = null);

        /// <summary>
        /// The get exist.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool GetExist(
            ISpecification<T> filter = null);
    }
}
