﻿namespace TicketSaleCore.Models.DAL.Interfaces
{
    using System;

    using TicketSaleCore.Models.Entities;

    /// <summary>
    /// The UnitOfWork interface.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        IRepository<OrderingCart> OrderCart { get; }
        IRepository<City> Cites { get; }// 1
        IRepository<Event> Events { get; }// 2
        IRepository<EventsType> EventsTypes { get; }// 3
        IRepository<Order> Orders { get; }// 4
        IRepository<OrderStatus> OrderStatuses { get; }// 5
        IRepository<Ticket> Tickets { get; }// 6
        IRepository<Venue> Venues { get; }// 7
        IRepository<AppUser> AppUsers { get; }// 9

        IRepository<OrderingTicket> OrderingTickets { get; }

        IRepository<AppUserClaim> UserClaims { get; }

        IRepository<ApplicationUserLogin> UserLogins { get; }
        IRepository<ApplicationUserRole> UserRoles { get; }
        IRepository<ApplicationUserToken> UserTokens { get; }
        IRepository<ApplicationRole> Roles { get; }
        IRepository<ApplicationRoleClaim> RoleClaims { get; }
    }
}