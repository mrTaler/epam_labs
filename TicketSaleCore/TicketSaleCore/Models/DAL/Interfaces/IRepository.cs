﻿namespace TicketSaleCore.Models.DAL.Interfaces
{
    /// <summary>
    /// The Repository interface.
    /// </summary>
    /// <typeparam name="T">db Entities
    /// </typeparam>
    public interface IRepository<T> : IReadableRepository<T>, IAsyncRepository<T>
        where T : class
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        void Create(T entity, string createdBy = null);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="modifiedBy">
        /// The modified by.
        /// </param>
        void Update(T entity, string modifiedBy = null);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="deleteBy">
        /// The delete by.
        /// </param>
        void Delete(object id, string deleteBy = null);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="deleteBy">
        /// The delete by.
        /// </param>
        void Delete(T entity, string deleteBy = null);
    }
}
