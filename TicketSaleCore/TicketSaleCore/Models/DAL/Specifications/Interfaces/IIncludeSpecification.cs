namespace TicketSaleCore.Models.DAL.Specifications.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The specification.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public interface IIncludeSpecification<T>
    {
        List<Func<IQueryable<T>, IQueryable<T>>> Includes { get; }
        void AddInclude(Func<IQueryable<T>, IQueryable<T>> includeExpression);
    }
}
