namespace TicketSaleCore.Models.DAL.Specifications.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;

    public class ThenBySpecification<T> : IOrderSpecification<T>
    {
        public List<IOrderSpecification<T>> OrderList;

        public ThenBySpecification(List<IOrderSpecification<T>> orderList, IOrderSpecification<T> right)
        {
            this.OrderList = orderList;
            this.OrderList.Add(right);
        }

        public ThenBySpecification<T> ThenBy(IOrderSpecification<T> other)
        {
            return new ThenBySpecification<T>(this.OrderList, other);
        }

        public IOrderedQueryable<T> Invoke(IQueryable<T> query)
        {
            var orderedQuery = this.OrderList[0].Invoke(query);
            for (var i = 1; i < this.OrderList.Count; i++)
            {
                orderedQuery = this.OrderList[i].Invoke(orderedQuery);
            }

            return orderedQuery;
        }

        public IOrderedQueryable<T> Invoke(IOrderedQueryable<T> query)
        {
            return this.OrderList.Aggregate(query, (current, order) => order.Invoke(current));
        }

        public IOrderedEnumerable<T> Invoke(IEnumerable<T> collection)
        {
            var orderedCollection = this.OrderList[0].Invoke(collection);
            for (var i = 1; i < this.OrderList.Count; i++)
            {
                orderedCollection = this.OrderList[i].Invoke(orderedCollection);
            }

            return orderedCollection;
        }

        public IOrderedEnumerable<T> Invoke(IOrderedEnumerable<T> collection)
        {
            return this.OrderList.Aggregate(collection, (current, order) => order.Invoke(current));
        }
    }
}
