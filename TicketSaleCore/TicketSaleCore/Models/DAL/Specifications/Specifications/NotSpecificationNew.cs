﻿namespace TicketSaleCore.Models.DAL.Specifications.Specifications
{
    using System;
    using System.Linq.Expressions;

    using TicketSaleCore.Models.DAL.Specifications.Interfaces;

    public class NotSpecification<T> : Specification<T>
        where T : class
    {
        private readonly ISpecification<T> other;

        public NotSpecification(ISpecification<T> other, int? skip, int? take)
        {
            this.other = other;
            if (skip.HasValue) Skip(skip.Value);
            if (take.HasValue) Take(take.Value);
        }

        public override Expression<Func<T, bool>> AsExpression()
        {
            var expression = this.other.AsExpression();

            var notExpression = Expression.Not(expression.Body);

            return Expression.Lambda<Func<T, bool>>(notExpression, expression.Parameters);
        }
    }
}