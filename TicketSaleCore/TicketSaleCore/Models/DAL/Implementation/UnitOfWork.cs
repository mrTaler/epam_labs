﻿namespace TicketSaleCore.Models.DAL.Implementation
{
    using System;
    using System.Collections.Generic;

    using TicketSaleCore.Models.DAL.Interfaces;
    using TicketSaleCore.Models.DAL._Ef;
    using TicketSaleCore.Models.Entities;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext context;
        private bool isDisposed;
        public UnitOfWork(ApplicationContext context)
        {
            this.context = context;
        }

        #region private
        private IRepository<OrderingCart> orderCart;
        private IRepository<City> cites;
        private IRepository<Event> events;
        private IRepository<EventsType> eventsTypes;
        private IRepository<Order> orders;
        private IRepository<OrderStatus> orderStatuses;
        private IRepository<Ticket> tickets;
        private IRepository<Venue> venues;
        private IRepository<AppUser> appUsers;
        private IRepository<OrderingTicket> orderingTickets;
        private IRepository<AppUserClaim> userClaims;
        private IRepository<ApplicationUserLogin> userLogins;
        private IRepository<ApplicationUserRole> userRoles;
        private IRepository<ApplicationUserToken> userTokens;
        private IRepository<ApplicationRole> roles;
        private IRepository<ApplicationRoleClaim> roleClaims;
        #endregion

        public IRepository<OrderingCart> OrderCart
            => this.orderCart ?? (this.orderCart = new EfGenericRepository<OrderingCart>(this.context));
        public IRepository<City> Cites
            => this.cites ?? (this.cites = new EfGenericRepository<City>(this.context));
        public IRepository<Event> Events
            => this.events ?? (this.events = new EfGenericRepository<Event>(this.context));
        public IRepository<EventsType> EventsTypes
            => this.eventsTypes ?? (this.eventsTypes = new EfGenericRepository<EventsType>(this.context));
        public IRepository<Order> Orders
            => this.orders ?? (this.orders = new EfGenericRepository<Order>(this.context));
        public IRepository<OrderStatus> OrderStatuses
            => this.orderStatuses ?? (this.orderStatuses = new EfGenericRepository<OrderStatus>(this.context));
        public IRepository<Ticket> Tickets
            => this.tickets ?? (this.tickets = new EfGenericRepository<Ticket>(this.context));
        public IRepository<Venue> Venues
            => this.venues ?? (this.venues = new EfGenericRepository<Venue>(this.context));
        public IRepository<AppUser> AppUsers
            => this.appUsers ?? (this.appUsers = new EfGenericRepository<AppUser>(this.context));
        public IRepository<OrderingTicket> OrderingTickets
            => this.orderingTickets ?? (this.orderingTickets = new EfGenericRepository<OrderingTicket>(this.context));
        public IRepository<AppUserClaim> UserClaims
            => this.userClaims ?? (this.userClaims = new EfGenericRepository<AppUserClaim>(this.context));
        public IRepository<ApplicationUserLogin> UserLogins
            => this.userLogins ?? (this.userLogins = new EfGenericRepository<ApplicationUserLogin>(this.context));
        public IRepository<ApplicationUserRole> UserRoles
            => this.userRoles ?? (this.userRoles = new EfGenericRepository<ApplicationUserRole>(this.context));
        public IRepository<ApplicationUserToken> UserTokens
            => this.userTokens ?? (this.userTokens = new EfGenericRepository<ApplicationUserToken>(this.context));
        public IRepository<ApplicationRole> Roles
            => this.roles ?? (this.roles = new EfGenericRepository<ApplicationRole>(this.context));
        public IRepository<ApplicationRoleClaim> RoleClaims
            => this.roleClaims ?? (this.roleClaims = new EfGenericRepository<ApplicationRoleClaim>(this.context));

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing)
                {
                    this.context.Dispose();
                }
            }

            this.isDisposed = true;
        }


        public int SaveChanges()
        {

            return this.context.SaveChanges();
        }
    }
}
