﻿namespace TicketSaleCore.Models.DAL.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Interfaces;

    using Microsoft.EntityFrameworkCore;

    using Specifications.Interfaces;

    using Specifications.OrderSpecification;
    using Specifications.Specifications;

    /// <inheritdoc />
    /// <summary>
    /// The ef generic repository.
    /// </summary>
    /// <typeparam name="T">db Entities
    /// </typeparam>
    public class EfGenericRepository<T> : IRepository<T> 
        where T : class
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly DbContext context;

        /// <summary>
        /// Database table
        /// </summary>
        private readonly DbSet<T> dbSetsSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="EfGenericRepository{T}"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public EfGenericRepository(DbContext context)
        {
            this.context = context;
            dbSetsSet = context.Set<T>();
        }

        #region CUD method

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        public virtual void Create(T entity, string createdBy = null)
        {
            dbSetsSet.Add(entity);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="modifiedBy">
        /// The modified by.
        /// </param>
        public virtual void Update(T entity, string modifiedBy = null)
        {
            dbSetsSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="deleteBy">
        /// The delete by.
        /// </param>
        public virtual void Delete(object id, string deleteBy = null)
        {
            T entity = dbSetsSet.Find(id);
            Delete(entity);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="deleteBy">
        /// The delete by.
        /// </param>
        public virtual void Delete(T entity, string deleteBy = null)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSetsSet.Attach(entity);
            }

            dbSetsSet.Remove(entity);
        }

        #endregion

        /// <summary>
        /// The get queryable.
        /// </summary>
        /// <param name="filter">
        /// The filter. 
        /// </param>
        /// <param name="sort">
        /// The order by. 
        /// </param>
        /// <param name="includeSpecification">
        /// The include Specification.
        /// </param>
        /// <param name="skip">
        /// The skip. 
        /// </param>
        /// <param name="take">
        /// The take. 
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        protected virtual IQueryable<T> GetQueryable(
            ISpecification<T> filter = null,
            IOrderSpecification<T> sort = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            IQueryable<T> query = dbSetsSet;

            if (includeSpecification != null)
            {
                foreach (var includeItem in includeSpecification.Includes)
                {
                    query = includeItem(query);
                }
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (sort != null)
            {
                query = query.OrderBy(sort);
            }

            return query;
        }

        #region Async

        /// <inheritdoc />
        public async Task<IEnumerable<T>> GetAsync(
            ISpecification<T> specification = null,
            IOrderSpecification<T> orderby = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = GetQueryable(
                filter: specification,
                sort: orderby,
                includeSpecification: includeSpecification);
            return await ret.AsNoTracking().ToListAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<T>> GetAllAsync(
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = await GetQueryable(
                          filter: null,
                          sort: orderBy,
                          includeSpecification: includeSpecification).AsNoTracking().ToListAsync();
            return ret;
        }

     /// <inheritdoc />
        public async Task<T> GetOneAsync(
            ISpecification<T> filter = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = await GetQueryable(
                          filter: filter,
                          includeSpecification: includeSpecification).SingleOrDefaultAsync();
            return ret;
        }

        /// <inheritdoc />
        public async Task<int> GetCountAsync(
            ISpecification<T> filter = null)
        {
            var ret = await GetQueryable(filter).CountAsync();
            return ret;
        }

        /// <inheritdoc />
        public async Task<bool> GetExistAsync(
            ISpecification<T> filter = null)
        {
            var ret = await GetQueryable(filter).AnyAsync();
            return ret;
        }

        #endregion


        /// <inheritdoc />
        public IEnumerable<T> GetAll(
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = GetQueryable(
                filter: null,
                sort: orderBy,
                includeSpecification: includeSpecification);
            return ret.ToList();
        }

        /// <inheritdoc />
        public IEnumerable<T> GetByFilter(
            ISpecification<T> filter = null,
            IOrderSpecification<T> orderBy = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = GetQueryable(
                filter: filter,
                sort: orderBy,
                includeSpecification: includeSpecification);
            return ret.AsNoTracking().ToList();
        }

        /// <inheritdoc />
        public T GetOne(
            ISpecification<T> filter = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = GetQueryable(
                filter: filter,
                includeSpecification: includeSpecification);
            return ret.SingleOrDefault();
        }

        /// <inheritdoc />
        public T GetOneNoTr(
            ISpecification<T> filter = null,
            IIncludeSpecification<T> includeSpecification = null)
        {
            var ret = GetQueryable(
                filter: filter,
                includeSpecification: includeSpecification);
            return ret.AsNoTracking().SingleOrDefault();
        }

        /// <inheritdoc />
        public int GetCount(
            ISpecification<T> filter = null)
        {
            var ret = GetQueryable(filter).Count();
            return ret;
        }

        /// <inheritdoc />
        public bool GetExist(
            ISpecification<T> filter = null)
        {
            var ret = GetQueryable(filter).Any();
            return ret;
        }
    }
}
