﻿namespace TicketSaleCore.Models.DAL._Ef
{
    using Entities;

    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The application context.
    /// </summary>
    public class ApplicationContext : IdentityDbContext<AppUser, ApplicationRole, string,
        AppUserClaim,
        ApplicationUserRole,
        ApplicationUserLogin,
        ApplicationRoleClaim, ApplicationUserToken>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationContext"/> class.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<City> Cites { get; set; }

        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<EventsType> EventsTypes { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<Venue> Venues { get; set; }
        public virtual DbSet<AppUser> AppUsers { get; set; }
        public DbSet<OrderingCart> OrderCart { get; set; }
        public DbSet<OrderingTicket> OrderingTickets { get; set; }

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>().HasKey(t => t.Id);

            modelBuilder.Entity<Event>().HasKey(t => t.Id);

            modelBuilder.Entity<Event>().HasOne(t => t.Venue).WithMany(t => t.Events).HasForeignKey(t => t.VenueId);

            modelBuilder.Entity<Event>()
                .HasOne(t => t.EventsType)
                .WithMany(t => t.Events)
                .HasForeignKey(t => t.EventsTypeId);

            modelBuilder.Entity<Order>().HasKey(t => t.Id);

            modelBuilder.Entity<Order>().HasOne(t => t.Buyer).WithMany(t => t.Orders).HasForeignKey(t => t.BuyerId);

            #region Status

            modelBuilder.Entity<OrderStatus>().HasKey(t => t.Id);

            // modelBuilder.Entity<OrderStatus>().HasMany(t => t.OrderTickets)
            // .WithOne(t => t.Status);
            #endregion

            #region Ticket

            modelBuilder.Entity<Ticket>().HasKey(t => t.Id);

            modelBuilder.Entity<Ticket>().HasOne(t => t.Event).WithMany(t => t.Tickets).HasForeignKey(t => t.EventId);

            modelBuilder.Entity<Ticket>().HasOne(t => t.Seller).WithMany(t => t.Tickets).HasForeignKey(t => t.SellerId);

            /*   modelBuilder.Entity<Ticket>().HasOne(t => t.OrderingCart)
                   .WithMany(t => t.Ticket)
                   .HasForeignKey(t => t.OrderingCartId);*/
            #endregion

            #region Venue

            modelBuilder.Entity<Venue>().HasKey(t => t.Id);

            modelBuilder.Entity<Venue>().HasOne(t => t.City).WithMany(t => t.Venues).HasForeignKey(t => t.CityFk);

            #endregion

            #region next feature

            // modelBuilder.AddConfiguration(new CityConfiguration());
            // modelBuilder.AddConfiguration(new EventConfiguration());
            // modelBuilder.AddConfiguration(new OrderConfiguration());
            // modelBuilder.AddConfiguration(new TicketConfiguration());
            // modelBuilder.AddConfiguration(new UserConfiguration());
            // modelBuilder.AddConfiguration(new VenueConfiguration());
            // modelBuilder.AddConfiguration(new StatusConfiguration());.
            #endregion

            modelBuilder.Entity<AppUser>()
                .HasOne(p => p.OrderCart)
                .WithOne(pp => pp.CartOwner)
                .HasForeignKey<OrderingCart>(fk => fk.OwnerId);

            modelBuilder.Entity<City>().ToTable("Cites");

            modelBuilder.Entity<OrderingTicket>()
                .HasOne(p => p.TicketStatus)
                .WithMany(p => p.OrderTickets)
                .HasForeignKey(p => p.TicketStatusId);

            modelBuilder.Entity<OrderingTicket>()
                .HasOne(p => p.Order)
                .WithMany(p => p.OrderTickets)
                .HasForeignKey(p => p.OrderId);

            modelBuilder.Entity<OrderingTicket>()
                .HasOne(p => p.Tiket)
                .WithMany(p => p.OrderTickets)
                .HasForeignKey(p => p.TiketId);

            modelBuilder.Entity<OrderingTicket>().HasKey(p => p.Id);

            modelBuilder.Entity<OrderCartTicket>()
                .HasKey(
                    p => p.Id
                    /*p => new {p.OrderingCartId, p.TicketId}*/);

            modelBuilder.Entity<OrderCartTicket>()
                .HasOne(p => p.OrderingCart)
                .WithMany(p => p.OrderCartTickets)
                .HasForeignKey(p => p.OrderingCartId);

            modelBuilder.Entity<Ticket>()
                .HasMany(p => p.OrderCartTickets)
                .WithOne(p => p.Tickett)
                .HasForeignKey(p => p.TicketId);

            // modelBuilder.Entity<OrderCartTicket>()
            // .HasOne(p => p.Tickett)
            // .WithMany(p => p.OrderCartTickets)
            // .HasForeignKey(p => p.TicketId);
        }

    }
}
