using System.Threading.Tasks;

using AutoMapper;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using TicketSaleCore.Features.Cities.ViewModels;
using TicketSaleCore.Models.BLL.Interfaces;
using TicketSaleCore.Models.Entities;

using BllValidationException = TicketSaleCore.Models.BLL.Infrastructure.BllValidationException;

namespace TicketSaleCore.Features.Cities
{
    using AuthorizationPolit.ResourceBased;

    [Authorize]
    public class CitiesController : Controller
    {
        private readonly ICityService context;
        private IAuthorizationService authorizationService;
        public CitiesController(ICityService context,
                                IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            this.context = context;
        }

        public async Task<IActionResult> Index()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                return this.View(this.context.GetAll());
            }

            return new ChallengeResult();
        }


        public async Task<IActionResult> Details(int? id)
        {
            if (await this.authorizationService
                .AuthorizeAsync(
                        this.User,
                        this.context, 
                Operations.Read))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var city = this.context.GetById(id);
                if (city == null)
                {
                    return this.NotFound();
                }

                return this.View(city);
            }

            return new ChallengeResult();
        }

        #region Edit [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (await this.authorizationService.AuthorizeAsync(
                    this.User,
                    this.context,
                Operations.Update))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                Mapper.Initialize(cfg => cfg.CreateMap<City, CityEditCreateViewModel>());
                var qery = Mapper.Map<City, CityEditCreateViewModel>(this.context.GetById(id));

                if (qery == null)
                {
                    return this.NotFound();
                }

                return this.View(qery);
            }

            return new ChallengeResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CityEditCreateViewModel model)
        {
            if (await this.authorizationService
                .AuthorizeAsync(
                        this.User,
                        this.context,
                Operations.Update))
            {
                if (this.ModelState.IsValid)
                {
                    try
                    {
                        Mapper.Initialize(cfg => cfg.CreateMap<CityEditCreateViewModel, City>());
                        this.context.Update(Mapper.Map<CityEditCreateViewModel, City>(model));
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!this.context.IsExists(model.Id))
                        {
                            return this.NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return this.RedirectToAction("Index");
                }

                return this.View(model);
            }

            return new ChallengeResult();
        }

        #endregion

        #region Create  [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                return this.View();
            }

            return new ChallengeResult();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CityEditCreateViewModel model)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                if (this.ModelState.IsValid)
                {
                    try
                    {
                        Mapper.Initialize(cfg => cfg.CreateMap<CityEditCreateViewModel, City>());
                        this.context.Create(Mapper.Map<CityEditCreateViewModel, City>(model));

                        return this.RedirectToAction("Index");
                    }
                    catch (BllValidationException er)
                    {
                        this.ModelState.AddModelError("Data exist", er.Message);
                        return this.View(model);
                    }
                }

                return this.View(model);
            }

            return new ChallengeResult();
        }

        #endregion

        #region delete 
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Delete))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var cityDelete = this.context.GetById(id);

                if (cityDelete == null)
                {
                    return this.NotFound();
                }

                return this.View(cityDelete);
            }

            return new ChallengeResult();
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Delete))
            {
                try
                {
                    this.context.Delete(this.context.GetById(id));

                    return this.RedirectToAction("Index");
                }
                catch (BllValidationException er)
                {
                    this.ModelState.AddModelError(er.Property, er.Message);
                    return this.View(this.context.GetById(id));
                }
            }

            return new ChallengeResult();
        }

        #endregion
    }
}
