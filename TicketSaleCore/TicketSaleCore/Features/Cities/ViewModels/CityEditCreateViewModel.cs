﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TicketSaleCore.Features.Cities.ViewModels
{
    public class CityEditCreateViewModel
    {
        public int Id
        {
            get; set;
        }
        [BindRequired]
        public string Name
        {
            get; set;
        }
    }
}
