using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TicketSaleCore.Features.Venues
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;

    using Models.BLL.Infrastructure;
    using Models.BLL.Interfaces;
    using Models.Entities;

    // [Authorize]
    [Authorize]
    public class VenuesController : Controller
    {
        private readonly IVenuesService context;

        private readonly ICityService cityService;

        public VenuesController(IVenuesService context, ICityService cityService)
        {
            this.context = context;
            this.cityService = cityService;
        }

        public async Task<IActionResult> Index()
        {
            return this.View(this.context.GetAll());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var venue = this.context.GetById(id);
            if (venue == null)
            {
                return this.NotFound();
            }

            return this.View(venue);
        }

        #region Edit
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            /*  
             *  Mapper.Initialize(cfg => cfg.CreateMap<City, CityEditCreateViewModel>());
             *  var qery = Mapper.Map<City, CityEditCreateViewModel>(this.context.Get(id));
             */
            var qery = this.context.GetById(id);
            if (qery == null)
            {
                return this.NotFound();
            }

            this.ViewBag.cities = new SelectList(
                this.cityService.GetAll(),
                "Id",
                "Name");

            return this.View(qery);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Venue model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    /*   Mapper.Initialize(cfg => cfg.CreateMap<CityEditCreateViewModel, City>());

                       this.context.Update(Mapper.Map<CityEditCreateViewModel, City>(model));*/
                    this.context.Update(model);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!this.context.IsExists(model.Id))
                    {
                        return this.NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return this.RedirectToAction("Index");
            }

            return this.View(model);
        }

        #endregion

        #region Create
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            this.ViewBag.cities = new SelectList(
                this.cityService.GetAll(),
                "Id",
                "Name");

            return this.View();

        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Venue model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    /*Mapper.Initialize(cfg => cfg.CreateMap<CityEditCreateViewModel, City>());
                    this.context.Add(Mapper.Map<CityEditCreateViewModel, City>(model));*/
                    this.context.Create(model);

                    return this.RedirectToAction("Index");
                }
                catch (BllValidationException er)
                {
                    this.ModelState.AddModelError("Data exist", er.Message);
                    return this.View(model);
                }
            }

            return this.View(model);
        }

        #endregion

        #region delete
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var cityDelete = this.context.GetById(id);

            if (cityDelete == null)
            {
                return this.NotFound();
            }

            return this.View(cityDelete);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            try
            {
                this.context.Delete(this.context.GetById(id));

                return this.RedirectToAction("Index");
            }
            catch (BllValidationException er)
            {
                this.ModelState.AddModelError(er.Property, er.Message);
                return this.View(this.context.GetById(id));
            }

        }

        #endregion
    }
}
