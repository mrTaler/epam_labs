namespace TicketSaleCore.Features.Orders
{
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using AutoMapper;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    using Models.BLL.Infrastructure;
    using Models.BLL.Interfaces;

    using OrderStatus.ViewModels;

    public class OrderStatusController : Controller
    {
        private readonly IOrderStatusService context;

        private IAuthorizationService authorizationService;

        public OrderStatusController(IOrderStatusService context,
                                     IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            this.context = context;
        }

        public async Task<IActionResult> Index()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                return this.View(this.context.GetAll());
            }

            return new ChallengeResult();
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var orderStatus = this.context.GetById(id);
                if (orderStatus == null)
                {
                    return this.NotFound();
                }

                return this.View(orderStatus);
            }

            return new ChallengeResult();
        }

        #region Edit [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Update))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                Mapper.Initialize(cfg => cfg.CreateMap<Models.Entities.OrderStatus, OrderStatusEditCreateViewModel>());
                var qery = Mapper.Map<Models.Entities.OrderStatus, OrderStatusEditCreateViewModel>(this.context.GetById(id));

                if (qery == null)
                {
                    return this.NotFound();
                }

                return this.View(qery);
            }

            return new ChallengeResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(OrderStatusEditCreateViewModel model)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Update))
            {
                if (this.ModelState.IsValid)
                {
                    try
                    {
                        Mapper.Initialize(cfg => cfg.CreateMap<OrderStatusEditCreateViewModel, Models.Entities.OrderStatus>());
                        this.context.Update(Mapper.Map<OrderStatusEditCreateViewModel, Models.Entities.OrderStatus>(model));
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!this.context.IsExists(model.Id))
                        {
                            return this.NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return this.RedirectToAction("Index");
                }

                return this.View(model);
            }

            return new ChallengeResult();
        }

        #endregion

        #region Create  [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                return this.View();
            }

            return new ChallengeResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrderStatusEditCreateViewModel model)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                if (this.ModelState.IsValid)
                {
                    try
                    {
                        Mapper.Initialize(cfg => cfg.CreateMap<OrderStatusEditCreateViewModel, Models.Entities.OrderStatus>());
                        this.context.Create(Mapper.Map<OrderStatusEditCreateViewModel, Models.Entities.OrderStatus>(model));

                        return this.RedirectToAction("Index");
                    }
                    catch (BllValidationException er)
                    {
                        this.ModelState.AddModelError("Data exist", er.Message);
                        return this.View(model);
                    }
                }

                return this.View(model);
            }

            return new ChallengeResult();
        }

        #endregion

        #region delete  [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Delete))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var orderStatusDelete = this.context.GetById(id);

                if (orderStatusDelete == null)
                {
                    return this.NotFound();
                }

                return this.View(orderStatusDelete);
            }

            return new ChallengeResult();
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Delete))
            {
                try
                {
                    this.context.Delete(this.context.GetById(id));

                    return this.RedirectToAction("Index");
                }
                catch (BllValidationException er)
                {
                    this.ModelState.AddModelError(er.Property, er.Message);
                    return this.View(this.context.GetById(id));
                }
            }

            return new ChallengeResult();
        }

        #endregion
    }
}
