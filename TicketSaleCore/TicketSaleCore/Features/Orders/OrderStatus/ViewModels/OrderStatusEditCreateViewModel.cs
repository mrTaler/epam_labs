﻿
namespace TicketSaleCore.Features.Orders.OrderStatus.ViewModels
{
    public class OrderStatusEditCreateViewModel
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}