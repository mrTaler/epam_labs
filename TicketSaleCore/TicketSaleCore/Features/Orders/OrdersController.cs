using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TicketSaleCore.Models.BLL.Interfaces;

namespace TicketSaleCore.Features.Orders
{
    using AuthorizationPolit.ResourceBased;

    [Authorize]
    public class OrdersController : Controller
    {
        private IOrdersService context;

        private IAuthorizationService authorizationService;

        public OrdersController(IOrdersService context,
                                IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            this.context = context;
        }

        public async Task<IActionResult> Index(string id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                var qq = this.context.GetUserOrders(id);// .Where(p => p.OrderTickets.Count > 0);// where ned to trans to service
                return this.View(qq);
            }

            return new ChallengeResult();
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var order = this.context.GetById(id);
                if (order == null)
                {
                    return this.NotFound();
                }

                return this.View(order);
            }

            return new ChallengeResult();
        }

        public async Task<IActionResult> NewOrers()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                return this.View(this.context.GetNewUserOrders(this.User));
            }

            return new ChallengeResult();
        }
    }
}
