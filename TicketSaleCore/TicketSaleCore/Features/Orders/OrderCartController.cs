namespace TicketSaleCore.Features.Orders
{
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using AutoMapper;

    using Home;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Models.BLL.Interfaces;
    using Models.Entities;

    using OrderCart.ViewModels;

    /// <summary>
    /// The order cart controller.
    /// </summary>
    public class OrderCartController : Controller
    {
        private IOrderCartService OrderCartService;
        private IAuthorizationService authorizationService;
        public OrderCartController(IOrderCartService OrderCartService,
                                   IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            this.OrderCartService = OrderCartService;
        }

        public async Task<IActionResult> GetCurrentCart(string userId)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.OrderCartService,
                        Operations.Read))
            {
                // var buyer = this.User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier).Value;
                Mapper.Initialize(cfg => cfg.CreateMap<OrderingCart, OrderCartViewModel>());
                var wwww = this.OrderCartService.GetUserCart(this.User);
                var qery = Mapper.Map<OrderingCart, OrderCartViewModel>(wwww);

                return this.PartialView(qery);
            }

            return new ChallengeResult();
        }

        public async Task<IActionResult> MakeOrder(string userId)
        {
            if (await this.authorizationService
                .AuthorizeAsync(
                        this.User,
                        this.OrderCartService,
                Operations.Create))
            {
                var newOrders = this.OrderCartService.MakeOrders(this.User);

                return this.RedirectToAction(nameof(OrdersController.NewOrers), "Orders");

                // return this.View();
            }

            return new ChallengeResult();
        }


        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            else
            {
                return this.RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}