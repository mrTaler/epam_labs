﻿namespace TicketSaleCore.Features.Accounts
{
    using System.Linq;
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    using Models.Entities;

    using Users.ViewModels;

    [Authorize]
    public class UsersController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly IAuthorizationService authorizationService;
        public UsersController(
            UserManager<AppUser> userManager,
            IAuthorizationService authorizationService)
        {
            this.userManager = userManager;
            this.authorizationService = authorizationService;
        }

        [Authorize(Roles = "admin")]
        public IActionResult Index() => this.View(this.userManager.Users.ToList());

        public async Task<IActionResult> Details(string id)
        {
           
                AppUser user = await this.userManager.FindByIdAsync(id);
                if (user == null)
                {
                    return this.NotFound();
                }

                return this.View(user);
           }

        [Authorize(Roles = "admin")]
        public IActionResult Create() => this.View();

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                AppUser user = new AppUser { Email = model.Email, UserName = model.Email, Year = model.Year };
                var result = await this.userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return this.RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        this.ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return this.View(model);
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(string id)
        {
            AppUser user = await this.userManager.FindByIdAsync(id);
            if (user == null)
            {
                return this.NotFound();
            }

            EditUserViewModel model = new EditUserViewModel { Id = user.Id, Email = user.Email, Year = user.Year };
            return this.View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                AppUser user = await this.userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    user.Year = model.Year;

                    var result = await this.userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return this.RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            this.ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }

            return this.View(model);
        }


        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            AppUser user = await this.userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await this.userManager.DeleteAsync(user);
            }

            return this.RedirectToAction("Index");
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> ChangePassword(string id)
        {
            AppUser user = await this.userManager.FindByIdAsync(id);
            if (user == null)
            {
                return this.NotFound();
            }

            ChangePasswordViewModel model = new ChangePasswordViewModel { Id = user.Id, Email = user.Email };
            return this.View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                AppUser user = await this.userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    IdentityResult result =
                        await this.userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return this.RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            this.ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "PLACE HOLDER");
                }
            }

            return this.View(model);
        }
    }
}