﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TicketSaleCore.Features.Accounts.Manage;
using TicketSaleCore.Features.Accounts.Manage.ViewModels;
using TicketSaleCore.Models.Entities;

namespace TicketSaleCore.Features.Accounts
{
    using Users.ViewModels;

    [Authorize]
    public class ManageController : Controller
    {
        private readonly UserManager<AppUser> userManager;

        private readonly SignInManager<AppUser> signInManager;

        private readonly string externalCookieScheme;

        public ManageController(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IOptions<IdentityCookieOptions> identityCookieOptions,
            ILoggerFactory loggerFactory)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.externalCookieScheme = identityCookieOptions.Value.ExternalCookieAuthenticationScheme;
        }

        // GET: /Manage/Index
        [HttpGet]
        public async Task<IActionResult> Index(ManageMessageId? message = null)
        {
            this.ViewData["StatusMessage"] = message == ManageMessageId.ChangePasswordSuccess
                                                 ? "Your password has been changed."
                                                 : message == ManageMessageId.SetPasswordSuccess
                                                     ? "Your password has been set."
                                                     : message == ManageMessageId.SetTwoFactorSuccess
                                                         ? "Your two-factor authentication provider has been set."
                                                         : message == ManageMessageId.Error
                                                             ? "An error has occurred."
                                                             : message == ManageMessageId.AddPhoneSuccess
                                                                 ? "Your phone number was added."
                                                                 : message == ManageMessageId
                                                                       .RemovePhoneSuccess
                                                                     ? "Your phone number was removed."
                                                                     : string.Empty;

            var user = await this.GetCurrentUserAsync();
            if (user == null)
            {
                return this.View("Error");
            }

            var model = new IndexViewModel
            {
                HasPassword = await this.userManager.HasPasswordAsync(user),
                PhoneNumber = await this.userManager.GetPhoneNumberAsync(user),
                TwoFactor = await this.userManager.GetTwoFactorEnabledAsync(user),
                Logins = await this.userManager.GetLoginsAsync(user),
                BrowserRemembered =
                                    await this.signInManager.IsTwoFactorClientRememberedAsync(user)
            };
            return this.View(model);
        }

        // GET: /Manage/SetPassword
        [HttpGet]
        public IActionResult SetPassword()
        {
            return this.View();
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var user = await this.GetCurrentUserAsync();
            if (user != null)
            {
                var result = await this.userManager.AddPasswordAsync(user, model.NewPassword);
                if (result.Succeeded)
                {
                    await this.signInManager.SignInAsync(user, isPersistent: false);
                    return this.RedirectToAction(
                        nameof(this.Index),
                        new { Message = ManageMessageId.SetPasswordSuccess });
                }

                this.AddErrors(result);
                return this.View(model);
            }

            return this.RedirectToAction(nameof(this.Index), new { Message = ManageMessageId.Error });
        }

        public async Task<IActionResult> ChangePassword(string id)
        {
            AppUser user = await this.userManager.FindByIdAsync(id);
            if (user == null)
            {
                return this.NotFound();
            }

            ChangePasswordViewModel model = new ChangePasswordViewModel { Id = user.Id, Email = user.Email };
            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                AppUser user = await this.userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    IdentityResult result =
                        await this.userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return this.RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            this.ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "PLACE HOLDER");
                }
            }

            return this.View(model);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<AppUser> GetCurrentUserAsync()
        {
            return this.userManager.GetUserAsync(this.HttpContext.User);
        }

        #endregion
    }
}
