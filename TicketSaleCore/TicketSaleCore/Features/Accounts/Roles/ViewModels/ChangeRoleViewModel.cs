﻿using System.Collections.Generic;

namespace TicketSaleCore.Features.Accounts.Roles.ViewModels
{
    using Models.Entities;

    public class ChangeRoleViewModel
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        public List<ApplicationRole> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public ChangeRoleViewModel()
        {
            this.AllRoles = new List<ApplicationRole>();
            this.UserRoles = new List<string>();
        }
    }
}
