namespace TicketSaleCore.Features.Accounts
{
    using Microsoft.AspNetCore.Mvc;

    public class AdminAreaController : Controller
    {
        public IActionResult Index()
        {
            return this.View();
        }
    }
}