namespace TicketSaleCore.Features.Accounts
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    using Models.Entities;
    using Models.IdentityEF;

    using Roles.ViewModels;

    [Authorize(Roles = "admin")]
    public class RolesController : Controller
    {
        private readonly ApplicationRoleManager roleManager;

        private readonly UserManager<AppUser> userManager;

        public RolesController(ApplicationRoleManager roleManager, UserManager<AppUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        public IActionResult Index() => this.View(this.roleManager.Roles.ToList());
        public IActionResult Create() => this.View();
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if(!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await this.roleManager.CreateAsync(new ApplicationRole(name));
                if(result.Succeeded)
                {
                    return this.RedirectToAction("Index");
                }
                else
                {
                    foreach(var error in result.Errors)
                    {
                        this.ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return this.View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
           ApplicationRole role = await this.roleManager.FindByIdAsync(id);
            if(role != null)
            {
                IdentityResult result = await this.roleManager.DeleteAsync(role);
            }

            return this.RedirectToAction("Index");
        }

        public IActionResult UserList() => this.View(this.userManager.Users.ToList());
        public async Task<IActionResult> Edit(string id)
        {
            // получаем пользователя
            AppUser user = await this.userManager.FindByIdAsync(id);
            if(user != null)
            {
                // получем список ролей пользователя
                var userRoles = await this.userManager.GetRolesAsync(user);
                var allRoles = this.roleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return this.View(model);
            }

            return this.NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            // get user
            AppUser user = await this.userManager.FindByIdAsync(userId);
            if(user != null)
            {
                // get usrer roles
                var userRoles = await this.userManager.GetRolesAsync(user);

                // get all role from store
                var allRoles = this.roleManager.Roles.ToList();

                // get added roles 
                var addedRoles = roles.Except(userRoles);

                // get remooved rolse
                var removedRoles = userRoles.Except(roles);

                await this.userManager.AddToRolesAsync(user, addedRoles);

                await this.userManager.RemoveFromRolesAsync(user, removedRoles);

                return this.RedirectToAction("UserList");
            }

            return this.NotFound();
        }
    }
}