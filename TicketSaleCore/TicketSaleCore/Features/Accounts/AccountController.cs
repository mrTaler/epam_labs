namespace TicketSaleCore.Features.Accounts
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Account.ViewModels;

    using Home;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    using Models.Entities;

    using Roles.ViewModels;

    using Services;

    /// <summary>
    /// The account controller.
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly ILogger logger;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;

        public AccountController(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            ILoggerFactory loggerFactory,
            IEmailSender emailSender,
            ISmsSender smsSender)
        {
            this._emailSender = emailSender;
            this._smsSender = smsSender;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = loggerFactory.CreateLogger<AccountController>();
        }

        #region Register HttpGet
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return this.View();
        }

        #endregion
        #region Register HttpPost
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;
            if (this.ModelState.IsValid)
            {

                var user = new AppUser { UserName = model.Email, Email = model.Email };
                var userinDb = await this.userManager.FindByNameAsync(model.Email);
                if (userinDb == null)
                {
                    var result = await this.userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var code = await this.userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = this.Url.Action(nameof(this.ConfirmEmail), "Account",
                            new { userId = user.Id, code = code }, protocol: this.HttpContext.Request.Scheme);


                        await this._emailSender.SendEmailAsync(model.Email, "Confirm your account",
                            $"Please confirm your account by clicking this link: <a href='{callbackUrl}'>link</a>");

                        this.logger.LogError(3, $"User {user.Email} created a new account with password.");


                        // await this.signInManager.SignInAsync(user, false);
                        return this.RedirectToLocal(returnUrl);
                    }

                    this.AddErrors(result);
                }
                else
                {
                    this.ModelState.AddModelError("userInDb", "User is registed");
                    return this.View(model);
                }
            }


            return this.View(model);
        }

        #endregion



        #region ConfirmEmail HttpGet
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return this.View("Error");
            }

            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return this.View("Error");
            }

            var result = await this.userManager.ConfirmEmailAsync(user, code);
            await this.userManager.AddClaimAsync(user, new Claim("CityService", "R", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("EventService", "R", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("EventTypeService", "R", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("OrderCartService", "CRU", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("OrderingTicketService", "CRU", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("OrdersService", "CRU", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("OrderStatusService", "R", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("TicketsService", "CRUD", ClaimValueTypes.String, "admin"));
            await this.userManager.AddClaimAsync(user, new Claim("VenuesService", "R", ClaimValueTypes.String, "admin"));
            await this.userManager.AddToRoleAsync(user, "user");
            return this.View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        #endregion


        #region Login HttpGet
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            await this.signInManager.SignOutAsync(); // LogOut
            return this.View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        #endregion

        #region Login HttpPost
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;
            if (this.ModelState.IsValid)
            {
                var user = await this.userManager.FindByNameAsync(model.Email);
                if (user != null)
                {
                    // ���������, ����������� �� email
                    if (!await this.userManager.IsEmailConfirmedAsync(user))
                    {
                        this.ModelState.AddModelError("emailConfirm", "email not confirm");
                        return this.View(model);
                    }


                    var result = await this.signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);

                    if (result.Succeeded)
                    {
                        this.logger.LogInformation(1, $"User {user?.Email} logged");
                        return this.RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        this.ModelState.AddModelError("PasswordError", "Invalid login attempt.");
                        this.logger.LogInformation(2, $"User {user?.Email}  account error.");
                        return this.View(model);
                    }
                }
                else
                {
                    this.ModelState.AddModelError("UserNotFound", "UserNotFound");
                    this.logger.LogInformation(2, $"User {user?.Email}  account error.");
                    return this.View(model);
                }
            }

            return this.View(model);
        }

        #endregion

        #region LogOff HttpPost
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff(string returnUrl = null)
        {
            // remove coocies
            await this.signInManager.SignOutAsync();
            this.logger.LogError(4, "User logged out.");
            return this.RedirectToLocal(returnUrl);

            // return RedirectToAction("Index", "Home");
        }

        #endregion

        #region AccessDenied HttpGet
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return this.View();
        }

        #endregion

        #region ForgotPassword HttpGet

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return this.View();
        }

        #endregion

        #region ForgotPassword HttpPost

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await this.userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    var codeEmail = await this.userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrlEmail = this.Url.Action(nameof(this.ConfirmEmail), "Account",
                        new { userId = user.Id, code = codeEmail }, protocol: this.HttpContext.Request.Scheme);


                    await this._emailSender.SendEmailAsync(model.Email, "Confirm your account",
                        $"Please confirm your account by clicking this link: <a href='{callbackUrlEmail}'>link</a>");

                    return this.View("ConfirmEmail");


                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await this.userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = this.Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: this.HttpContext.Request.Scheme);
                await this._emailSender.SendEmailAsync(model.Email, "Reset Password",
                    $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
                return this.View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return this.View(model);
        }

        #endregion

        #region ForgotPasswordConfirmation HttpGet

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return this.View();
        }

        #endregion

        #region ResetPassword HttpGet

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? this.View("Error") : this.View();
        }

        #endregion

        #region ResetPassword HttpPost

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var user = await this.userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return this.RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            var result = await this.userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return this.RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            this.AddErrors(result);
            return this.View();
        }

        #endregion

        #region ResetPasswordConfirmation HttpGet

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return this.View();
        }

        #endregion

        #region Magic
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            else
            {
                return this.RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}