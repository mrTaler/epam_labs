﻿namespace TicketSaleCore.Features.Accounts.Manage.ViewModels
{
    public class RemoveLoginViewModel
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
    }
}
