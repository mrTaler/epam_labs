namespace TicketSaleCore.Features.Events
{
    using System.IO;
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;

    using Models.BLL.Interfaces;
    using Models.Entities;

    using TicketSaleCore.Models.BLL.DTO;

    [Authorize]
    public class EventsController : Controller
    {
        private readonly IEventService context;

        private readonly IHostingEnvironment appEnvironment;

        private IAuthorizationService authorizationService;

        public EventsController(
            IEventService context,
            IAuthorizationService authorizationService,
            IHostingEnvironment appEnvironment)
        {
            this.context = context;
            this.appEnvironment = appEnvironment;
            this.authorizationService = authorizationService;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            return this.View(this.context.GetAll());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (await this.authorizationService.AuthorizeAsync(this.User, this.context, Operations.Read))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var @event = this.context.GetById(id);
                if (@event == null)
                {
                    return this.NotFound();
                }

                return this.View(@event);
            }

            return new ChallengeResult();
        }

        #region Not For Task01

        // GET: Events/Create
        [Authorize(Roles = "admin")]
        public IActionResult Create([FromServices] IVenuesService venuesService, [FromServices] IEventTypeService eventTypeService)
        {
            this.ViewData["VenueFk"] = new SelectList(venuesService.GetAll(), "Id", "Address");
            this.ViewData["VenueTypeFk"] = new SelectList(eventTypeService.GetAll(), "Id", "NameEventsType");
            return this.View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [FromServices] IVenuesService venuesService,
            [FromServices] IEventTypeService eventTypeService,
            Event @event,
            IFormFile uploadedFile)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    if (uploadedFile != null)
                    {
                        string path = "/images/EventImg/" + uploadedFile.FileName;
                        using (var fileStream = new FileStream(this.appEnvironment.WebRootPath + path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }

                        @event.Banner = path;
                    }

                    this.context.Create(@event);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!this.EventExists(@event.Id))
                    {
                        return this.NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return this.RedirectToAction("Index");
            }

            this.ViewData["VenueFk"] = new SelectList(venuesService.GetAll(), "Id", "Address", @event.VenueId);
            this.ViewData["VenueTypeFk"] = new SelectList(eventTypeService.GetAll(), "Id", "NameEventsType", @event.EventsTypeId);
            return this.View(@event);
        }

        // GET: Events/Edit/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(
            [FromServices] IVenuesService venuesService,
            [FromServices] IEventTypeService eventTypeService,
            int? id)
        {
            var @event = this.context.GetForEdit(id);
            if (@event == null)
            {
                return this.NotFound();
            }

            this.ViewData["VenueFk"] = new SelectList(venuesService.GetAll(), "Id", "Address", @event.VenueId);
            this.ViewData["VenueTypeFk"] = new SelectList(eventTypeService.GetAll(), "Id", "NameEventsType", @event.EventsTypeId);
            return this.View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(
            [FromServices] IVenuesService venuesService,
            [FromServices] IEventTypeService eventTypeService,
            int id,
            EventsDTO @event,
            IFormFile uploadedFile)
        {
            this.ViewData["VenueFk"] = new SelectList(venuesService.GetAll(), "Id", "Address", @event.VenueId);
            this.ViewData["VenueTypeFk"] = new SelectList(eventTypeService.GetAll(), "Id", "NameEventsType", @event.EventsTypeId);
            if (id != @event.Id)
            {
                return this.NotFound();
            }

            if (this.ModelState.IsValid)
            {
                try
                {
                    if (uploadedFile != null)
                    {
                        string path = "/images/EventImg/" + uploadedFile.FileName;
                        using (var fileStream = new FileStream(this.appEnvironment.WebRootPath + path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }

                        @event.Banner = path;
                    }

                    this.context.Update(@event);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!this.EventExists(@event.Id))
                    {
                        return this.NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return this.View(@event); // RedirectToAction("Index");
            }

            return this.View(@event);
        }

        [Authorize(Roles = "admin")]

        // GET: Events/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var eventForDelete = this.context.GetById(id);

            if (eventForDelete == null)
            {
                return this.NotFound();
            }

            return this.View(eventForDelete);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @event = this.context.Delete(this.context.GetById(id));
            return this.RedirectToAction("Index");
        }

        #endregion

        private bool EventExists(int id)
        {
            return this.context.IsExists(id);
        }
    }
}
