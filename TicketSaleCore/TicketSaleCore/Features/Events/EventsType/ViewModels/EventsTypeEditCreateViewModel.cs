﻿namespace TicketSaleCore.Features.Events.EventsType.ViewModels
{
    public class EventsTypeEditCreateViewModel
    {
        public int Id { get; set; }
        public string NameEventsType { get; set; }
    }
}