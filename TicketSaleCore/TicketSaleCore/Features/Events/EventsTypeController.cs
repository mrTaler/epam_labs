using System.Threading.Tasks;

using AutoMapper;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using TicketSaleCore.Features.Events.EventsType.ViewModels;
using TicketSaleCore.Models.BLL.Infrastructure;
using TicketSaleCore.Models.BLL.Interfaces;

namespace TicketSaleCore.Features.Events
{
    using AuthorizationPolit.ResourceBased;

    [Authorize]
    public class EventsTypeController : Controller
    {
        private readonly IEventTypeService context;

        private IAuthorizationService authorizationService;

        public EventsTypeController(IEventTypeService context,
                                    IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            this.context = context;
        }

        public async Task<IActionResult> Index()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                return this.View(this.context.GetAll());
            }

            return new ChallengeResult();
        }


        public async Task<IActionResult> Details(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var EventType = this.context.GetById(id);
                if (EventType == null)
                {
                    return this.NotFound();
                }

                return this.View(EventType);
            }

            return new ChallengeResult();
        }

        #region Edit [Authorize(Roles = "admin")]

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (await this.authorizationService.AuthorizeAsync(
                    this.User,
                    this.context,
                    Operations.Update))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                Mapper.Initialize(cfg => cfg.CreateMap<Models.Entities.EventsType, EventsTypeEditCreateViewModel>());
                var qery = Mapper.Map<Models.Entities.EventsType, EventsTypeEditCreateViewModel>(this.context.GetById(id));

                if (qery == null)
                {
                    return this.NotFound();
                }

                return this.View(qery);
            }

            return new ChallengeResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EventsTypeEditCreateViewModel model)
        {
            if (await this.authorizationService.AuthorizeAsync(
                    this.User,
                    this.context,
                    Operations.Update))
            {
                if (this.ModelState.IsValid)
                {
                    try
                    {
                        Mapper.Initialize(cfg => cfg.CreateMap<EventsTypeEditCreateViewModel, Models.Entities.EventsType>());
                        this.context.Update(Mapper.Map<EventsTypeEditCreateViewModel, Models.Entities.EventsType>(model));
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!this.context.IsExists(model.Id))
                        {
                            return this.NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return this.RedirectToAction("Index");
                }

                return this.View(model);
            }

            return new ChallengeResult();
        }

        #endregion

        #region Create  [Authorize(Roles = "admin")]

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                return this.View();
            }

            return new ChallengeResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EventsTypeEditCreateViewModel model)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Create))
            {
                if (this.ModelState.IsValid)
                {
                    try
                    {
                        Mapper.Initialize(cfg => cfg.CreateMap<EventsTypeEditCreateViewModel, Models.Entities.EventsType>());
                        this.context.Create(Mapper.Map<EventsTypeEditCreateViewModel, Models.Entities.EventsType>(model));

                        return this.RedirectToAction("Index");
                    }
                    catch (BllValidationException er)
                    {
                        this.ModelState.AddModelError("Data exist", er.Message);
                        return this.View(model);
                    }
                }

                return this.View(model);
            }

            return new ChallengeResult();
        }

        #endregion

        #region delete  [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Delete))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var cityDelete = this.context.GetById(id);

                if (cityDelete == null)
                {
                    return this.NotFound();
                }

                return this.View(cityDelete);
            }

            return new ChallengeResult();
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Delete))
            {
                try
                {
                    this.context.Delete(this.context.GetById(id));

                    return this.RedirectToAction("Index");
                }
                catch (BllValidationException er)
                {
                    this.ModelState.AddModelError(er.Property, er.Message);
                    return this.View(this.context.GetById(id));
                }
            }

            return new ChallengeResult();

        }

        #endregion
    }
}