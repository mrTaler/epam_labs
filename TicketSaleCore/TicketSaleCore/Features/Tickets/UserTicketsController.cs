namespace TicketSaleCore.Features.Tickets
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    using Models.BLL.Infrastructure;
    using Models.BLL.Interfaces;
    using Models.Entities;

    using UserTickets.ViewModels;

    /// <summary>
    /// The user tickets controller.
    /// </summary>
    [Authorize]
    public class UserTicketsController : Controller
    {
        private readonly ITicketsService context;
        private readonly UserManager<AppUser> userManager;
        private IAuthorizationService authorizationService;

        private IOrderingTicketService orderingTicketService;
        public UserTicketsController(
            ITicketsService context,
            IOrderingTicketService orderingTicketService,
            UserManager<AppUser> userManager,
            IAuthorizationService authorizationService)
        {
            this.orderingTicketService = orderingTicketService;
            this.userManager = userManager;
            this.context = context;
            this.authorizationService = authorizationService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                return this.View();
            }

            return new ChallengeResult();
        }

        [HttpGet]
        public async Task<IActionResult> IndexAnotherUser(string userId)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                if (userId != null)
                {
                    if (!userId.Equals(this.User.FindFirst(p => p.Type == ClaimTypes.NameIdentifier).Value))
                    {
                        // if id not equal Authorized User
                        var qq = await this.userManager.Users.FirstOrDefaultAsync(p => p.Id == userId);// find User id repository
                        return this.View(qq);
                    }
                    else
                    {
                        return this.RedirectToAction("Index", "UserTickets");
                    }
                }

                return this.View("Error");
            }

            return new ChallengeResult();
        }

        [HttpGet]
        public async Task<IActionResult> SellingTickets(string userId = null)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                var ret = this.context.GetAllUserTickets(userId, TicketStatus.SellingTickets);
                return this.PartialView(ret);
            }

            return new ChallengeResult();
        }

        [HttpGet]
        public async Task<IActionResult> WaitingConfomition()
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                var userId = this.User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;

                var dto = this.context.GetWaitingConfomitionTickets(userId);

                ICollection<ConfWaitTicketsViewModel> vm = new List<ConfWaitTicketsViewModel>();

                foreach (var ticket in dto)
                {
                    vm.Add(new ConfWaitTicketsViewModel
                    {
                        WaitingTicket = ticket.Tiket,
                        BuyerUser = ticket.Order.Buyer,
                        OrderingId = ticket.Id.ToString()
                    });
                }

                return this.PartialView("ConfWaitTickets", vm);
            }

            return new ChallengeResult();
        }

        [HttpGet]
        public async Task<IActionResult> Sold(string userId = null)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                return this.PartialView("SellingTickets",
                    this.context.GetAllUserTickets(userId,
                    TicketStatus.Sold));
            }

            return new ChallengeResult();
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmTicket(string ticketOrderingId)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.context,
                        Operations.Read))
            {
                var rew = this.orderingTicketService.ConfirmToSaleTicket(ticketOrderingId);
                var buyer = this.User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value;

                return this.RedirectToAction("WaitingConfomition");
            }

            return new ChallengeResult();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return this.RedirectToAction("Create", "Tickets");
        }
    }
}