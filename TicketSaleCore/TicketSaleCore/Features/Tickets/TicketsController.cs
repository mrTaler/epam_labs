namespace TicketSaleCore.Features.Tickets
{
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using AutoMapper;

    using Home;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;

    using Models.BLL.Infrastructure;
    using Models.BLL.Interfaces;
    using Models.Entities;

    using Tickets.ViewModels;

    /// <summary>
    /// The tickets controller.
    /// </summary>
    [Authorize]
    public class TicketsController : Controller
    {
        private ITicketsService ticketsService;

        private IAuthorizationService authorizationService;

        private IOrderCartService orderCartService;

        private IEventService eventService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TicketsController"/> class.
        /// </summary>
        /// <param name="ticketsService">
        /// The tickets service.
        /// </param>
        /// <param name="orderCartService">
        /// The order cart service.
        /// </param>
        /// <param name="authorizationService">
        /// The authorization service.
        /// </param>
        /// <param name="eventService">
        /// The event service.
        /// </param>
        public TicketsController(
           ITicketsService ticketsService,
           IOrderCartService orderCartService,
            IAuthorizationService authorizationService,
            IEventService eventService)
        {
            this.authorizationService = authorizationService;
            this.orderCartService = orderCartService;
            this.ticketsService = ticketsService;
            this.eventService = eventService;
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Index(int? id)
        {
            var ret = this.ticketsService.GetTicketByEventWBuyer(
                id,
                this.User);
            return this.View(ret);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var newTicket = new CreateTicketViewModel();

            this.ViewBag.events = new SelectList(
                this.eventService.GetAll(),
                "Id",
                "Name");

            return this.View(newTicket);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateTicketViewModel model)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<CreateTicketViewModel, Ticket>());

            var q = this.ticketsService.CreateNewTicket(Mapper.Map<CreateTicketViewModel, Ticket>(model), this.User);

            return this.RedirectToAction("Index", "UserTickets");
        }

        #region Edit
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            /*  
             *  Mapper.Initialize(cfg => cfg.CreateMap<City, CityEditCreateViewModel>());
             *  var qery = Mapper.Map<City, CityEditCreateViewModel>(this.context.Get(id));
             */
            var qery = this.ticketsService.GetById(id);
            if (qery == null)
            {
                return this.NotFound();
            }
            
            return this.View(qery);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Ticket model)
        {
            if (this.ModelState.IsValid)
            {
                this.ticketsService.Update(model);
                return this.RedirectToAction("Index");
            }

            return this.View(model);
        }

        #endregion

        #region delete 

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.ticketsService,
                        Operations.Delete))
            {
                if (id == null)
                {
                    return this.NotFound();
                }

                var ticketDelete = this.ticketsService.GetById(id);

                if (ticketDelete == null)
                {
                    return this.NotFound();
                }

                return this.View(ticketDelete);
            }

            return new ChallengeResult();
        }

        /// <summary>
        /// The delete confirm.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.ticketsService,
                        Operations.Delete))
            {
                try
                {
                    this.ticketsService.Delete(this.ticketsService.GetById(id));

                    return this.RedirectToAction("Index");
                }
                catch (BllValidationException er)
                {
                    this.ModelState.AddModelError(er.Property, er.Message);
                    return this.View(this.ticketsService.GetById(id));
                }
            }

            return new ChallengeResult();
        }

        #endregion

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var ticket = this.ticketsService.GetById(id);
            if (ticket == null)
            {
                return this.NotFound();
            }

            return this.View(ticket);
        }

        /// <summary>
        /// The redirect to local.
        /// </summary>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            else
            {
                return this.RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        /// <summary>
        /// The add to cart.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> AddToCart(int id)
        {
            if (await this.authorizationService
                    .AuthorizeAsync(
                        this.User,
                        this.orderCartService,
                        Operations.Update))
            {
                this.orderCartService.AddToCart(
                    id,
                    this.User);
                return this.RedirectToLocal("/");
            }

            return new ChallengeResult();
        }
    }
}
