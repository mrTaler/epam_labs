﻿namespace TicketSaleCore.Features.Tickets.Tickets.ViewModels
{
    public class CreateTicketViewModel
    {

        public decimal Price { get; set; }

        public string SellerNotes { get; set; }

       public int EventId { get; set; }

    }
}
