﻿

namespace TicketSaleCore.Features.Tickets.UserTickets.ViewModels
{
    using Models.Entities;

    public class ConfWaitTicketsViewModel
    {
       public Ticket WaitingTicket { get; set; }
       public AppUser BuyerUser { get; set; }

        public string OrderingId { get; set; }
    }
}
