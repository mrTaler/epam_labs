﻿namespace TicketSaleCore.Features.Home
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;

    using AuthorizationPolit.ResourceBased;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    using Models.BLL.Interfaces;

    // [ServiceFilter(typeof(LanguageActionFilter))]

    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// The authorization service.
        /// </summary>
        private readonly IAuthorizationService authorizationService;

        /// <summary>
        /// The event service new.
        /// </summary>
        private readonly IEventServiceNew eventServiceNew;

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="eventService">
        /// The event service.
        /// </param>
        /// <param name="authorizationService">
        /// The authorization Service.
        /// </param>
        /// <param name="eventServiceNew">
        /// The event Service New.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger Factory.
        /// </param>
        public HomeController(
            IAuthorizationService authorizationService,
            ILoggerFactory loggerFactory,
            IEventServiceNew eventServiceNew)
        {
            this.authorizationService = authorizationService;
            this.eventServiceNew = eventServiceNew;

            this.logger = loggerFactory.CreateLogger<HomeController>();
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IActionResult> Index(int? taskNo=null)
        {
            this.logger.LogInformation($"Task: {taskNo}");


              Stopwatch sw = new Stopwatch();
            sw.Start();
            var eve = await this.eventServiceNew.GetAllEventWithTicketsAsync(this.User);
            sw.Stop();
            this.logger.LogInformation($"await eventServiceNew.GetAllEventWithTicketsAsync:" +
                             $" {sw.Elapsed}");

            return  await Task.FromResult(this.View(eve));
        }

        /// <summary>
        /// The about.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        public async Task<IActionResult> About()
        {
            return this.View();
        }

        /// <summary>
        /// The contact.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        public async Task<IActionResult> Contact()
        {
            return await Task.FromResult( this.View());
        }

        /// <summary>
        /// The set language.
        /// </summary>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            this.Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) });
            return this.LocalRedirect(returnUrl);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        public IActionResult Error()
        {
            return this.View();
        }
    }
}
