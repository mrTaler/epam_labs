﻿
using TicketSaleCore.Models.DAL.Interfaces;


namespace TicketSaleCore
{
    using System.Globalization;
    using System.IO;

    using AuthorizationPolit.Password;
    using AuthorizationPolit.ResourceBased;
    using AuthorizationPolit.UserAndPassword;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.AspNetCore.Mvc.Razor;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.PlatformAbstractions;
    using Microsoft.Net.Http.Headers;

    using Models.BLL.Interfaces;
    using Models.BLL.Services;
    using Models.DAL;
    using Models.DAL.Implementation;
    using Models.DAL._Ef;
    using Models.Entities;
    using Models.IdentityEF;

    using Services;

    using Swashbuckle.AspNetCore.Swagger;

    public class Startup
    {
        string testSecret = null;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)

                // .AddUserSecrets<Startup>()
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();

        }

        public IConfigurationRoot Configuration
        {
            get;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // use localization
            services.Configure<RequestLocalizationOptions>(
                options =>
                    {
                        var supportedCultures = new[]
                                                    {
                                                        new CultureInfo("en"), new CultureInfo("ru"),
                                                        new CultureInfo("be")
                                                    };

                        options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                        options.SupportedCultures = supportedCultures;
                        options.SupportedUICultures = supportedCultures;

                        options.RequestCultureProviders.Insert(
                            0,
                            new CustomRequestCultureProvider(async context => new ProviderCultureResult("en")));
                    });

            // Add Pasword validator
            services.AddTransient<IPasswordValidator<AppUser>, CustomPasswordValidator>(
                serv => new CustomPasswordValidator(5));
            services.AddTransient<IUserValidator<AppUser>, MyUserValidator>();

            // Add localizaion based on Resx files
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            // services.AddDbContext<ApplicationContext>(opt => opt.UseInMemoryDatabase());
            services.AddDbContext<ApplicationContext>(
                options => options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Scoped);

            

            services
                .AddScoped<UserStore<AppUser, ApplicationRole, ApplicationContext, string, AppUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationUserToken, ApplicationRoleClaim>,
                    ApplicationUserStore>();
            services.AddScoped<UserManager<AppUser>, ApplicationUserManager>();
            services.AddScoped<RoleManager<ApplicationRole>, ApplicationRoleManager>();
            services.AddScoped<SignInManager<AppUser>, ApplicationSignInManager>();
            services
                .AddScoped<RoleStore<ApplicationRole, ApplicationContext, string, ApplicationUserRole, ApplicationRoleClaim>, ApplicationRoleStore>();

            // Add Identity to services
            services.AddIdentity<AppUser, ApplicationRole>()
                .AddUserStore<ApplicationUserStore>()
                .AddUserManager<ApplicationUserManager>()
                .AddRoleStore<ApplicationRoleStore>()
                .AddRoleManager<ApplicationRoleManager>()
                .AddSignInManager<ApplicationSignInManager>()
                .AddDefaultTokenProviders();

            services.AddAuthorization();

            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<CityService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<EventService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<EventTypeService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<OrderCartService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<OrderingTicketService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<OrdersService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<OrderStatusService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<TicketsService>>();
            services.AddScoped<IAuthorizationHandler, GenerikServicerAccesHander<VenuesService>>();

            

            var csvFormatterOptions = new CsvFormatterOptions();
            services.AddMvc(
                    o =>
                        {
                            o.RespectBrowserAcceptHeader = true;
                            o.FormatterMappings.SetMediaTypeMappingForFormat(
                                "xml",
                                new MediaTypeHeaderValue("application/xml"));
                            o.FormatterMappings.SetMediaTypeMappingForFormat(
                                "json",
                                new MediaTypeHeaderValue("application/json"));
                            o.FormatterMappings.SetMediaTypeMappingForFormat(
                                "csv",
                                new MediaTypeHeaderValue("text/csv"));

                            o.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                            o.OutputFormatters.Add(new CsvOutputFormatter(csvFormatterOptions));

                            o.Conventions.Add(new FeatureConvention());
                            o.CacheProfiles.Add(
                                "Caching",
                                new CacheProfile() { Duration = 500, Location = ResponseCacheLocation.Any });

                            o.CacheProfiles.Add(
                                "NoCaching",
                                new CacheProfile() { Location = ResponseCacheLocation.None, NoStore = true });
                        })
                .AddRazorOptions(options => { options.ConfigureFeatureFolders(); })
                .AddViewLocalization(
                    LanguageViewLocationExpanderFormat.Suffix,
                    opts => { opts.ResourcesPath = "Resources"; })
                .AddDataAnnotationsLocalization()
                .AddXmlSerializerFormatters()
                .AddXmlDataContractSerializerFormatters();

            services.AddMvcCore().AddApiExplorer();
            services.AddSwaggerGen(
                c =>
                    {
                        c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                        var filePath = PlatformServices.Default.Application.ApplicationBasePath;
                        var xmlPath = Path.Combine(filePath, "TicketSaleCore.xml");
                        c.IncludeXmlComments(xmlPath);
                    });

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            // services.Configure<AuthMessageSenderOptions>(Configuration);
            #region BLL Services

            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IEventService, EventService>();

            services.AddScoped<IEventServiceNew, EventServiceNew>();

            services.AddScoped<IEventTypeService, EventTypeService>();
            services.AddScoped<IOrderCartService, OrderCartService>();
            services.AddScoped<IOrderingTicketService, OrderingTicketService>();
            services.AddScoped<IOrdersService, OrdersService>();
            services.AddScoped<IOrderStatusService, OrderStatusService>();

            services.AddScoped<ITicketsService, TicketsService>();
            services.AddScoped<IVenuesService, VenuesService>();

            #endregion

            services.AddCors();

            #region IUnitOfWork serv

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // services.AddSingleton<IUnitOfWork, MemoryUnitOfWork>(); 
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IUnitOfWork applicationContext,
            ICityService cityService,
            IEventTypeService eventTypeService,
            IOrderStatusService orderStatusService,
            IEventService eventService,
            IVenuesService venuesService,
            ITicketsService ticketsService,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IOrderCartService orderCartService)
        {
            // Logger settings
            loggerFactory.AddConsole( /*Configuration.GetSection("Logging")*/ (category, level) =>
                {
                    if (category.Contains("TicketSaleCore") && level >= LogLevel.Information) return true;
                    else if (category.Contains("Microsoft") && level >= LogLevel.Error) return true;
                    return false;
                });

            // loggerFactory.AddDebug();

            // Available localization
            var supportedCultures = new[] { new CultureInfo("en"), new CultureInfo("ru"), new CultureInfo("be") };

            // Add Localization (default is en-US)
            app.UseRequestLocalization(
                new RequestLocalizationOptions
                    {
                        DefaultRequestCulture = new RequestCulture("en-US"),

                        // Formatting numbers, dates, etc.
                        SupportedCultures = supportedCultures,

                        // UI strings that we have localized.
                        SupportedUICultures = supportedCultures
                    });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCors(bild => bild.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            // use Identity
            app.UseIdentity();

            app.UseMvc(
                routes => { routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}"); });
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });

            // using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            // {
            // var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationContext>();
            // context.Database.EnsureDeleted();
            // context.Database.EnsureCreated();
            // // //context.Database.Migrate();
            // // // context.Database.ExecuteSqlCommand(//$"DELETE FROM dbo.Cites;" +
            // // //                                 //  $"DELETE FROM dbo.Events;" +
            // // //                                  // $"DELETE FROM dbo.EventsTypes;" +

            // // //                                  // $"DELETE FROM dbo.OrderStatuses;" +
            // // //  $"DELETE FROM dbo.Tickets;" +
            // // // $"DELETE FROM dbo.Orders;" +
            // // //                                // $"DELETE FROM dbo.Venues;"
            // // //                                     $"" 
            // // //                                        );
            // // //    context.Database.EnsureCreated();
            // // //    //    .Migrate();
            // }

            // User&role&Claim Init 
     //       DbInit.UserInit(app.ApplicationServices).Wait();

            // Db content init
   /*// /*        DbInit.AddTestData(
                    applicationContext,
                    cityService,
                    eventTypeService,
                    orderStatusService,
                    eventService,
                    venuesService,
                    ticketsService,
                    userManager,
                    signInManager,
                    orderCartService)
                .Wait();*/
        }
    }
}