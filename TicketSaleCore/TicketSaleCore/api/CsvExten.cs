﻿using System;

namespace TicketSaleCore.api
{
    public interface ICsvExtend
    {
         Tuple<string, string> ConvertToCsv();
    }
}