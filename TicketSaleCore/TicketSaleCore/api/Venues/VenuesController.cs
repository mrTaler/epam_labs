﻿

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketSaleCore.api.Venues
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoQueryable.AspNetCore.Filter.FilterAttributes;

    using Microsoft.AspNetCore.Mvc;

    using Models.BLL.Interfaces;

    /// <summary>
    /// Venues
    /// </summary>
    [FormatFilter]
    [Route("api/[controller]")]
    [ResponseCache(CacheProfileName = "NoCaching")]
    [Produces("application/json", "application/xml", "text/csv")]
   
    public class VenuesController : Controller
    {
        private IVenuesService venuesService;
        public VenuesController(IVenuesService venuesService)
        {
            this.venuesService = venuesService;
        }


        /// <summary>
        /// get Venues with qery
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AutoQueryable]

        public IQueryable<VenuesApiViewModel> Get()
        {
            var allVenue = this.venuesService.GetAll();
            List<VenuesApiViewModel> ret = new List<VenuesApiViewModel>();
            foreach (var venItem in allVenue)
            {
                ret.Add(new VenuesApiViewModel(venItem));
            }

            return ret.AsQueryable();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="venuesService"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}.{format?}")]
        public VenuesApiViewModel Get([FromServices] IVenuesService venuesService, int id)
        {

            var Venue = venuesService.GetById(id);
            var query = new VenuesApiViewModel(Venue);
            return query;
        }
    }
}
