﻿using System;

namespace TicketSaleCore.api.Venues
{
    using Cities;
    using Models.Entities;

    public class VenuesApiViewModel: ICsvExtend
    {
        public VenuesApiViewModel() { }
        public VenuesApiViewModel(Venue venue)
        {
            this.Id = venue.Id;

            this.Name = venue.Name;

            this.Address = venue.Address;

            this.CityFk = venue.CityFk;

            this.City = new CityApiViewModel(venue.City);
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public int CityFk { get; set; }

        public CityApiViewModel City { get; set; }

        public Tuple<string, string> ConvertToCsv()
        {
            var cityCSV = this.City.ConvertToCsv();
            var header = $"Venue{nameof(this.Id)};Venue{nameof(this.Name)};{nameof(this.Address)};{cityCSV.Item1}";

            var value = $"{this.Id};{this.Name};{this.Address};{cityCSV.Item2}";
            
            return new Tuple<string, string>(header, value);
        }

    }
}
