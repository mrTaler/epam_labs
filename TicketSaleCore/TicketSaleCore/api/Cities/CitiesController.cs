using AutoMapper;
using TicketSaleCore.Models.Entities;

namespace TicketSaleCore.api.Cities
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoQueryable.AspNetCore.Filter.FilterAttributes;

    using Microsoft.AspNetCore.Mvc;

    using Models.BLL.Interfaces;

    // [FormatFilter]
    [FormatFilter]
    [ResponseCache(CacheProfileName = "NoCaching")]
    [Route("api/[controller]")]
    [Produces("application/json", "application/xml", "text/csv")]
    public class CitiesController : Controller
    {
        private readonly ICityService cityService;

        public CitiesController(ICityService context)
        {
            this.cityService = context;
        }



        // GET: api/Cities
        [HttpGet]
        [AutoQueryable]
       
        public IQueryable<CityApiViewModel> GetAll()
        {

            var allCity = this.cityService.GetAll()/*.ToList()*/;
            List<CityApiViewModel> ret=new List<CityApiViewModel>();
            foreach (var cityItem in allCity)
            {
                ret.Add(new CityApiViewModel(cityItem));
            }

            return ret.AsQueryable();
        }

        // GET: api/Cities/5
        [HttpGet("{id}.{format?}")]
        public CityApiViewModel GetCity([FromRoute] int id, [FromServices] ICityService context)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<City, CityApiViewModel>());
            var City = context.GetById(id);
            var query = Mapper.Map<City, CityApiViewModel>(City);
            return query;
        }
    }
}