﻿using System;

using TicketSaleCore.Models.Entities;

namespace TicketSaleCore.api.Cities
{
    public class CityApiViewModel: ICsvExtend
    {
        public CityApiViewModel() { }
        public CityApiViewModel(City city)
        {
            if (city != null)
            {
                this.Id = city.Id;
                this.Name = city.Name;
            }
            else
            {
                this.Id = 0;
                this.Name = "Name";
            }
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public Tuple<string, string> ConvertToCsv()
        {
           var header = $"City{nameof(this.Id)};City{nameof(this.Name)}";
            var value = $"{this.Id};{this.Name}";

            return  new Tuple<string, string>(header, value);
        }
    }
}
