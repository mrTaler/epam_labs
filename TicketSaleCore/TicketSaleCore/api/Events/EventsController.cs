﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using TicketSaleCore.Models.DAL.Specifications.Interfaces;
using TicketSaleCore.Models.DAL.Specifications.OrderSpecification;
using TicketSaleCore.Models.Pagin;

namespace TicketSaleCore.Api.Events
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using api.Events;

    using AutoMapper;

    using AutoQueryable.Extensions;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    using Models.BLL.Interfaces;
    using Models.BLL.Specification.EventSpecification;
    using Models.Entities;

    using TicketSaleCore.Models.BLL.Specification.Filter.EventSpecification;

    /// <inheritdoc />
    /// <summary>
    /// The events api controller.
    /// </summary>
    [FormatFilter]
    [Route("api/[controller]")]
    [ResponseCache(CacheProfileName = "NoCaching")]
    [Produces("application/json", "application/xml", "text/csv")]
    public class EventsController : Controller
    {
        private ILogger<EventsController> logger;
        private IEventService context;
        private string idController;

        private int page = 1;
        private int pageSize = 3;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventsController"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public EventsController(ILoggerFactory logger, IEventService context)
        {
            this.logger = logger.CreateLogger<EventsController>();
            this.context = context;
            this.idController = Guid.NewGuid().ToString();
            this.logger.LogCritical($"EventsController create : {DateTime.Now} id is:{this.idController}");
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="EventsController"/> class. 
        /// </summary>
        ~EventsController()
        {
            this.logger.LogCritical($"EventsController destruct: {DateTime.Now} id is:{this.idController}");
        }


        [HttpGet]
        public async Task<IEnumerable<EventApiViewModel>> Get(
            string nameContains = null,
           string orderBy = null)
        {
            var pagination = Request.Headers["Pagination"];
            if (!string.IsNullOrEmpty(pagination))
            {
                string[] vals = pagination.ToString().Split(',');
                int.TryParse(vals[0], out page);
                int.TryParse(vals[1], out pageSize);
            }
            int currentPage = page;
            int currentPageSize = pageSize;
            int totalEvents;
            int totalPages;

            var query = this.Request.QueryString.Value.Replace("&undefined", string.Empty);
            query = query.Replace("?undefined", string.Empty);
           
            ICollection<EventApiViewModel> retColl = new List<EventApiViewModel>();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (!string.IsNullOrEmpty(nameContains))
            {
                var nameSpec = new EventContainsName(nameContains);
                var qq =  this.context.GetAllEventWithTicketsWithSpecAsync(nameSpec);

                foreach (var item in qq.Result)
                {
                    retColl.Add(this.Selector(item));
                }
                totalEvents = retColl.Count;
                totalPages = (int)Math.Ceiling((double)totalEvents / pageSize);
            }

            else
            {
             var   queryResult = this.context.GetAllEventWithTicketsAsync(
                    new FindEventsWithTickets());

                var allEvents = queryResult.Result.AsQueryable().AutoQueryable(query);

                foreach (Event item in allEvents)
                {
                    retColl.Add(new EventApiViewModel(item));
                }

                totalEvents = retColl.Count;
                totalPages = (int)Math.Ceiling((double)totalEvents / pageSize);
                }
            Response.AddPagination(page, pageSize, totalEvents, totalPages);

            sw.Stop();
            IOrderSpecification<EventApiViewModel> sort;
            var second = sw.Elapsed;
            switch (orderBy)
            {
                case "Date":
                    sort=   new SortEventApiViewModelByDataSpecification();
                    break;
                case "DateDesc":
                    sort=  new SortEventApiViewModelByDataSpecification(OrderEn.Descending);
                    break;
                case "Name":
                    sort=  new SortEventApiViewModelByNameSpecification();
                    break;
                case "NameDesc":
                    sort=  new SortEventApiViewModelByNameSpecification(OrderEn.Descending);
                    break;
                default:
                    sort = new SortEventApiViewModelByDataSpecification();
                    break;
            }

            this.logger.LogCritical($"MainCollection query end:{DateTime.Now}, timer {second}");
            return retColl
                .Skip((currentPage - 1) * currentPageSize)
                .Take(currentPageSize).OrderBy(sort); ;
        }



        private EventApiViewModel Selector(Event item)
        {
            return new EventApiViewModel(item);
        }

        [HttpGet("EntityCount")]
        [Produces("application/json")]
        public async Task<IActionResult> EntityCount()
        {
            var query = this.Request.QueryString.Value.Replace("?null", string.Empty);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var queryResult = await this.context.GetAllEventWithTicketsAsync(new FindEventsWithTickets());// .AsQueryable().AutoQueryable(query);

            ICollection<Event> allEvents = queryResult.AsQueryable().AutoQueryable(query);

            //  var ass = await this.context.GetCountAsync();

            sw.Stop();

            var second = sw.ElapsedMilliseconds;
            this.logger.LogCritical($"Count  query end:{DateTime.Now}, timer {second}");
            return new ObjectResult(new { Count = allEvents.Count() });
        }

        [HttpGet("{id}/Tickets")]
        public IActionResult Get(int id)
        {
            var query = this.Request.QueryString.Value;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Ticket, TicketApiViewModel>();
            });
            Mapper.AssertConfigurationIsValid();

            var allEvents = this.context.GetAllEventWithTickets(new ClaimsPrincipal());

            var ticketss = allEvents.First(p => p.Id == id).Tickets.AsQueryable().AutoQueryable(query);

            var retColl = new List<TicketApiViewModel>();
            foreach (Ticket item in ticketss)
            {
                retColl.Add(new TicketApiViewModel(item));
            }

            return new ObjectResult(retColl);
        }

        [HttpGet("{id}.{format?}")]
        public IActionResult GetEvent(int id)
        {
            var Event = this.context.GetById(id);

            var query = new EventApiViewModel(Event);
            return new ObjectResult(query);
        }
    }
}
