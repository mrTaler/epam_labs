﻿
using System;

using TicketSaleCore.Models.Entities;

namespace TicketSaleCore.api.Events
{
    public class TicketApiViewModel: ICsvExtend
    {
        public TicketApiViewModel() { }

        public TicketApiViewModel(Ticket ticket)
        {
            this.Id = ticket.Id;
            this.Price = ticket.Price;
            this.SellerNotes = ticket.SellerNotes;
            this.SellerId = ticket.SellerId;
            this.EventId = ticket.EventId;
        }

        public int Id { get; set; }

        public decimal Price { get; set; }

        public string SellerNotes { get; set; }

        // public ICollection<OrderingTicket> OrderTickets { get; set; }
        public string SellerId { get; set; }

        // public AppUser Seller { get; set; }
        public int EventId { get; set; }

        public Tuple<string, string> ConvertToCsv()
        {
            var header = $"Ticket{nameof(this.Id)};Ticket{nameof(this.Price)};{nameof(this.SellerNotes)};{nameof(this.SellerId)}";
            var value = $"{this.Id};{this.Price};{this.SellerNotes};{this.SellerId}";

            return new Tuple<string, string>(header, value);
        }
    }

   
}
