﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TicketSaleCore.api.Venues;
using TicketSaleCore.Models.Entities;

namespace TicketSaleCore.api.Events
{
    public class EventApiViewModel: ICsvExtend
    {
        public EventApiViewModel() { }

        public EventApiViewModel(Event eEvent)
        {
            this.Id = eEvent.Id;

            this.Name = eEvent.Name;

            this.Date = eEvent.Date;

            this.EventsTypeId = eEvent.EventsTypeId;

            this.Banner = eEvent.Banner;

            this.Description = eEvent.Description;

            this.VenueId = eEvent.VenueId;

            this.Venue = eEvent.Venue!=null ? new VenuesApiViewModel(eEvent.Venue) : null;

            this.Tickets = new List<TicketApiViewModel>();
            foreach (var ticket in eEvent.Tickets)
            {
                this.Tickets.Add(new TicketApiViewModel(ticket));
            }


        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public int EventsTypeId { get; set; }

        // public virtual EventsType EventsType { get; set; }
        public string Banner { get; set; }

        public string Description { get; set; }

        public int VenueId { get; set; }

        public virtual VenuesApiViewModel Venue { get; set; }

        public virtual ICollection<TicketApiViewModel> Tickets { get; set; }

        public Tuple<string, string> ConvertToCsv()
        {




            var ticketHead = this.Tickets.FirstOrDefault().ConvertToCsv().Item1;
            var   ticketsstr=new StringBuilder();



            var header = $"{nameof(this.Id)}" +
                         $";{nameof(this.Name)}" +
                         $";{nameof(this.Date)}" +
                         $";{nameof(this.EventsTypeId)}" +
                         $";{nameof(this.Banner)}" +
                         $";{nameof(this.Description)}" +
                         $";{this.Venue.ConvertToCsv().Item1}" +
                         $";{ticketHead}";

            var value = $"{this.Id};" +
                        $"{this.Name};" +
                        $"{this.Date};" +
                        $"{this.EventsTypeId};" +
                        $"{this.Banner};" +
                        $"{this.Description};" +
                        $"{this.Venue.ConvertToCsv().Item2};";
            foreach (var ticket in this.Tickets)
            {
                ticketsstr.Append(value);
                ticketsstr.Append($"{ticket.ConvertToCsv().Item2}");
                ticketsstr.Append("\r\n");
            }

            return new Tuple<string, string>(header, ticketsstr.ToString());
        }
    }
}
