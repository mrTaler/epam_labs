﻿namespace TicketSaleCore.AuthorizationPolit.ResourceBased
{
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization.Infrastructure;
    using Microsoft.AspNetCore.Identity;

    using Models.Entities;

    /// <summary>
    /// The user manager acces hander.
    /// </summary>
    public class UserManagerAccesHander : AuthorizationHandler<OperationAuthorizationRequirement, UserManager<AppUser>>
    {
        /// <summary>
        /// The handle requirement async.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="requirement">
        /// The requirement.
        /// </param>
        /// <param name="resource">
        /// The resource.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            OperationAuthorizationRequirement requirement,
            UserManager<AppUser> resource)
        {
            if (requirement.Name == nameof(Operations.Create))
            {
                if (context.User.HasClaim(p => p.Type == "UserManagerAcces" && p.Value.Contains("C")))
                {
                    context.Succeed(requirement);
                }

                return Task.FromResult(0);
            }
            else if (requirement.Name == nameof(Operations.Read))
            {
                if (context.User.HasClaim(p => p.Type == "UserManagerAcces" && p.Value.Contains("R")))
                {
                    context.Succeed(requirement);
                }

                return Task.FromResult(0);
            }
            else if (requirement.Name == nameof(Operations.Update))
            {
                if (context.User.HasClaim(p => p.Type == "UserManagerAcces" && p.Value.Contains("U")))
                {
                    context.Succeed(requirement);
                }

                return Task.FromResult(0);
            }
            else if (requirement.Name == nameof(Operations.Delete))
            {
                if (context.User.HasClaim(p => p.Type == "UserManagerAcces" && p.Value.Contains("D")))
                {
                    context.Succeed(requirement);
                }

                return Task.FromResult(0);
            }

            return Task.FromResult(0);
        }
    }
}