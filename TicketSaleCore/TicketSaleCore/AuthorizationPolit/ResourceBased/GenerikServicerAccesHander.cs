﻿namespace TicketSaleCore.AuthorizationPolit.ResourceBased
{
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization.Infrastructure;

    /// <summary>
    /// The user manager acces hander.
    /// </summary>
    /// <typeparam name="T"> Protected resource
    /// </typeparam>
    public class GenerikServicerAccesHander<T> : AuthorizationHandler<OperationAuthorizationRequirement, T> 
        where T : class 
    {
        /// <summary>
        /// The handle requirement async.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="requirement">
        /// The requirement.
        /// </param>
        /// <param name="resource">
        /// The resource.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            OperationAuthorizationRequirement requirement,
            T resource)
        {
            switch (requirement.Name)
            {
                case nameof(Operations.Create):
                    if (context.User.HasClaim(p => p.Type == resource.GetType().Name && p.Value.Contains("C")))
                    {
                        context.Succeed(requirement);
                    }

                    return Task.FromResult(0);

                case nameof(Operations.Read):
                    var q = resource.GetType().Name;

                    if (context.User.HasClaim(p => p.Type == resource.GetType().Name && p.Value.Contains("R")))
                    {
                        context.Succeed(requirement);
                    } 

                    return Task.FromResult(0);
                case nameof(Operations.Update):
                    if (context.User.HasClaim(p => p.Type == resource.GetType().Name && p.Value.Contains("U")))
                    {
                        context.Succeed(requirement);
                    }

                    return Task.FromResult(0);
                case nameof(Operations.Delete):
                    if (context.User.HasClaim(p => p.Type == resource.GetType().Name && p.Value.Contains("D")))
                    {
                        context.Succeed(requirement);
                    }

                    return Task.FromResult(0);
                default:
                    context.Fail();
                    return Task.FromResult(0);
            }

        }
    }
    }