﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace TicketSaleCore.AuthorizationPolit.ResourceBased
{
    public static class Operations
    {
        /// <summary>
        /// The claim type for db work.
        /// </summary>
        public const string ClaimTypeForDbWork = "CRUD";

        /// <summary>
        /// The create.
        /// </summary>
        public static OperationAuthorizationRequirement Create
            = new OperationAuthorizationRequirement { Name = "Create" };

        /// <summary>
        /// The read.
        /// </summary>
        public static OperationAuthorizationRequirement Read
            = new OperationAuthorizationRequirement { Name = "Read" };

        /// <summary>
        /// The update.
        /// </summary>
        public static OperationAuthorizationRequirement Update
            = new OperationAuthorizationRequirement { Name = "Update" };

        /// <summary>
        /// The delete.
        /// </summary>
        public static OperationAuthorizationRequirement Delete
            = new OperationAuthorizationRequirement { Name = "Delete" };
    }
}