using System;
using System.Linq.Expressions;
using TicketSaleCore.api.Events;
using TicketSaleCore.Models.BLL.Specification.OrderBy;
using TicketSaleCore.Models.DAL.Specifications.OrderSpecification;

namespace TicketSaleCore.Api.Events
{
    /// <inheritdoc />
    /// <summary>
    /// The data order specification.
    /// </summary>
    public class SortEventApiViewModelByDataSpecification : OrderSpecification<EventApiViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SortEventByDataOrderSpecification"/> class.
        /// </summary>
        /// <param name="order">
        /// The order.
        /// </param>
        public SortEventApiViewModelByDataSpecification(OrderEn order = OrderEn.Ascending)
            : base(order)
        {
        }

        /// <summary>
        /// The as expression.
        /// </summary>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public override Expression<Func<EventApiViewModel, IComparable>> AsExpression()
        {
            return entity => entity.Date;
        }
    }

    public class SortEventApiViewModelByNameSpecification : OrderSpecification<EventApiViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SortEventByDataOrderSpecification"/> class.
        /// </summary>
        /// <param name="order">
        /// The order.
        /// </param>
        public SortEventApiViewModelByNameSpecification(OrderEn order = OrderEn.Ascending)
            : base(order)
        {
        }

        /// <summary>
        /// The as expression.
        /// </summary>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public override Expression<Func<EventApiViewModel, IComparable>> AsExpression()
        {
            return entity => entity.Name;
        }
    }
}