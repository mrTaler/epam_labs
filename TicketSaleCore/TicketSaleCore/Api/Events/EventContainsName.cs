﻿namespace TicketSaleCore.Models.BLL.Specification.EventSpecification
{
    using System;
    using System.Linq.Expressions;

    using DAL.Specifications.Interfaces;
    using Entities;
    using TicketSaleCore.Models.DAL.Specifications.Specifications;

    /// <summary>
    /// The event find by name.
    /// </summary>
    public class EventContainsName : Specification<Event>
    {
        public EventContainsName(string name)
        {
            this.name = name;

        }

        private string name;
        
        public override Expression<Func<Event, bool>> AsExpression()
        {
            return p => p.Name.ToLower().Contains(this.name.ToLower());
        }
    }
}
